/*
 * MDB_UART.c
 *
 * Created: 7/7/2021 4:01:48 PM
 *  Author: User
 */

#include "basic_requirement.h"



unsigned char gucRx_Product_Code[5]={0,0,0,0,0};

const unsigned char Key_Matrix[7][4]={{'1','2','3'},
{'4','5','6'},
{'7','8','9'},
{'*','0','#'}};

//const unsigned char Key_Matrix[7][4]={  {'A','1','2'},
										//{'B','3','4'},
										//{'C','5','6'},
										//{'D','7','8'}};


unsigned char ucMatched_Row=0, ucMatched_Col=0;
unsigned char ucKeyPressMask_Flg=0, ucKey_DelayTimerSRT_FLG=0,ucKeyEND_FLG=0;
unsigned int uiKey_DelayTimerCount=0;
volatile unsigned char ucKey_Sqnce_Number=0;








////**********************************************************************************************************************//
//********************************** void Start_Key_Pressing(void)  *****************************************************//
void Start_Key_Pressing(void)
{

	gucAmountCreditedPressKey=1;
	ucKeyPressMask_Flg=1;
	ucKey_Sqnce_Number=1;
}
////**********************************************************************************************************************//
//********************************** void Clear_Key_Pressing(void)  *****************************************************//
void Clear_Key_Pressing(void)
{

	gucAmountCreditedPressKey=0;
	ucKeyPressMask_Flg=0;
	ucKey_Sqnce_Number=0;
	ucKey_DelayTimerSRT_FLG=0;
	uiKey_DelayTimerCount=0;
}


////**********************************************************************************************************************//
//********************************** void Press_Keys(unsigned char Key_posi_Num)  ****************************************//
void Press_Keys(unsigned char Key_posi_Num)
{
	if (ucKeyPressMask_Flg==1)
	{


		switch (Key_posi_Num)
		{
			case 1:
					if (gucRx_Product_Code[0]>=0x30)
					{
						Compare_2D_Array(gucRx_Product_Code[0]);
						Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//Debug_PutString("\r\n Key case 11");
						Debug_PutString("\r\nKey Pos 1");
					}
					else
					{
						ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=1;
						Debug_PutString("\r\nKey Pos 1 is NULL");
					}
					//Debug_PutString("\r\n Key case 1");
					break;
			case 2:
					if (gucRx_Product_Code[1]>=0x30)
					{
						Compare_2D_Array(gucRx_Product_Code[1]);
						Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//Debug_PutString("\r\n Key case 22");
						Debug_PutString("\r\nKey Pos 2");
					}
					else
					{
						ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=1;
						Debug_PutString("\r\nKey Pos 2 is NULL");
					}
					//Debug_PutString("\r\n Key case 2");
					break;

			case 3:
					if (gucRx_Product_Code[2]>=0x30)
					{
						Compare_2D_Array(gucRx_Product_Code[2]);
						Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//Debug_PutString("\r\n Key case 33");
						Debug_PutString("\r\nKey Pos 3");
					}
					else
					{
						ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=1;
						Debug_PutString("\r\nKey Pos 3 is NULL");
					}
					//Debug_PutString("\r\n Key case 3");
					break;
			case 4:
					if (gucRx_Product_Code[3]>=0x30)
					{
						Compare_2D_Array(gucRx_Product_Code[3]);
						Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//ucKeyEND_FLG=1;
						Debug_PutString("\r\nKey Pos 4");

					}
					else
					{
						//ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=0;
						ucKeyEND_FLG=1;
						Debug_PutString("\r\nKey Pos 4 is NULL");

					}
					//Debug_PutString("\r\n Key case 4");
					break;

			default: //ucKeyPressMask_Flg=1;
					 Debug_PutString("\r\n Key Not Matched");
					 MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,6);  // MDB_Logbyte1 Bit 6
					break;

		}
	}

	else if (ucKeyPressMask_Flg==2)
	{

		switch (Key_posi_Num)
		{
			case 1:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
					ucKeyPressMask_Flg=0;

					//Debug_PutString("\r\ncase 1 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;
			case 2:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
					ucKeyPressMask_Flg=0;

					//Debug_PutString("\r\ncase 2 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;

			case 3:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
					ucKeyPressMask_Flg=0;

					//Debug_PutString("\r\ncase 3 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;
			case 4:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKeyPressMask_Flg=0;
					ucKey_DelayTimerSRT_FLG=0;
					uiKey_DelayTimerCount=0;
					ucKeyPressMask_Flg=0;
					ucKeyEND_FLG=1;
					ucKey_Sqnce_Number=0;
					//Debug_PutString("\r\ncase 4 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;

			default: //ucKeyPressMask_Flg=1;
					break;

		}






	}

}



////**********************************************************************************************************************//
//********************************** void Compare_2D_Array( unsigned char value)  ****************************************//
void Compare_2D_Array( unsigned char value)
{
	   for (int row = 0; row < 7 ; row++)
	   {
		   for (int col = 0; col < 4; col++)
		   {
			   if (Key_Matrix[row][col] == value)
			   {
				 ucMatched_Row=row+1;
				 ucMatched_Col=col+1;
				// Debug_PutString("\r\n Matched ROW: ") ;
				 //Debug_Put_Num(row,2);
				 //Debug_PutString("\r\n Matched COL: ");
				 //Debug_Put_Num(col,2);
				 break;
			   }

		   }
	   }

}



////**********************************************************************************************************************//
//********************************** void Pressed_Matrix_KeyON(unsigned char Row,unsigned char col)  *********************//
void Pressed_Matrix_KeyON(unsigned char Row,unsigned char col)
{
	switch (Row)
	{
		case 1:
				switch (col)
				{
					case 1: gpio_set_pin_level(R1C1_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R1C1");
							break;
					case 2: gpio_set_pin_level(R1C2_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R1C2");
							break;
					case 3: gpio_set_pin_level(R1C3_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R1C3");
							break;
					case 4: gpio_set_pin_level(R1C4_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R1C42");
							break;
					default: break;
				}
				break;

		case 2:
				switch (col)
				{
					case 1: gpio_set_pin_level(R2C1_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R2C1");
							break;
					case 2: gpio_set_pin_level(R2C2_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R2C2");
							break;
					case 3: gpio_set_pin_level(R2C3_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R2C3");
							break;
					case 4: gpio_set_pin_level(R2C4_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R2C4");
							break;
					default: break	;
				}
				break;

		case 3:
				switch (col)
				{
					case 1: gpio_set_pin_level(R3C1_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R3C1");
							break;
					case 2: gpio_set_pin_level(R3C2_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R3C2");
							break;
					case 3: gpio_set_pin_level(R3C3_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R3C3");
							break;
					case 4: gpio_set_pin_level(R3C4_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R3C4");
							break;
					default: break;
				}
				break;

		case 4:
				switch (col)
				{
					case 1: gpio_set_pin_level(R4C1_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R24C1");
							break;
					case 2: gpio_set_pin_level(R4C2_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R4C2");
							break;
					case 3: gpio_set_pin_level(R4C3_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R4C3");
							break;
					case 4: gpio_set_pin_level(R4C4_RELAY,1);
							gpio_set_pin_level(RELAY_LED,1);
							Debug_PutString("\r\n R4C4");
							break;
					default: break;
				}
				break;
		default: break;
	}

}

////**********************************************************************************************************************//
//********************************** void Press_Keys(unsigned char Key_posi_Num)  ****************************************//
void Pressed_Matrix_KeyOFF(unsigned char Row1,unsigned char col1)
{
	switch (Row1)
	{
		case 1:
				switch (col1)
				{
					case 1: gpio_set_pin_level(R1C1_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR1C1");
							break;
					case 2: gpio_set_pin_level(R1C2_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR1C2");
							break;
					case 3: gpio_set_pin_level(R1C3_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR1C3");
							break;
					case 4: gpio_set_pin_level(R1C4_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR1C42");
							break;
					default: break;
				}
				break;

		case 2:
				switch (col1)
				{
					case 1: gpio_set_pin_level(R2C1_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR2C1");
							break;
					case 2: gpio_set_pin_level(R2C2_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR2C2");
							break;
					case 3: gpio_set_pin_level(R2C3_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR2C3");
							break;
					case 4: gpio_set_pin_level(R2C4_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR2C4");
							break;
					default: break	;
				}
				break;

		case 3:
				switch (col1)
				{
					case 1: gpio_set_pin_level(R3C1_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR3C1");
							break;
					case 2: gpio_set_pin_level(R3C2_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR3C2");
							break;
					case 3: gpio_set_pin_level(R3C3_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR3C3");
							break;
					case 4: gpio_set_pin_level(R3C4_RELAY,0);
					        gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR3C4");
							break;
					default: break;
				}
				break;

		case 4:
				switch (col1)
				{
					case 1: gpio_set_pin_level(R4C1_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR24C1");
							break;
					case 2: gpio_set_pin_level(R4C2_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR4C2");
							break;
					case 3: gpio_set_pin_level(R4C3_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR4C3");
							break;
					case 4: gpio_set_pin_level(R4C4_RELAY,0);
							gpio_set_pin_level(RELAY_LED,0);
							Debug_PutString("\r\n OFR4C4");
							break;
					default: break;
				}
				break;
		default: break;
	}

}





////**********************************************************************************************************************//
//******************************************** OLD Function for key pad  *************************************************//
////**********************************************************************************************************************//

unsigned int compare_strings(const unsigned char a[], unsigned char b[])
{
	int c = 0;

	while (a[c] == b[c])
	{
		if (a[c] == '\0' || b[c] == '\0')
		break;
		c++;
	}

	if (a[c] == '\0' && b[c] == '\0')
	return 0;
	else
	return 1;
}


void motor_delay_ON()
{
	delay_ms(1000);
}
void motor_delay_OFF()
{
	delay_ms(1000);
}



void PressMachineKey(unsigned char gucMCMotorNumber)
{
	////	Debug_PutString("\r\n Pressing Key:- ");

//	gucMCMotorNumber = gucMCMotorNumber-1;
	switch(gucMCMotorNumber)
	{
		case 1: //10
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 2: //11
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
				delay_ms(500);
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 3: //12

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				//motor_delay_OFF();

				break;

		case 4: //13

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				//motor_delay_OFF();



		break;

		case 5: //14

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				//motor_delay_OFF();

				break;

		case 6: //15
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();

				break;

		case 7: //16
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				//motor_delay_OFF();

				break;

		case 8: //17
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C1_RELAY,0);
				//motor_delay_OFF();

				break;

		case 9: //18
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 10: //19
				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 11: //20

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 12: //21

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 13: //22

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
				delay_ms(500);
				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 14://23

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 15://24
				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 16: //25
				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 17: //26
				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 18: //27
				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 19: //28
				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 20: //29

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C3_RELAY,0);
				//motor_delay_OFF();
				break;


		case 21: //30
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				//motor_delay_OFF();

				break;

		case 22: //31
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 23: //32
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 24: //33
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
				delay_ms(500);
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 25: //34
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				//motor_delay_OFF();
				break;


		case 26: //35
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();

				break;

		case 27: //36
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 28: //37
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 29: //38
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 30: //39
				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C3_RELAY,0);
				//motor_delay_OFF();
				break;


		case 31: //40

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 32: //41

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 33://42

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 34: //43
				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 35: //44

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
				delay_ms(500);

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 36: //45
				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 37: //46
				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 38: //47
				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 39: //48
				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 40: //49
				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C3_RELAY,0);
				//motor_delay_OFF();

				break;

		case 41: //50
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 42: //51
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 43: //52
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 44: //53
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 45: //54
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				//motor_delay_OFF();
				break;


		case 46: //55
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
		        delay_ms(500);
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 47: //56
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 48: //57
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 49: //58
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 50: //59
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 51: //60
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 52: //61
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 53: //62
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 54: //63
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 55: //64
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();



				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				//motor_delay_OFF();
				break;


		case 56: //65
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 57: //66
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
				delay_ms(500);
		        Debug_PutString("Start ESC Timer\r\n");

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				//motor_delay_OFF();
				break;

		case 58: //67
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C1_RELAY,0);
				//motor_delay_OFF();
				break;

		case 59: //68
				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C2_RELAY,0);
				//motor_delay_OFF();
				break;

		case 60: //69

				gpio_set_pin_level(R2C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C3_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R3C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R3C3_RELAY,0);
				//motor_delay_OFF();
				break;



		default :
				break;

	}


}




void KeyPAD_INIT(void)
{
	gpio_set_pin_direction(RELAY_LED,GPIO_DIRECTION_OUT);

	gpio_set_pin_direction(KEY1,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY2,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY3,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY4,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY5,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY6,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY7,GPIO_DIRECTION_OUT);

	gpio_set_pin_direction(KEY8,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY9,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY10,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY11,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY12,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY13,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY14,GPIO_DIRECTION_OUT);

	gpio_set_pin_direction(KEY15,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY16,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY17,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY18,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY19,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY20,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY21,GPIO_DIRECTION_OUT);



	gpio_set_pin_direction(KEY22,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY23,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY24,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY25,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY26,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY27,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(KEY28,GPIO_DIRECTION_OUT);

}




void BEVMAX1_PressMachineKey(unsigned char gucMCMotorNumber)
{
	////	Debug_PutString("\r\n Pressing Key:- ");

	gucMCMotorNumber = gucMCMotorNumber-1;
	switch(gucMCMotorNumber)
	{
		case 1: //A1
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 2: //A2
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 3: //A3

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 4: //A4

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();



		break;

		case 5: //A5

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 6: //A6
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();

		break;

		case 7: //A7
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 8: //A8
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C3_RELAY,0);
		//motor_delay_OFF();

		break;

		case 9: //A9
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R5C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C2_RELAY,0);
		//motor_delay_OFF();
		break;
		case 10: //A10

		break;

		case 11: //B1

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 12: //B2

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 13: //B3

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 14://B4

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 15://B5
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 16: //B6
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 17: //B7
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 18: //B8
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 19: //B9
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R5C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C2_RELAY,0);
		//motor_delay_OFF();

		break;
		case 20: //B10

		break;

		case 21: //C1
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		break;

		case 22: //C2
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 23: //C3
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 24: //C4
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 25: //C5
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		motor_delay_OFF();
		break;


		case 26: //C6
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		motor_delay_OFF();

		break;

		case 27: //C7
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 28: //C8
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 29: //C9
		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R5C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C2_RELAY,0);
		motor_delay_OFF();
		break;
		case 30: //C10

		break;

		case 31: //D1

		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 32: //D2

		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 33://D3

		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 34: //D4
		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 35: //D5
		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 36: //D6
		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 37: //D7
		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 38: //D8
		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 39: //D9
		gpio_set_pin_level(R4C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R5C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C2_RELAY,0);
		motor_delay_OFF();
		break;
		case 40: //D10

		break;

		case 41: //E1
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 42: //E2
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 43: //E3
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 44: //E4
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 45: //E5
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		motor_delay_OFF();
		break;


		case 46: //E6
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 47: //E7
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 48: //E8
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 49: //E9
		gpio_set_pin_level(R5C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R5C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C2_RELAY,0);
		motor_delay_OFF();
		break;
		case 50: //E10

		break;
		case 51: //F1
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 52: //F2
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 53: //F3
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 54: //F4
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 55: //F5
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		motor_delay_OFF();
		break;


		case 56: //F6
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		motor_delay_OFF();
		break;

		case 57: //F7
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 58: //F8
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C3_RELAY,0);
		motor_delay_OFF();
		break;
		case 59: //F9
		gpio_set_pin_level(R6C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R6C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R5C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R5C2_RELAY,0);
		motor_delay_OFF();
		break;

		case 60: //F10

		break;

		default :
		break;

	}


}



void SEAGA_MachineKey(unsigned char gucMCMotorNumber)
{
	////	Debug_PutString("\r\n Pressing Key:- ");

	//	gucMCMotorNumber = gucMCMotorNumber-1;
	switch(gucMCMotorNumber)
	{
		case 1: //10
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 2: //11
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		//motor_delay_OFF();
		delay_ms(500);
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 3: //12

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 4: //13

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();



		break;

		case 5: //14

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		//motor_delay_OFF();

		break;

		case 6: //15
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 7: //16
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();

		break;

		case 8: //17
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		//motor_delay_OFF();

		break;

		case 9: //18
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 10: //19
		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 11: //20

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 12: //21

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 13: //22

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		//motor_delay_OFF();
		delay_ms(500);
		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 14://23

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 15://24
		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 16: //25
		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 17: //26
		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 18: //27
		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 19: //28
		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 20: //29

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();
		break;


		case 21: //30
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 22: //31
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 23: //32
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 24: //33
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		//motor_delay_OFF();
		delay_ms(500);
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 25: //34
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		//motor_delay_OFF();
		break;


		case 26: //35
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();

		break;

		case 27: //36
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 28: //37
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 29: //38
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 30: //39
		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();
		break;


		case 31: //40

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 32: //41

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 33://42

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 34: //43
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 35: //44

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		//motor_delay_OFF();
		delay_ms(500);

		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 36: //45
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 37: //46
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 38: //47
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 39: //48
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 40: //49
		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();

		break;

		case 41: //501
					gpio_set_pin_level(R2C2_RELAY,1);
					motor_delay_ON();
					gpio_set_pin_level(R2C2_RELAY,0);
					motor_delay_OFF();

					gpio_set_pin_level(R4C2_RELAY,1);
					motor_delay_ON();
					gpio_set_pin_level(R4C2_RELAY,0);
					motor_delay_OFF();

					gpio_set_pin_level(R1C1_RELAY,1);
					motor_delay_ON();
					gpio_set_pin_level(R1C1_RELAY,0);
					break;

		case 42: //502
					gpio_set_pin_level(R2C2_RELAY,1);
					motor_delay_ON();
					gpio_set_pin_level(R2C2_RELAY,0);
					motor_delay_OFF();

					gpio_set_pin_level(R4C2_RELAY,1);
					motor_delay_ON();
					gpio_set_pin_level(R4C2_RELAY,0);
					motor_delay_OFF();

					gpio_set_pin_level(R1C2_RELAY,1);
					motor_delay_ON();
					gpio_set_pin_level(R1C2_RELAY,0);
					//motor_delay_OFF();
					break;

		case 43: //503
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R1C3_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R1C3_RELAY,0);

				break;

		case 44: //504
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C1_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C1_RELAY,0);
				break;

		case 45: //505
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R4C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R4C2_RELAY,0);
				motor_delay_OFF();

				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);

				break;


		case 46: //55
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				motor_delay_OFF();

				//motor_delay_OFF();
				delay_ms(500);
				gpio_set_pin_level(R2C2_RELAY,1);
				motor_delay_ON();
				gpio_set_pin_level(R2C2_RELAY,0);
				//motor_delay_OFF();
		break;

		case 47: //56
		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 48: //57
		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 49: //58
		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 50: //59
		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 51: //60
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R4C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R4C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 52: //61
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 53: //62
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 54: //63
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R1C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R1C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 55: //64
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();



		gpio_set_pin_level(R2C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C1_RELAY,0);
		//motor_delay_OFF();
		break;


		case 56: //65
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R2C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 57: //66
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		//motor_delay_OFF();
		delay_ms(500);
		Debug_PutString("Start ESC Timer\r\n");

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		//motor_delay_OFF();
		break;

		case 58: //67
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C1_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C1_RELAY,0);
		//motor_delay_OFF();
		break;

		case 59: //68
		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C2_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C2_RELAY,0);
		//motor_delay_OFF();
		break;

		case 60: //69

		gpio_set_pin_level(R2C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R2C3_RELAY,0);
		motor_delay_OFF();

		gpio_set_pin_level(R3C3_RELAY,1);
		motor_delay_ON();
		gpio_set_pin_level(R3C3_RELAY,0);
		//motor_delay_OFF();
		break;



		default :
		break;

	}


}






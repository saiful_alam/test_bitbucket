/*
 * ble.h
 *
 * Created: 5/13/2021 4:24:06 PM
 *  Author: User
 */ 


#ifndef BLE_H_
#define BLE_H_


#define PA16 GPIO(GPIO_PORTA, 16)
#define PA17 GPIO(GPIO_PORTA, 17)

//#define BLE_RESET GPIO(GPIO_PORTA, 20)
//#define BLE_STATUS_PIN GPIO(GPIO_PORTA, 21)





extern void initilize_BLE_uart(void);
extern void BLE_PutChar(unsigned char data);
extern void BLE_PutString(unsigned char *str);
extern void BLE_PutStringL(unsigned char *str,unsigned int len);
//-------------- Protocol Addition ----------------

#define dNULL                       0x00

#define dPROTO_REQUEST				0xA5
#define dPROTO_RESPONSE				0xA6

#define dAPP_CONNECTED				0xFD
#define dAPP_DISCONNECTED			0xFE
#define dHARDWARE_INFO				0xD0
#define dVEND_PRODUCT				0xD1
#define dVEND_SUCESS				0xD2
#define dVEND_CANCEL				0xD3
#define dVEND_FAIL					0xD4
#define dMACHINE_READY				0xD5
#define dSEND_DEX_DATA				0xD6
#define dDEX_PAGE_UPDATE			0xD7
#define dEND_DEX_DATA				0xD8
#define dSAFE_DISCONNECT			0xD9
#define dPARCED_DEX_DATA			0xDA
#define dEND_OF_PARCED_DEX_DATA		0xDB
#define dTAB_TO_PAY_RQST		    0xDD
#define dMONY_ACCEPTED		        0xDE

#define dAPP_ACK				0x01
#define dAPP_NACK				0x00
typedef struct
{
	uint8_t hardware_version;
	uint8_t software_version;
	uint8_t error_code;
	uint8_t last_vend_status;
	uint8_t dex_file_present_status;
	uint8_t machine_type;
}HardwareInfo_config_t;

typedef struct
{
	uint8_t compartment[4];
	uint8_t price[4];
	uint8_t ack_nack;
}VendRequest_Config_t;



extern HardwareInfo_config_t hardwareInfo;
extern VendRequest_Config_t vendRequest;



#define dRXBLE_BUFFER_INDEX  40
#define dTXBLE_BUFFER_INDEX  520



extern unsigned char gucBLE_RXBuffer[dRXBLE_BUFFER_INDEX],gucBLE_RXBuffer_copy[dRXBLE_BUFFER_INDEX];
extern unsigned char gucBLE_TXBuffer[dTXBLE_BUFFER_INDEX];
extern unsigned char gucBLE_Frm_rec_Flag;

extern unsigned char gucMony_Acc_Buff[10];




extern unsigned int guiBLE_RXBuffer_Index;






extern unsigned int guiDex_Data_Total_Page,guiCurrentDexPage;

extern unsigned char gucVendkinAppConnected;
extern unsigned char gucResp,gucRept,guAxpect_Subcode;

extern unsigned int guiBLE_Total_Frm_Len,guiExpectedLen;

extern unsigned char gucVendCancelReasone;

extern void send_NACK_onCRC_Mismatch(unsigned char );



extern void Send_Hardware_Info_Debug18();

extern void Send_Hardware_Info();

extern void Send_VendRequest_ACK_NACK(unsigned char);
extern void Send_VendSucess();
extern void  Send_VendFail(unsigned char errorname);
extern void Send_VendCancel(unsigned char response);
extern void Send_MachineReady();

extern void Send_Parced_DEX_start_Info();
extern void Write_Data_on_BLE_Write(unsigned int guiCurrentDexPage);
extern void Send_Parced_Dex_Data_Update();
extern void Send_Parced_Dex_data_Complete();
extern void BLE_CovertChar_in_ASCII(uint8_t data);
extern void BLE_CovertChar_in_ASCIIwithLen(uint8_t *data,int len);
extern void Send_BLE_Safe_Disconnect();

extern void Send_TAB_TO_PAY_RQST_ACK_NACK(unsigned char ack);
extern void Send_Money_Creadited_byCreditCard(unsigned char Crdt_Status);

extern void Reset_Bluetooth();
#endif /* BLE_H_ */
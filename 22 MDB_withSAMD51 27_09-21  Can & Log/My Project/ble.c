/*
 * ble.c
 *
 * Created: 5/13/2021 4:23:56 PM
 *  Author: User
 */ 

#include "basic_requirement.h"

unsigned char gucResp=0,gucRept=0,guAxpect_Subcode=0;


unsigned char gucBLE_RXBuffer[dRXBLE_BUFFER_INDEX];

unsigned char gucMony_Acc_Buff[10];

unsigned char gucBLE_TXBuffer[dTXBLE_BUFFER_INDEX];
unsigned char gucBLE_Frm_rec_Flag=0;


unsigned int guiBLE_RXBuffer_Index=0;


unsigned int guiDex_Data_Total_Page,guiCurrentDexPage;

HardwareInfo_config_t hardwareInfo;
VendRequest_Config_t vendRequest;
//VendSucess_Config_t vendSucess;
//VendCancel_Config_t vendCancel;
//VendFail_Config_t vendFail;

unsigned char gucVendkinAppConnected=0;


unsigned char gucVendCancelReasone;


//local variable only related to this file
unsigned int guiBLE_Total_Frm_Len=0,guiExpectedLen=0;
unsigned char gucBLE_St_RcvData_Flag=0;


void SERCOM3_2_Handler()
{

	volatile unsigned char bleRXData;
	
	if (REG_SERCOM3_USART_INTFLAG & 4)
	{
		bleRXData = REG_SERCOM3_USART_DATA;     /* read the receive char and return it */
		
			 if(((bleRXData==0xA6)||(bleRXData==0xA5)) && guiBLE_RXBuffer_Index==0 && gucBLE_Frm_rec_Flag==0)
			 {
				 gucBLE_RXBuffer[guiBLE_RXBuffer_Index]=bleRXData;		//save the data after start request Header
				 guiBLE_RXBuffer_Index++;                                // increment the index of array to save data
			 }
			 else if(guiBLE_RXBuffer_Index==1 && gucBLE_Frm_rec_Flag==0)
			 {
				 gucBLE_RXBuffer[guiBLE_RXBuffer_Index]=bleRXData;		//save the data after start request Header
				 guiBLE_RXBuffer_Index++;                                // increment the index of array to save data
			 }
			 else if(guiBLE_RXBuffer_Index==2 && gucBLE_Frm_rec_Flag==0)
			 {
				 gucBLE_RXBuffer[guiBLE_RXBuffer_Index]=bleRXData;		//save the data after start request Header
				 guiBLE_RXBuffer_Index++;                                // increment the index of array to save data
				 guiBLE_Total_Frm_Len= (gucBLE_RXBuffer[1]);
				 guiBLE_Total_Frm_Len =(guiBLE_Total_Frm_Len<<8) | gucBLE_RXBuffer[2] ;
			 }
			 else if(guiBLE_RXBuffer_Index==3 && gucBLE_Frm_rec_Flag==0)
			 {
				 gucBLE_RXBuffer[guiBLE_RXBuffer_Index]=bleRXData;		//save the data after start request Header
				 guiBLE_RXBuffer_Index++;
	 
				 switch(gucBLE_RXBuffer[3])
				 {
				case dAPP_CONNECTED:
				guiExpectedLen=0x08;
				break;

				case dAPP_DISCONNECTED:
				guiExpectedLen=0x08;
				break;

				case dHARDWARE_INFO:
				guiExpectedLen=0x03;
				break;

				case dVEND_PRODUCT:
				guiExpectedLen=0x0A;
				break;

				case dVEND_SUCESS:
				guiExpectedLen=0x03;
				break;

				case dVEND_CANCEL:
				guiExpectedLen=0x03;
				break;

				case dVEND_FAIL:
				guiExpectedLen=0x03;
				break;

				case dMACHINE_READY:
				guiExpectedLen=0x03;
				break;

				case dSEND_DEX_DATA:
				//guiExpectedLen=0x07;
				break;

				case dDEX_PAGE_UPDATE:
				guiExpectedLen=0x03;
				break;

				case dEND_DEX_DATA:
				//guiExpectedLen=0x07;
				break;

				case dSAFE_DISCONNECT:
				//guiExpectedLen=0x07;
				break;

				case dEND_OF_PARCED_DEX_DATA:
				guiExpectedLen=0x03;
				break;

				case dPARCED_DEX_DATA:
				guiExpectedLen=0x03;
				break;
				case dTAB_TO_PAY_RQST:
				guiExpectedLen=0x0A;
				break;
				case dMONY_ACCEPTED:
				guiExpectedLen=0x03;
				break;
				default:
					break;
				 }

				 gucBLE_St_RcvData_Flag=1;                           //make flag high to indicate data receiving is ON
			 }
			 else if(gucBLE_St_RcvData_Flag==1)                             //	check reception is on or not to save data
			 {
				 gucBLE_RXBuffer[guiBLE_RXBuffer_Index]=bleRXData;		//save the character in array
				 guiBLE_RXBuffer_Index++;							// increase array index
				 //if(guiBLE_Total_Frm_Len==(guiBLE_RXBuffer_Index-3))                //check whether is frame correctly received or not
	 
				 if(guiExpectedLen==(guiBLE_RXBuffer_Index-3))                //check whether is frame correctly received or not
				 {
					 

		 
					 gucBLE_St_RcvData_Flag=0;                                  // make reception is off
					 gucBLE_Frm_rec_Flag=1;                                  // flag high to show correct frame is received
				 }
			 }
		REG_SERCOM5_USART_INTFLAG = 4;  /* clear interrupt flag */
	}
}

void initilize_BLE_uart(void)
{
	
	GCLK->PCHCTRL[24].reg=  0x40;		//selecting the main core clock for SERCOM2
	GCLK->PCHCTRL[3].reg=  0x43;		//selecting slow clock for SERCOM2
	MCLK->APBBMASK.reg |= MCLK_APBBMASK_SERCOM3;
	
	
	gpio_set_pin_function(PA17, PINMUX_PA17D_SERCOM3_PAD0);
	gpio_set_pin_function(PA16, PINMUX_PA16D_SERCOM3_PAD1);
	
	REG_SERCOM3_USART_CTRLA |= 1;               /* reset SERCOM7 */
    while (REG_SERCOM3_USART_SYNCBUSY & 1) {}   /* wait for reset to complete */
    REG_SERCOM3_USART_CTRLA = 0x40106004;       /* LSB first, async, no parity,
        PAD[0]-Tx, BAUD uses fraction, 8x oversampling, internal clock */
    REG_SERCOM3_USART_CTRLB = 0x00030000;       /* enable Tx/Rx, one stop bit, 8 bit */
    REG_SERCOM3_USART_BAUD  = (((float)dXTAL_FREQUENCY / 8)/dBLE_BAUDRATE);	//set Baud rate ;               /* 1000000/8/9600 = 13.02 */
    REG_SERCOM3_USART_CTRLA |= 2;               /* enable SERCOM3 */
    while (REG_SERCOM3_USART_SYNCBUSY & 2) {}   /* wait for enable to complete */
		
	REG_SERCOM3_USART_INTENSET = 4;             /* enable receive complete interrupt */
	
	
	//GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_SERCOM3_CORE;      // SERCOM0 peripheral channel
	//GCLK->CLKCTRL.reg |= GCLK_CLKCTRL_GEN_GCLK0;           // select source GCLK_GEN[1]
	//GCLK->CLKCTRL.bit.CLKEN = 1;                           // enable generic clock
	//
	//gpio_set_pin_function(PA16, PINMUX_PA17D_SERCOM3_PAD0);
	//gpio_set_pin_function(PA17, PINMUX_PA16D_SERCOM3_PAD1);
//
	//PM->APBCSEL.bit.APBCDIV = 0;                           // no pre scaler
	//PM->APBCMASK.bit.SERCOM3_ = 1;                         // enable SERCOM0 interface
	//
	//uint16_t baud_value;
	//baud_value=0xC504;//calculate_baud_value(USART_BLE_BAUD_RATE,system_gclk_chan_get_hz(SERCOM5_GCLK_ID_CORE),USART_SAMPLE_NUM);
	//
	//SERCOM3->USART.CTRLA.reg=SERCOM_USART_CTRLA_DORD|SERCOM_USART_CTRLA_RXPO(0x1)|SERCOM_USART_CTRLA_TXPO(0x0)|SERCOM_USART_CTRLA_SAMPR(0x0)|SERCOM_USART_CTRLA_RUNSTDBY|SERCOM_USART_CTRLA_MODE_USART_INT_CLK;
	//
	///* baud register value corresponds to the device communication baud rate */
	//SERCOM3->USART.BAUD.reg=baud_value;
	//
	///* 8-bits size is selected as character size by setting the bit CHSIZE as 0, TXEN bit and RXEN bits are set to enable the Transmitter and receiver*/
	//SERCOM3->USART.CTRLB.reg|=SERCOM_USART_CTRLB_CHSIZE(0x0)|SERCOM_USART_CTRLB_TXEN|SERCOM_USART_CTRLB_RXEN;
	//
	///* synchronization busy */
	//while(SERCOM3->USART.SYNCBUSY.bit.CTRLB);
	//
	///* SERCOM2 handler enabled */
	////system_interrupt_enable(SERCOM5_IRQn);
	//REG_SERCOM3_USART_INTENSET = 4;             /* enable receive complete interrupt */
	///* receive complete interrupt set */
	//SERCOM3->USART.INTENSET.reg=SERCOM_USART_INTFLAG_RXC;
	///* SERCOM2 peripheral enabled */
	//SERCOM3->USART.CTRLA.reg|=SERCOM_USART_CTRLA_ENABLE;
	///* synchronization busy */
	//while(SERCOM3->USART.SYNCBUSY.reg&SERCOM_USART_SYNCBUSY_ENABLE);
}

void BLE_PutChar(unsigned char data)
{
	while(!(REG_SERCOM3_USART_INTFLAG & 1)) {}  /* wait for data register empty */
	REG_SERCOM3_USART_DATA = data;              /* send a char */
}

void BLE_PutString(unsigned char *str)
{
	unsigned int i=0;
	while(str[i]!='\0')                 //check for NULL character to terminate loop
	{
		while(!(REG_SERCOM3_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM3_USART_DATA = str[i];              /* send a char */
		i++;
	}
}

void BLE_PutStringL(unsigned char *str,unsigned int len)
{
	unsigned int i=0;
	while(len>0)
	{
		while(!(REG_SERCOM3_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM3_USART_DATA = str[i];              /* send a char */
		i++;
		len--;
	}
}



void Send_Hardware_Info()
{
	unsigned int tempcehksum=0;
	
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x09;
	gucBLE_TXBuffer[3] = dHARDWARE_INFO;
	gucBLE_TXBuffer[4] = dHARDWARE_VERSION;
	gucBLE_TXBuffer[5] = dSOFWARE_VERSION;
	gucBLE_TXBuffer[6] = 0x05;
	//	 hardwareInfo.error_code=0x01;      //ok state
	gucBLE_TXBuffer[7] = hardwareInfo.error_code;
	gucBLE_TXBuffer[8] = hardwareInfo.last_vend_status;
	gucBLE_TXBuffer[9] = hardwareInfo.dex_file_present_status;
	//gucBLE_TXBuffer[10] = hardwareInfo.machine_type;
	gucBLE_TXBuffer[10] = 0x01;
	
	for(int tlen=0;tlen<=10;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	
	gucBLE_TXBuffer[11] = tempcehksum;
	gucBLE_TXBuffer[12] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,11);
	
	//Debug_PutString("\r\n Sending HW info :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,13);
	
	guAxpect_Subcode = dHARDWARE_INFO;
	BLE_PutStringL(gucBLE_TXBuffer,13);
}








void Send_Hardware_Info_Debug18()
{
	unsigned int tempcehksum=0;
	
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x10;
	gucBLE_TXBuffer[3] = dHARDWARE_INFO;
	gucBLE_TXBuffer[4] = dHARDWARE_VERSION;
	gucBLE_TXBuffer[5] = dSOFWARE_VERSION;
	gucBLE_TXBuffer[6] = 0x05;
	//	 hardwareInfo.error_code=0x01;      //ok state
	gucBLE_TXBuffer[7] = hardwareInfo.error_code;
	gucBLE_TXBuffer[8] = hardwareInfo.last_vend_status;
	gucBLE_TXBuffer[9] = hardwareInfo.dex_file_present_status;
	//gucBLE_TXBuffer[10] = hardwareInfo.machine_type;
	gucBLE_TXBuffer[10] =0X01;
	
	gucBLE_TXBuffer[11] = MDB_logInfo.MDB_Initial_DataLogByte;      // VMC Setup data
	
	gucBLE_TXBuffer[12] =MDB_logInfo.VMCMAXPrice/256;
	gucBLE_TXBuffer[13] =MDB_logInfo.VMCMAXPrice%256;
	gucBLE_TXBuffer[14] =MDB_logInfo.VMCMINPrice/256;
	gucBLE_TXBuffer[15] =MDB_logInfo.VMCMINPrice%256;
	//gucBLE_TXBuffer[12] =VMC_PRICE_Data[0];				//MaximumPrice(MSB)
	//gucBLE_TXBuffer[13] =VMC_PRICE_Data[1];				//MaximumPrice(LSB)
	//gucBLE_TXBuffer[14] =VMC_PRICE_Data[2];				//MinimumPrice(MSB)
	//gucBLE_TXBuffer[15] =VMC_PRICE_Data[3];				//MinimumPrice(LSB)
	
	gucBLE_TXBuffer[16] =MDB_logInfo.Eport_MDB_SETUP_Data[0];		//EPORT MDB Level
	gucBLE_TXBuffer[17] =MDB_logInfo.Eport_MDB_SETUP_Data[3];		//EPORT Scale factor
	
	//gucBLE_TXBuffer[18] =MDB_logInfo.Eport_MDB_SETUP_Data[2];		//EPORT Country code LSB
	//gucBLE_TXBuffer[19] =MDB_logInfo.Eport_MDB_SETUP_Data[3];		//EPORT Scale factor
	//gucBLE_TXBuffer[20] =MDB_logInfo.Eport_MDB_SETUP_Data[4];		//EPORT Decimal Place
	//gucBLE_TXBuffer[21] =MDB_logInfo.Eport_MDB_SETUP_Data[5];		//EPORT Max Response time
	//gucBLE_TXBuffer[22] =MDB_logInfo.Eport_MDB_SETUP_Data[6];		//EPORT MDB Miscellaneous  byte
	
	for(int tlen=0;tlen<=17;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	
	gucBLE_TXBuffer[18] = tempcehksum;
	gucBLE_TXBuffer[19] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,11);
	
	//Debug_PutString("\r\n Sending HW info :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,13);
	
	guAxpect_Subcode = dHARDWARE_INFO;
	BLE_PutStringL(gucBLE_TXBuffer,20);
}




void Send_Hardware_Info_Debug24()
{
	unsigned int tempcehksum=0;
	
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x10;
	gucBLE_TXBuffer[3] = dHARDWARE_INFO;
	gucBLE_TXBuffer[4] = dHARDWARE_VERSION;
	gucBLE_TXBuffer[5] = dSOFWARE_VERSION;
	gucBLE_TXBuffer[6] = 0x05;
	//	 hardwareInfo.error_code=0x01;      //ok state
	gucBLE_TXBuffer[7] = hardwareInfo.error_code;
	gucBLE_TXBuffer[8] = hardwareInfo.last_vend_status;
	gucBLE_TXBuffer[9] = hardwareInfo.dex_file_present_status;
	//gucBLE_TXBuffer[10] = hardwareInfo.machine_type;
	gucBLE_TXBuffer[10] =0X01;
	
	gucBLE_TXBuffer[11] = MDB_logInfo.MDB_Initial_DataLogByte;      // VMC Setup data
	
	gucBLE_TXBuffer[12] =MDB_logInfo.VMCMAXPrice/256;
	gucBLE_TXBuffer[13] =MDB_logInfo.VMCMAXPrice%256;
	gucBLE_TXBuffer[14] =MDB_logInfo.VMCMINPrice/256;
	gucBLE_TXBuffer[15] =MDB_logInfo.VMCMINPrice%256;
	//gucBLE_TXBuffer[12] =VMC_PRICE_Data[0];				//MaximumPrice(MSB)
	//gucBLE_TXBuffer[13] =VMC_PRICE_Data[1];				//MaximumPrice(LSB)
	//gucBLE_TXBuffer[14] =VMC_PRICE_Data[2];				//MinimumPrice(MSB)
	//gucBLE_TXBuffer[15] =VMC_PRICE_Data[3];				//MinimumPrice(LSB)
	
	gucBLE_TXBuffer[16] =MDB_logInfo.Eport_MDB_SETUP_Data[0];		//EPORT MDB Level
	gucBLE_TXBuffer[17] =MDB_logInfo.Eport_MDB_SETUP_Data[1];		//EPORT Country code MSB
	gucBLE_TXBuffer[18] =MDB_logInfo.Eport_MDB_SETUP_Data[2];		//EPORT Country code LSB
	gucBLE_TXBuffer[19] =MDB_logInfo.Eport_MDB_SETUP_Data[3];		//EPORT Scale factor
	gucBLE_TXBuffer[20] =MDB_logInfo.Eport_MDB_SETUP_Data[4];		//EPORT Decimal Place
	gucBLE_TXBuffer[21] =MDB_logInfo.Eport_MDB_SETUP_Data[5];		//EPORT Max Response time
	gucBLE_TXBuffer[22] =MDB_logInfo.Eport_MDB_SETUP_Data[6];		//EPORT MDB Miscellaneous  byte
	
	for(int tlen=0;tlen<=22;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	
	gucBLE_TXBuffer[23] = tempcehksum;
	gucBLE_TXBuffer[24] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,11);
	
	//Debug_PutString("\r\n Sending HW info :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,13);
	
	guAxpect_Subcode = dHARDWARE_INFO;
	BLE_PutStringL(gucBLE_TXBuffer,25);
}




void Send_VendRequest_ACK_NACK(unsigned char ack)
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_RESPONSE;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x07;
	gucBLE_TXBuffer[3] = dVEND_PRODUCT;
	gucBLE_TXBuffer[4] = gucBLE_RXBuffer[4];
	gucBLE_TXBuffer[5] = gucBLE_RXBuffer[5];
	gucBLE_TXBuffer[6] = gucBLE_RXBuffer[6];
	gucBLE_TXBuffer[7] = gucBLE_RXBuffer[7];
	gucBLE_TXBuffer[8] = ack;
	for(int tlen=0;tlen<=8;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[9] = tempcehksum;
	gucBLE_TXBuffer[10] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,11);
	
		if(ack==0x01)
		{
			//Debug_PutString("\r\n Sending Vend Request ACK :- ");
		}
		else if(ack==0x01)
		{
			//Debug_PutString("\r\n Sending Vend Request NACK :- ");
		}
		//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,11);
	
	//    guAxpect_Subcode = dVEND_PRODUCT;
	BLE_PutStringL(gucBLE_TXBuffer,11);
}

void Send_TAB_TO_PAY_RQST_ACK_NACK(unsigned char ack)
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_RESPONSE;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x03;
	gucBLE_TXBuffer[3] = dTAB_TO_PAY_RQST;
	gucBLE_TXBuffer[4] = ack;
	for(int tlen=0;tlen<=4;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[5] = tempcehksum;
	gucBLE_TXBuffer[6] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,11);
	
	
	BLE_PutStringL(gucBLE_TXBuffer,7);
}

void Send_Money_Creadited_byCreditCard(unsigned char Crdt_Status)
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x0B;
	gucBLE_TXBuffer[3] = dMONY_ACCEPTED;
	
	gucBLE_TXBuffer[4] = gucMony_Acc_Buff[0];
	gucBLE_TXBuffer[5] = gucMony_Acc_Buff[1];
	gucBLE_TXBuffer[6] = gucMony_Acc_Buff[2];
	gucBLE_TXBuffer[7] = gucMony_Acc_Buff[3];
	
	gucBLE_TXBuffer[8] = gucMony_Acc_Buff[4];
	gucBLE_TXBuffer[9] = gucMony_Acc_Buff[5];
	gucBLE_TXBuffer[10] = gucMony_Acc_Buff[6];
	gucBLE_TXBuffer[11] = gucMony_Acc_Buff[7];
	
	gucBLE_TXBuffer[12] = Crdt_Status;
	
	for(int tlen=0;tlen<=12;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[13] = tempcehksum;
	gucBLE_TXBuffer[14] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,11);
	
	guAxpect_Subcode = dMONY_ACCEPTED;
	BLE_PutStringL(gucBLE_TXBuffer,15);
}

void Send_VendSucess()
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x0C;
	gucBLE_TXBuffer[3] = dVEND_SUCESS;
	gucBLE_TXBuffer[4] = vendRequest.compartment[0];
	gucBLE_TXBuffer[5] = vendRequest.compartment[1];
	gucBLE_TXBuffer[6] = vendRequest.compartment[2];
	gucBLE_TXBuffer[7] = vendRequest.compartment[3];
	
	gucBLE_TXBuffer[8] =  MDB_logInfo.MDB_Logbyte1;
	gucBLE_TXBuffer[9] =  MDB_logInfo.MDB_Logbyte2;
	gucBLE_TXBuffer[10] = MDB_logInfo.MDB_Logbyte3;
	gucBLE_TXBuffer[11] = MDB_logInfo.MDB_Logbyte4;
	gucBLE_TXBuffer[12] =MDB_logInfo.VMC_MDB_Compartment_Data[0];
	gucBLE_TXBuffer[13] =MDB_logInfo.VMC_MDB_Compartment_Data[1];
	
	//gucBLE_TXBuffer[8] = dAPP_ACK;
	for(int tlen=0;tlen<=13;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[14] = tempcehksum;
	gucBLE_TXBuffer[15] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,10);
	
	//Debug_PutString("\r\n Sending Vend Success :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,10);
	
	guAxpect_Subcode = dVEND_SUCESS;
	BLE_PutStringL(gucBLE_TXBuffer,16);
}

void Send_MachineReady()
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x03;
	gucBLE_TXBuffer[3] = dMACHINE_READY;
	gucBLE_TXBuffer[4] = dAPP_ACK;
	for(int tlen=0;tlen<=4;tlen++)
	{
		tempcehksum = tempcehksum^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[5] = tempcehksum;
	gucBLE_TXBuffer[6] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,7);
	
	//Debug_PutString("\r\n Sending Machine Ready :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,7);
	
	guAxpect_Subcode = dMACHINE_READY;
	BLE_PutStringL(gucBLE_TXBuffer,7);
}



void Send_VendFail(unsigned char errorname)
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x0D;
	gucBLE_TXBuffer[3] = dVEND_FAIL;
	gucBLE_TXBuffer[4] = vendRequest.compartment[0];
	gucBLE_TXBuffer[5] = vendRequest.compartment[1];
	gucBLE_TXBuffer[6] = vendRequest.compartment[2];
	gucBLE_TXBuffer[7] = vendRequest.compartment[3];
	gucBLE_TXBuffer[8] = errorname;  //vend fail by VMC
	
	gucBLE_TXBuffer[9] = MDB_logInfo.MDB_Logbyte1;
	gucBLE_TXBuffer[10] = MDB_logInfo.MDB_Logbyte2;
	gucBLE_TXBuffer[11] = MDB_logInfo.MDB_Logbyte3;
	gucBLE_TXBuffer[12] = MDB_logInfo.MDB_Logbyte4;
	gucBLE_TXBuffer[13] =MDB_logInfo.VMC_MDB_Compartment_Data[0];
	gucBLE_TXBuffer[14] =MDB_logInfo.VMC_MDB_Compartment_Data[1];
	
	for(int tlen=0;tlen<=14;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[15] = tempcehksum;
	gucBLE_TXBuffer[16] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,10);
	
	//Debug_PutString("\r\n Sending Vend Request Response Fail :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,10);
	
	guAxpect_Subcode = dVEND_FAIL;
	BLE_PutStringL(gucBLE_TXBuffer,17);
}

void Send_VendCancel(unsigned char response)
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x0D;
	gucBLE_TXBuffer[3] = dVEND_CANCEL;
	gucBLE_TXBuffer[4] = vendRequest.compartment[0];
	gucBLE_TXBuffer[5] = vendRequest.compartment[1];
	gucBLE_TXBuffer[6] = vendRequest.compartment[2];
	gucBLE_TXBuffer[7] = vendRequest.compartment[3];
	//gucBLE_TXBuffer[8] = gucVendCancelReasone;  //Price mismatch
	gucBLE_TXBuffer[8] = response;  //Price mismatch
	
	gucBLE_TXBuffer[9] = MDB_logInfo.MDB_Logbyte1;
	gucBLE_TXBuffer[10] = MDB_logInfo.MDB_Logbyte2;
	gucBLE_TXBuffer[11] = MDB_logInfo.MDB_Logbyte3;
	gucBLE_TXBuffer[12] = MDB_logInfo.MDB_Logbyte4;
	gucBLE_TXBuffer[13] =MDB_logInfo.VMC_MDB_Compartment_Data[0];
	gucBLE_TXBuffer[14] =MDB_logInfo.VMC_MDB_Compartment_Data[1];
	
	for(int tlen=0;tlen<=14;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[15] = tempcehksum;
	gucBLE_TXBuffer[16] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,10);
	
	//Debug_PutString("\r\n Sending Vend Request FeedBack :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,10);
	
	guAxpect_Subcode = dVEND_CANCEL;
	BLE_PutStringL(gucBLE_TXBuffer,17);
}


void Send_Parced_DEX_start_Info()
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x04;
	gucBLE_TXBuffer[3] = dPARCED_DEX_DATA;
	gucBLE_TXBuffer[4] = guiCurrentDexPage;
	gucBLE_TXBuffer[5] = guiDex_Data_Total_Page;
	//gucBLE_TXBuffer[8] = dAPP_ACK;
	for(int tlen=0;tlen<=5;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[6] = tempcehksum;
	gucBLE_TXBuffer[7] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,8);
	
	//Debug_PutString("\r\n Sending Parced DEX data Start :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,8);
	
	guAxpect_Subcode = dPARCED_DEX_DATA;
	BLE_PutStringL(gucBLE_TXBuffer,8);
}


void Write_Data_on_BLE_Write(unsigned int guiCurrentDexPage)
{
	//unsigned int tempcehksum=0,tlen=0,arlen=0,strtPoint=0;
	//
	//if(guiCurrentDexPage!=0xFFFF)
	//{
		//gucBLE_TXBuffer[0] = dPROTO_REQUEST;
		//gucBLE_TXBuffer[1] = 0x01;
		//gucBLE_TXBuffer[2] = 0xF6;
		//gucBLE_TXBuffer[3] = dPARCED_DEX_DATA;
		////for(arlen=4,tlen=0;tlen<(guiCurrentDexPage * 500);tlen++,arlen++)
		////{
		////gucBLE_TXBuffer[arlen] = gucCash_SalesData_Buff[tlen];
		////}
		//strtPoint = (guiCurrentDexPage *500)-500;
		////for(arlen=4,tlen=strtPoint;tlen<(500 +strtPoint);tlen++,arlen++)
		//for(arlen=4,tlen=strtPoint;((tlen<(500 +strtPoint)) && (gucCash_SalesData_Buff[tlen]!=0x01));tlen++,arlen++)
		//{
			//gucBLE_TXBuffer[arlen] = gucCash_SalesData_Buff[tlen];
		//}
		////
		////	if(gucCash_SalesData_Buff[tlen]=='#')
		////	{
		////		gucBLE_TXBuffer[arlen++] = '#';
		////	}
//
		//for(tlen=0;tlen<=arlen;tlen++)
		//{
			//tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
		//}
		//gucBLE_TXBuffer[arlen++] = tempcehksum;
		//gucBLE_TXBuffer[arlen++] = 'W';
//
		////Debug_PutString("\r\n Now Writing Page Number:- ");
		////Debug_PutChar((((guiCurrentDexPage)/100))+0x30);
		////Debug_PutChar((((guiCurrentDexPage)/10)%10)+0x30);
		////Debug_PutChar((((guiCurrentDexPage)%10))+0x30);
		////
		////Debug_PutString("\r\n Dex Remaining Pages:- ");
		////Debug_PutChar((((guiDex_Data_Total_Page - guiCurrentDexPage)/100))+0x30);
		////Debug_PutChar((((guiDex_Data_Total_Page - guiCurrentDexPage)/10)%10)+0x30);
		////Debug_PutChar((((guiDex_Data_Total_Page - guiCurrentDexPage)%10))+0x30);
		//gucBLE_TXBuffer[1] = (arlen-4)/256;
		//gucBLE_TXBuffer[2] = (arlen-4)%256;
//
		/////Debug_PutString("\r\n Now Writing Page Number:- ");
		////Debug_PutChar((((arlen)/100))+0x30);
		////Debug_PutChar((((arlen)/10)%10)+0x30);
		////Debug_PutChar((((arlen)%10))+0x30);
		////Debug_PutChar('\r');
		////Debug_PutChar('\n');
//
		////tempcehksum= (gucBLE_TXBuffer[1]);
		////tempcehksum =(tempcehksum<<8) | gucBLE_TXBuffer[2] ;
//
		//BLE_PutStringL(gucBLE_TXBuffer,arlen++);
		////Debug_PutString("\r\n ---------------------------------------------------------------------------------------");
		////Debug_PutString("\r\n tempcehksum:- ");
		////Debug_PutChar((((tempcehksum)/100))+0x30);
		////Debug_PutChar((((tempcehksum)/10)%10)+0x30);
		////Debug_PutChar((((tempcehksum)%10))+0x30);
		////Debug_PutString("\r\n Now Writing Page Number:- ");
		////Debug_PutChar((((arlen)/100))+0x30);
		////Debug_PutChar((((arlen)/10)%10)+0x30);
		////Debug_PutChar((((arlen)%10))+0x30);
		////Debug_PutChar('\r');
		////Debug_PutChar('\n');
		////
		////Debug_PutStringL(gucBLE_TXBuffer,arlen++);
		////Debug_PutString("\r\n ---------------------------------------------------------------------------------------");
	//}
	//else
	//{
		//gucBLE_TXBuffer[0] = dPROTO_REQUEST;
		//gucBLE_TXBuffer[1] = 0x01;
		//gucBLE_TXBuffer[2] = 0xF6;
		//gucBLE_TXBuffer[3] = dPARCED_DEX_DATA;
		//
		//arlen=4;
		//gucBLE_TXBuffer[arlen] = 0;
		//arlen++;
		//gucBLE_TXBuffer[arlen] = 0;
		//arlen++;
		//gucBLE_TXBuffer[arlen] = 0;
		//arlen++;
		//gucBLE_TXBuffer[arlen] = 0;
		//arlen++;
		//for(tlen=0;tlen<=arlen;tlen++)
		//{
			//tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
		//}
		//gucBLE_TXBuffer[arlen++] = tempcehksum;
		//gucBLE_TXBuffer[arlen++] = 'W';
//
		//gucBLE_TXBuffer[1] = (arlen-4)/256;
		//gucBLE_TXBuffer[2] = (arlen-4)%256;
//
		//BLE_PutStringL(gucBLE_TXBuffer,arlen++);
		//
		////
		////Debug_PutStringL(gucBLE_TXBuffer,arlen++);
		////Debug_PutString("\r\n ---------------------------------------------------------------------------------------");
	//}
}



void Send_Parced_Dex_Data_Update()
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x04;
	gucBLE_TXBuffer[3] = dDEX_PAGE_UPDATE;
	gucBLE_TXBuffer[4] = guiCurrentDexPage;
	gucBLE_TXBuffer[5] = guiDex_Data_Total_Page;
	for(int tlen=0;tlen<=5;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[6] = tempcehksum;
	gucBLE_TXBuffer[7] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,8);
	
	//Debug_PutString("\r\n Sending Next Parced Data :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,8);
	
	guAxpect_Subcode = dDEX_PAGE_UPDATE;
	BLE_PutStringL(gucBLE_TXBuffer,8);
}


void Send_Parced_Dex_data_Complete()
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x03;
	gucBLE_TXBuffer[3] = dEND_OF_PARCED_DEX_DATA;
	gucBLE_TXBuffer[4] = guiDex_Data_Total_Page;//guiCurrentDexPage;
	//gucBLE_TXBuffer[5] = guiDex_Data_Total_Page;
	for(int tlen=0;tlen<=4;tlen++)
	{
		tempcehksum = tempcehksum ^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[5] = tempcehksum;
	gucBLE_TXBuffer[6] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,7);
	
	//Debug_PutString("\r\n Sending Parced DEX data Complete :- ");
	//Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,7);
	
	guAxpect_Subcode = dEND_OF_PARCED_DEX_DATA;
	BLE_PutStringL(gucBLE_TXBuffer,7);
}


void send_NACK_onCRC_Mismatch(unsigned char subCode)
{
	
	switch (subCode)
	{
		case dVEND_PRODUCT:
							Send_VendRequest_ACK_NACK(dAPP_NACK);
							break;
		default:
							break;
	}
}

void Send_BLE_Safe_Disconnect()
{
	unsigned int tempcehksum=0;
	gucBLE_TXBuffer[0] = dPROTO_REQUEST;
	gucBLE_TXBuffer[1] = 0x00;
	gucBLE_TXBuffer[2] = 0x03;
	gucBLE_TXBuffer[3] = dSAFE_DISCONNECT;
	gucBLE_TXBuffer[4] = dAPP_ACK;
	for(int tlen=0;tlen<=4;tlen++)
	{
		tempcehksum = tempcehksum^ gucBLE_TXBuffer[tlen];
	}
	gucBLE_TXBuffer[5] = tempcehksum;
	gucBLE_TXBuffer[6] = 'N';
	//BLE_PutStringL(gucBLE_TXBuffer,7);
	
	Debug_PutString("\r\n Sending Machine Ready :- ");
	Debug_CovertChar_in_ASCIIwithLen(gucBLE_TXBuffer,7);
	
	BLE_PutStringL(gucBLE_TXBuffer,7);
	
	
	
	delay_ms(500);
	reset_5Sec_timer();
	
	guAxpect_Subcode = dAPP_CONNECTED;
	
	//Reset_Bluetooth();
}

void Reset_Bluetooth()
{
	//gpio_set_pin_level(BLE_RESET, true);
	//delay_ms(1000);
	//gpio_set_pin_level(BLE_RESET, false);
	//delay_ms(1000);
}
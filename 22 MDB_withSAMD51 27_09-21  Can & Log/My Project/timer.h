/*
 * timer.h
 *
 * Created: 30-Apr-19 1:05:48 PM
 *  Author: User
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#define dTIMER3_ENABLE	(TC3->COUNT16.CTRLA.bit.ENABLE = 1)
#define dTIMER3_DISABLE (TC3->COUNT16.CTRLA.bit.ENABLE = 0)

#define SEC 10

extern unsigned int gucLED_Counter;

extern unsigned int gui5SecCounter,guiSendResponseCounter;
extern unsigned char gucStart5SecTimerFlag,guc5SecIntFlag;

extern unsigned char gucStartRelayPressCounterFlag,gucRelayPressCounter;

extern unsigned int ucTimmer_Credit_Count;
extern unsigned char ucTimmer_Credit_FLG,Send_MonyACPFLG;
extern unsigned int uiEportVendReqstwithoutCredit;
extern unsigned char ucEPORT_NO_CrediteFlg;
extern unsigned int uiDisconnectionwaitCount;

extern unsigned char ucLoopStratFlg;
extern unsigned int ucLoopStratCount;
//extern unsigned char ucSuccesssendbyTimer;
//extern unsigned int uiNormalCardpaymentTimerCount;

extern void init_timer100ms();

extern void send_After_5sec(unsigned char );
extern void reset_5Sec_timer(void);
extern void start_5Sec_timer(void);
#endif /* TIMER_H_ */
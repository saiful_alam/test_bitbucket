/*
 * Cashless_1.c
 *
 * Created: 23-03-2020 11:58:40
 *  Author: Elitebook
 */
//#include "GLOBAL.h"


#include "basic_requirement.h"

//extern volatile eSystemState eNextState ;
//extern volatile eSystemEvent eNewEvent ;


unsigned char ucCashless1_ReadDisableFlg=0,ucCashless1_ReadEnableFlg=0;



unsigned char VNK_Expansion_ID[31]={0x117, 0x000, 'V','N','K','0','0','0','0','0','0','0','0','0','0','0','1', 'V','N','K', '0','0','0','0','0','0','0','3','0','0','1',};

	unsigned char EX_CRC=0;

	unsigned char Poll_state=0;
	unsigned char CRC_Setup_Price=0;
	unsigned int New_RecvPrice=0;
    unsigned char MSB_New_RecvPrice=0,LSB_New_RecvPrice=0;
	uint16_t Cashless1_MDB_Rcv_Buff[40];
	uint8_t  Cashless1_MDB_Rcv_Buff_Index=0,Cashless1_Rcv_Buff_Index_Limit=0,Cashless1_MDB_Rcv_Cmplt_Flg=0;;
	uint8_t  Cashless1_Step_Flg=0;


	unsigned char  E_PORT_Start_SessionFLG=0,ucEportCreditRcv=0,ucE_Port_trn_Stage=0;
  unsigned char Eport_checksum=0;



void Listen_EPORT_AsMaster(void)
{

	if (EportToVmc_Buff_Index>39)
	{
		EportToVmc_Buff_Index=0;
		RCV_TO_Buff_FLG=0;
		Debug_PutString("\r\nEportToVmc_Buff over flow");
	}


if (ucEportToVmc_RcvCompleteFLG)
{

	switch(Cashless1_Step_Flg)
	{


		case 1:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{

						Eport_checksum=0;
						//Debug_PutString("\r\nM_Eport Setup Checksum Match:");

						 if (EportToVmc_Buff_Index==9)
						{

						   Debug_PutString("\r\nM_Eport Setup data:");
						   Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);

						   MDB_logInfo.Eport_MDB_SETUP_Data[0]=EportToVmc_Buff[1];
						   MDB_logInfo.Eport_MDB_SETUP_Data[1]=EportToVmc_Buff[2];
						   MDB_logInfo.Eport_MDB_SETUP_Data[2]=EportToVmc_Buff[3];
						   MDB_logInfo.Eport_MDB_SETUP_Data[3]=EportToVmc_Buff[4];
						   MDB_logInfo.Eport_MDB_SETUP_Data[4]=EportToVmc_Buff[5];
						   MDB_logInfo.Eport_MDB_SETUP_Data[5]=EportToVmc_Buff[6];
						   MDB_logInfo.Eport_MDB_SETUP_Data[6]=EportToVmc_Buff[7];

						   Debug_PutString("\r\nM_Eport Buff Setup data:");
						   Debug_CovertChar_in_ASCIIwithLen(MDB_logInfo.Eport_MDB_SETUP_Data,EportToVmc_Buff_Index);

						}
						else
						{

							// Debug_PutString("\r\nM_E Setup with wrong len:");
						}



					}
					else
					{
						//Debug_PutString("\r\nM_E Setup with wrong Checksum:");
					}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;
		case 2:

					Debug_PutString("\r\nM_E_PORT Display Rqst:");
					Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);


					RCV_TO_Buff_FLG=0;
					Cashless1_Step_Flg=0;
					EportToVmc_Buff_Index=0;

					break;

		case 3:   // Start Session


					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{


						if ((EportToVmc_Buff_Index==4)||(EportToVmc_Buff_Index==11)||(EportToVmc_Buff_Index==18))
						{
							Debug_PutString("\r\nM_E_PORT Strt Session:");
							MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,0);
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
							ucTimmer_Credit_Count=0;
							ucTimmer_Credit_FLG=0;

							if (guStartTaptoPAY_Comm)
							{
								ucEportCreditRcv=1;
								ucE_Port_trn_Stage=1;

								if (!E_PORT_Start_SessionFLG)
								{
									E_PORT_Start_SessionFLG=1;
									E_PORT_CreditStatus=0x01;
									Send_Money_Creadited_byCreditCard(E_PORT_CreditStatus);
									start_5Sec_timer();
									guiSendResponseCounter=0;
									gucRept=dMONY_ACCEPTED;
								}
								Debug_PutString("\r\nM_E_PORT Strt Session T&P");
							}
							else if((!guStartTaptoPAY_Comm))
							{
								gucCardReaderSession_Start=1;
							}

						}
						else
						{
							//Debug_PutString("\r\nM_E Session Strt  with wrong len");

						}


					}
					else
					{
						//Debug_PutString("\r\n\r\nM_E Session Strt  with wrong Checksum");

					}



					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;
					break;

		case 4	:

					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if (EportToVmc_Buff_Index==2)
						{
							Debug_PutString("\r\nM_E_PORT Session Cancel:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
							MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,6);
						}
						else
						{
						 //Debug_PutString("\r\nM_E Session Cancel with wrong len");

						}
					}
					else
					{
					  //Debug_PutString("\r\nM_E Session Cancel with wrong Checksum");

					}
					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;

		case 5:
				Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

				if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
				{
					if ((EportToVmc_Buff_Index==4)||(EportToVmc_Buff_Index==6))
					{
						Debug_PutString("\r\nM_E_PORT  Vend Approved:");
						Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);

						MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,1);
					}
					else
					{

					  //Debug_PutString("\r\nM_E Vend Approved with wrong len");
					}
				}
				else
				{
					//Debug_PutString("\r\nM_E Vend Approved  with wrong Cheksum");
				}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;
					break;

		case 6:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==2))
						{
							Debug_PutString("\r\nM_E_PORT  Vend Denied:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
							MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,2);

						}
						else
						{

							//Debug_PutString("\r\nM_E Vend Denied  with wrong Len");
						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Vend Denied  with wrong Cheksum");
					}



					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;
					break;


		case 7:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==2))
						{
							Debug_PutString("\r\nM_E_PORT Session END:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
							MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,3);


							if (guStartTaptoPAY_Comm)
							{
								ucEportCreditRcv=0;
								ucSendVendResponseToApp=1;
								Debug_PutString("\r\nM_E_PORT Session END T&P");
							}
							else
							{
								gucCardReaderSession_Start=0;     //Comment by Saiful for test
							}

							ucEportCreditRcv=0;
						}
						else
						{
							//Debug_PutString("\r\nM_E Session END  with wrong Len");
						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Session END  with wrong Checksum");

					}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;
					break;

		case 8:

					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==2))
						{
							Debug_PutString("\r\nM_E_PORT  Vend canceled:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
							MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,4);
						}
						else
						{
							//Debug_PutString("\r\nM_E Vend canceled with wrong Len");
						}

					}
					else
					{
						//Debug_PutString("\r\nM_E Vend canceled with wrong Checksum");
					}


					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;

		case 9:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==31)||(EportToVmc_Buff_Index==35))
						{
							Debug_PutString("\r\nM_EPORT Peri ID:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{

							//Debug_PutString("\r\nM_E ID with wrong Len");
						}
					}
					else
					{
						//Debug_PutString("\r\nM_E ID with wrong Checksum:");

					}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					ucEportToVmc_RcvCompleteFLG=0;
					RCV_TO_Buff_FLG=0;

					break;

		case 10:

					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==3))
						{
							Debug_PutString("\r\nM_E_PORT  ERROR Msg:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E Error with wrong Len");
						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Error with wrong Checksum");

					}



					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;
		case 11:

					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==2)||(EportToVmc_Buff_Index==3))
						{
							Debug_PutString("\r\nM_E_PORT CMD out of Seqn:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E CMD out Sq with wrong Len");

						}
					}
					else
					{
						//Debug_PutString("\r\nM_E CMD out Sq with wrong Checksum");
					}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;

		case 12:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==2))
						{
							Debug_PutString("\r\nM_E_PORT Revalue Approved:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E Revalue Approved with wrong Len");

						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Revalue Approved with wrong Checksum");
					}


					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;
		case 13:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==4))
						{
							Debug_PutString("\r\nM_E_PORT Revalue Denied:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E Revalue Limit with wrong Len");
						}
					}
					else
					{

						//Debug_PutString("\r\nM_E Revalue Limit with wrong Checksum");
					}


					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;

		case 14:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==EportToVmc_Buff[2]))
						{
							Debug_PutString("\r\nM_E_PORT Revalue limit");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E Revalue User file with wrong Len");
						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Revalue User file with wrong Checksum");
					}


					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;
					break;


		case 15:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==EportToVmc_Buff[2]))
						{
							Debug_PutString("\r\nM_E_PORT User file data:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E Revalue User file with wrong Len");
						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Revalue User file with wrong Checksum");
					}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;
		case 16:
					Eport_checksum = Cal_EPORT_checksum(EportToVmc_Buff,(EportToVmc_Buff_Index-1));

					if ((Eport_checksum==(unsigned char)EportToVmc_Buff[EportToVmc_Buff_Index-1]))
					{
						if ((EportToVmc_Buff_Index==2))
						{
							Debug_PutString("\r\nM_E_PORT Time & Date Rqst:");
							Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);
						}
						else
						{
							//Debug_PutString("\r\nM_E Revalue Time & Date with wrong Len");

						}
					}
					else
					{
						//Debug_PutString("\r\nM_E Revalue Time & Date with wrong Checksum");

					}

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;

		case 17:
					Debug_PutString("\r\nM_E_PORT Diagnostic Response:");
					Debug_CovertChar_in_ASCIIwithLen(EportToVmc_Buff,EportToVmc_Buff_Index);

					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;

					break;

		default:	Debug_PutString("\r\nM_E_PORT default case");
					EportToVmc_Buff_Index=0;
					Cashless1_Step_Flg=0;
					RCV_TO_Buff_FLG=0;
					break;


	}

	ucEportToVmc_RcvCompleteFLG=0;
}

}









void Enable_Disable_To_Eport (unsigned state)
{

	MDB_Master_PutChar(0X114);     // sending Read disable to the Eport
	MDB_Master_PutChar(state);         //   sending Read disable to the Eport
	MDB_Master_PutChar((0X014+state));     // sending Read disable to the  Eport
}



unsigned char Cal_EPORT_checksum(unsigned char *array1, unsigned int arr_size1)
{
	unsigned char  ret_val1 = 0x00;
	unsigned int iz;
	//__asm("NOP");
	//for (i = 0; i < arr_size; ++i)
	//ret_val += *(array + i);
	for(iz=0;iz<arr_size1;iz++)
	{
	  ret_val1 = ret_val1+array1[iz];

	 // Debug_CovertChar_in_ASCIIwithLen(array1[iz],1);
	}

	//__asm("NOP");
	//Debug_CovertChar_in_ASCII(ret_val1);
	return ret_val1;
}




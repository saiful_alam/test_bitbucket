/*
 * MDB_UART.h
 *
 * Created: 7/7/2021 4:02:01 PM
 *  Author: User
 */ 


#ifndef DEBUG_UART_H_
#define DEBUG_UART_H_


#define PC05 GPIO(GPIO_PORTC, 5)
#define PC13 GPIO(GPIO_PORTC, 13)


extern unsigned char Convert_DataArray[30];

void initilize_Debug_uart(void);
void Debug_PutChar(char data);
void Debug_PutString(char *str);
void Debug_PutStringL(unsigned char *str,unsigned int len);
void Debug_Put_Num(unsigned long Num1,char digit1);

void Debug_CovertChar_in_ASCII(uint8_t data);
void Debug_CovertChar_in_ASCIIwithLen(uint8_t *data,int len);

#endif /* MDB_UART_H_ */
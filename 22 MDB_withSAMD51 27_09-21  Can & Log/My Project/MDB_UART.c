/*
 * MDB_UART.c
 *
 * Created: 7/5/2021 3:04:57 PM
 *  Author: User
 */


#include "basic_requirement.h"



#include "basic_requirement.h"

unsigned char rev_amount_H=0,rev_amount_L=0;
volatile unsigned int guiMDB_RxData=0,guiLastMDB_RxData=0;
unsigned char gucMDB_Step=0;
unsigned char gucMDB_ReaderDisable_Flag=0,gucMDB_ReaderEnable_Flag=0,gucMDB_ReaderCancel_Flag=0;
unsigned int MDB_NEXT_POLL_STATE = 0;
unsigned int guiBLERecPrice=0;
unsigned int guiVMCReceivedPrice,guiVMCReceivedCMP;
unsigned int gucMDB_RecBuff[80];
unsigned int guiMDB_RecBuff_Index=0;




VMC_Config_t vmc_config = {0, 0, 0, 0};
//VMC_Prices_t vmc_prices = {0, 0};

unsigned char gucCancel_Session=0;

unsigned char gucVMC_PER_ID[30];
unsigned char gucVMC_PER_ID_Index=0;


CASHLESS_DEVICE_Config_t CASHLESS_Device_Config_INDIA = {
	0x01, // featureLevel
	0x13,	//country code H
	0x56,	// country code L
	0x64, // Scale Factor
	0x02, // Decimal Places
	0x05, // Max Response Time
	//0x008  // Misc Options
	0x009  // Misc Options
};

CASHLESS_DEVICE_Config_t CASHLESS_Device_Config_US = {

	0x01,   // featureLevel
	0x18,	//country code H
	0x26,	// country code L
	0x01,  // Scale Factor
	0x02,  // Decimal Places
	0x05,  // Max Response Time
	0x00D  // Misc Options
};






void SERCOM5_2_Handler()
{
	unsigned char checksum=0;
	if (REG_SERCOM5_USART_INTFLAG & 4)
	{
		guiMDB_RxData = REG_SERCOM5_USART_DATA;     /* read the receive char and return it */

		switch (gucMDB_Step)
		{
			case 0 :
						switch (guiMDB_RxData)
						{
							case VMC_ACK_A		:
													switch (ACK_Stage_State)
													{
														case ACK_RCV_JUST_RESET:
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				ACK_Stage_State=0;
																				break;
														case ACK_RCV_CONFIGDATA:
																				ACK_Stage_State=0;
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				break;

														case ACK_RCV_PERI_ID:
																				ACK_Stage_State=0;
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				break;

														case ACK_RCV_START_SESSION:
																				gucStartRelayPressCounterFlag=1;
																				gucRelayPressCounter=0;
																				MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,4);  // MDB_Logbyte1 Bit 4
																				if (ucVendProcessRunning)
																				{
																				  Start_Key_Pressing();
																				}
																				
																				ACK_Stage_State=0;
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				ucResPollStage=VMC_POLL_ACK;
																				gucMDB_Step=0;
																				break;
														case ACK_RCV_VEND_APPROVE:
																				ACK_Stage_State=0;
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				ucResPollStage=VMC_POLL_ACK;
																				break;
														case ACK_RCV_END_SESSION:
																				ACK_Stage_State=0;
																				ucPresentMDB_Stage=0;
																				gpio_set_pin_level(DEX_LED,true);
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				ucResPollStage=VMC_POLL_ACK;
																				ucSession_END_ACkRcv=1;  // For  Vend Status send to APP
																				break;
														case ACK_RCV_SESSION_CANCEL:
																				ACK_Stage_State=0;
																				gucMDB_Step=0;
																				gpio_set_pin_level(DEX_LED,true);
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				ucResPollStage=VMC_POLL_ACK;
																				break;
														case ACK_RCV_VEND_DENIED:
																				ACK_Stage_State=0;
																				MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																				ucResPollStage=VMC_POLL_ACK;
																				gucMDB_Step=0;
																				break;

														default:			break;
													}

													break;
							case VMC_RESET_M     :

													break;
							case VMC_RESET		:
													if (guiLastMDB_RxData==VMC_RESET_M)
													{
														MDB_Response_Stage=RES_RESET_M;
														Debug_PutString("\r\n VMC_45");
														ucRESET_RecvFLG=1;
													}
													
													break;
							case VMC_SETUP     :
													guiMDB_RecBuff_Index=0;
													gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_SETUP;
													gucMDB_Step=1;
													// Debug_PutString("\r\n VMC_123");
													break;
							case VMC_POLL_M    :
													//Debug_PutString("\r\n VMC_POLL_M");
													break;
							case VMC_POLL      :
												//Debug_PutString("\r\n VMC_POLL");
												if (ucRESET_RecvFLG)
												{

													switch(MDB_NEXT_POLL_STATE)
													{
														case VMC_POLL_ACK           :   
																						//Debug_PutString("\r\n VMC_POLL12");
																						MDB_Response_Stage=RES_POLL_ACK;
																						break;

														case JUST_RESET             :
														                                //Debug_PutString("\r\n VMC_123");
																						MDB_Response_Stage=RES_JUSTRESET;
																						break;
														default						:
																						break;
													}

												}
												else
												{
													//
													if (ucInitialMDBCommError)
													{
														MDB_PutChar(0x000);
														MDB_PutChar(VMC_ACK);
														Debug_PutString("\r\nPOlll");
													}
												 	
													
												}
												
												break;
							case VMC_VEND      :
												guiMDB_RecBuff_Index=0;
												gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_VEND;
												gucMDB_Step=4;
												gpio_set_pin_level(BLE_Status_LED,0);
												break;
							case VMC_READER    :
												guiMDB_RecBuff_Index=0;
												gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_READER;
												gucMDB_Step=3;
												break;
							case VMC_REVALUE:  
												guiMDB_RecBuff_Index=0;
												gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_REVALUE;
												gucMDB_Step=12;
											    break;					
												
												
							case VMC_EXPANSION :
												guiMDB_RecBuff_Index=0;
												gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_EXPANSION;
												gucMDB_Step=2;
												break;
												
							//case PERIPHERAL_ID_SLV:
												//gucMDB_Step=11;
												//gucVMC_PER_ID_Index=0;
												//gucVMC_PER_ID[gucVMC_PER_ID_Index++]=PERIPHERAL_ID_SLV;
												//break;
												
							//case 0x10C:  
											//Debug_PutString("\r\n Coin data");
											//break;	
							//case 0x134:
										    //Debug_PutString("\r\n Note data");
										     //break;								
												
												
							default			   :
												break;
						}
						break;
			case 1:
						//collecting the VMC SETP & VMC PRICE Data
						//Debug_PutString("\r\n vcase1");
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							
							MDB_Response_Stage=RES_CONFIGSETUP;
							//gucMDB_Step=0;
						}
						break;
			case 2:
						//collecting the VMC EXPANSION data
						//Debug_PutString("\r\n vcase2");
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>31)
						{
							
							MDB_Response_Stage=RES_PERI_ID;

							//gucMDB_Step=0;
						}
						break;
			case 3:
						//Collecting the VMC READER data
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=3)
						{
							
							MDB_Response_Stage=RES_READER;
							
						}
						break;
			case 4:
						//Vend Request Data
						//Debug_PutString("\r\n vcase4");
						switch(guiMDB_RxData)
						{
							case 0x00	:
										//vend request
										//Debug_PutString("\r\n vend2 request");
										gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
										gucMDB_Step=5;
										break;
							case 0x01	:
										//vend cancel
										gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
										gucMDB_Step=6;
										break;
							case  0x02	:
										//vend success
										//Debug_PutString("\r\n vend Succ int1");
										gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
										gucMDB_Step=7;
										break;
							case 0x03	:
										//vend fail
										gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
										gucMDB_Step=8;
										break;
							case 0x04	:
										//ssn complete
										gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
										gucMDB_Step=9;
										//gucRec_VMCSessionComplete_Flag=0;
										break;
							case 0x05	:
										//Cash Vend
										gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
										gucMDB_Step=10;
										break;
							default:
										break;
						}
						break;
			case 5:
						//Vend Request by VMC
						//Debug_PutString("\r\n vcase5");
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							
							MDB_Response_Stage=RES_VENDRQST;
							//gucMDB_Step=0;
						}
						break;
			case 6:
						//vend cancel
						//Debug_PutString("\r\ncase6");
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=3)
						{
							MDB_Response_Stage=RES_VEND_CANCEL;

							gpio_set_pin_level(BLE_Status_LED,1);

						}
						break;
			case 7:
						//vend success
						//Debug_PutString("\r\n case7");
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=5)
						{
							//Debug_PutString("\r\n sscase7");
							MDB_Response_Stage=RES_VNDSUCCESS;
							//gucMDB_Step=0;
						}
						break;
			case 8:
						//vend fail
						
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=3)
						{
							MDB_Response_Stage=RES_VEND_FAIL_SLAVE;
							//gucMDB_Step=0;
						}
						break;

						
			case 9:
						//ssn complete
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						//if((guiMDB_RecBuff_Index>=2)&&(!gucRec_VMCSessionComplete_Flag))
						if((guiMDB_RecBuff_Index>=2))
						{
							//gucRec_VMCSessionComplete_Flag=1;
							MDB_Response_Stage=RES_SESSIONCOMPLETE;

							//gucMDB_Step=0;
						}
						break;
			case 10:
						//vend using coin
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							//Debug_PutString("\r\n Count:");
							//Debug_Put_Num(guiMDB_RecBuff_Index,6);
							MDB_Response_Stage=RES_CASH_VEND_SLAVE;
							//gucMDB_Step=0;
						}
						break;

			case 11:
						gucVMC_PER_ID[gucVMC_PER_ID_Index++]=guiMDB_RxData;
						if (gucVMC_PER_ID_Index>25)
						{
							//Send_Acknowledgement();
							gucVMC_PER_ID_Index=0;
							gucMDB_Step=0;
						}
						break;
			case 12: 	
						switch(guiMDB_RxData)
						{
							case 0x00	:
											//Revalue request
											//Debug_PutString("\r\n vend2 request");
											gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
											gucMDB_Step=13;
											MDB_logInfo.MDB_Logbyte2=Set_Bit(MDB_logInfo.MDB_Logbyte2,6);  // MDB_Logbyte2 Bit 5
											break;
							case 0x01	:
											//Revalue Limit
											gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
											MDB_logInfo.MDB_Logbyte2=Set_Bit(MDB_logInfo.MDB_Logbyte2,6);  // MDB_Logbyte2 Bit 5
											guiMDB_RecBuff_Index=0;
											gucMDB_Step=0;
											break;
						}
						break;
						
			case 13:  	
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{

							MDB_Response_Stage=RES_REVALUE_RQST;
							//gucMDB_Step=0;
						}
						break;					
						
			default:
					break;
		}

		guiLastMDB_RxData = guiMDB_RxData;
		REG_SERCOM5_USART_INTFLAG = 4;  /* clear interrupt flag */
	}
}


void initilize_MDB_uart(void)
{

	 //GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_SERCOM5_CORE;      // SERCOM0 peripheral channel
	 //GCLK->CLKCTRL.reg |= GCLK_CLKCTRL_GEN_GCLK0;           // select source GCLK_GEN[1]
	 //GCLK->CLKCTRL.bit.CLKEN = 1;                           // enable generic clock
//
	 //PM->APBCSEL.bit.APBCDIV = 0;                           // no pre scaler
	 //PM->APBCMASK.bit.SERCOM5_ = 1;                         // enable SERCOM0 interface

	 GCLK->PCHCTRL[35].reg=  0x40;		//selecting the main core clock for SERCOM5
	 GCLK->PCHCTRL[3].reg=  0x43;		//selecting slow clock for SERCOM5
	 MCLK->APBDMASK.reg |= MCLK_APBDMASK_SERCOM5;


    REG_SERCOM5_USART_CTRLA |= 1;               /* reset SERCOM1 */
    while (REG_SERCOM5_USART_SYNCBUSY & 1) {}   /* wait for reset to complete */
    REG_SERCOM5_USART_CTRLA = 0x40106004;       /* LSB first, async, no parity,
        PAD[0]-Tx, BAUD uses fraction, 8x oversampling, internal clock */
    REG_SERCOM5_USART_CTRLB = 0x00030001;       /* enable Tx/Rx, one stop bit, 9 bit */
    //REG_SERCOM5_USART_BAUD  = 104;               /* 8000000/8/115200 = 8.68 */
	REG_SERCOM5_USART_BAUD  = (((float)dXTAL_FREQUENCY/8)/dMDB_BAUDRATE);               /* 8000000/8/115200 = 8.68 */
    REG_SERCOM5_USART_CTRLA |= 2;               /* enable SERCOM1 */
    while (REG_SERCOM5_USART_SYNCBUSY & 2) {}   /* wait for enable to complete */

	gpio_set_pin_function(PB16, PINMUX_PB16C_SERCOM5_PAD0);
	gpio_set_pin_function(PB17, PINMUX_PB17C_SERCOM5_PAD1);

	REG_SERCOM5_USART_INTENSET = 4;              /* enable receive complete interrupt */
}

void MDB_PutChar(unsigned int data)
{
    while(!(REG_SERCOM5_USART_INTFLAG & 1)) {}  /* wait for data register empty */
    REG_SERCOM5_USART_DATA = data;              /* send a char */
}

void MDB_PutString(unsigned char *str)
{
	unsigned int i=0;
	while(str[i]!='\0')                 //check for NULL character to terminate loop
	{
		while(!(REG_SERCOM5_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM5_USART_DATA = str[i];              /* send a char */
		i++;
	}
}

void MDB_PutStringL(unsigned char *str,unsigned int len)
{
	unsigned int i=0;
	while(len>0)
	{
		while(!(REG_SERCOM5_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM5_USART_DATA = str[i];              /* send a char */
		i++;
		len--;
	}
}




void Send_Cashless_Device_Configuration(void)
{
	unsigned int checksum=0;
	// calculate checksum, no Mode bit yet
	checksum = ( READER_CONFIG_INFO
	+ CASHLESS_Device_Config_US.featureLevel
	+ CASHLESS_Device_Config_US.countryCodeH
	+ CASHLESS_Device_Config_US.countryCodeL
	+ CASHLESS_Device_Config_US.scaleFactor
	+ CASHLESS_Device_Config_US.decimalPlaces
	+ CASHLESS_Device_Config_US.maxResponseTime
	+ CASHLESS_Device_Config_US.miscOptions );

	delay_ms(1);	delay_us(800);
	MDB_PutChar(READER_CONFIG_INFO);
	MDB_PutChar(CASHLESS_Device_Config_US.featureLevel);
	MDB_PutChar(CASHLESS_Device_Config_US.countryCodeH);
	MDB_PutChar(CASHLESS_Device_Config_US.countryCodeL);
	MDB_PutChar(CASHLESS_Device_Config_US.scaleFactor);
	MDB_PutChar(CASHLESS_Device_Config_US.decimalPlaces);
	MDB_PutChar(CASHLESS_Device_Config_US.maxResponseTime);
	MDB_PutChar(CASHLESS_Device_Config_US.miscOptions);
	MDB_PutChar(checksum | VMC_ACK);

	//Debug_PutString("\r\n Send the MDB Slave Setup Configuration");
}

void Send_Acknowledgement(void)
{
	delay_ms(1);	delay_us(800);
	MDB_PutChar(VMC_ACK);
}

void Send_Just_Reset(void)
{

	MDB_PutChar(JUST_RESET);
	MDB_PutChar(VMC_ACK);
	//Debug_PutString("\r\n Send Just Reset");
}


void Peripheral_ID_Send(void)
{
	
	delay_ms(1); delay_us(800);
	MDB_PutChar(0x009);   //Z1
	MDB_PutChar(0x056);   // Manufacturer 1st(Z2) code :V
	MDB_PutChar(0x04E);   // Manufacturer 2nd(Z3) code :N
	MDB_PutChar(0x04B);   // Manufacturer 3rd(Z4) code :K
	MDB_PutChar(0x030);   // Serial No Z5
	MDB_PutChar(0x030);   // Serial No Z6
	MDB_PutChar(0x030);   // Serial No Z7
	MDB_PutChar(0x030);   // Serial No Z8
	MDB_PutChar(0x030);   // Serial No Z9
	MDB_PutChar(0x030);   // Serial No Z10
	MDB_PutChar(0x031);   // Serial No Z11
	MDB_PutChar(0x030);   // Serial No Z12
	MDB_PutChar(0x032);   // Serial No Z13
	MDB_PutChar(0x030);   // Serial No Z14
	MDB_PutChar(0x032);   // Serial No Z15
	MDB_PutChar(0x031);   // Serial No Z16
	MDB_PutChar(0x056);   // Model  No Z17
	MDB_PutChar(0x045);   // Model  No Z18
	MDB_PutChar(0x04E);   // Model  No Z19
	MDB_PutChar(0x044);   // Model  No Z20
	MDB_PutChar(0x045);   // Model  No Z21
	MDB_PutChar(0x04B);   // Model  No Z22
	MDB_PutChar(0x049);   // Model  No Z23
	MDB_PutChar(0x04E);   // Model  No Z24
	MDB_PutChar(0x02D);   // Model  No Z25
	MDB_PutChar(0x030);   // Model  No Z26
	MDB_PutChar(0x037);   // Model  No Z27
	MDB_PutChar(0x031);   // Model  No Z28
	MDB_PutChar(0x030);   // Software vrn Z29
	MDB_PutChar(0x031);   // Software vrn Z30

	MDB_PutChar(0x1B8);   // CRC
	

	//Debug_PutString("\r\n Sending Peripheral ID");

}

void Approved_Vend(void)
{
	MDB_PutChar(0x005);
	MDB_PutChar(rev_amount_H);
	MDB_PutChar(rev_amount_L);
	MDB_PutChar( VMC_ACK | (0x05+rev_amount_H+rev_amount_L) );
	Debug_PutString("\r\nSend Vend Approved");
}

void Denied_Vend(void)
{
	delay_ms(1);	delay_us(800);
	MDB_PutChar(0x006);
	MDB_PutChar(0x106);
	Debug_PutString("\r\nSend Vend denied");
}


void Cancel_Session(void)
{
	MDB_PutChar(0x004);
	MDB_PutChar(0x104);
	Debug_PutString("\r\nSend Session Cancel Request");
}

void Vend_Cancelled(void)
{
	MDB_PutChar(0x008);
	MDB_PutChar(0x108);

	Debug_PutString("\r\nSend VMC Read Cancel Response ");
}

void Start_Session(void)
{
	


	MDB_PutChar(0x003);
	MDB_PutChar(rev_amount_H);
	MDB_PutChar(rev_amount_L);
	MDB_PutChar( 0x100 | (0x03+rev_amount_H+rev_amount_L) );

	Debug_PutString("\r\nSend MDB Session Started");
}


void End_Session(void)
{
	Debug_PutString("\r\nSend MDB END Session ");
	MDB_PutChar(0x007);
	MDB_PutChar(0x107);
	gucStartRelayPressCounterFlag=0;
	gucRelayPressCounter=0;

}

/*
  * calc_checksum()
  * Calculates checksum of *array from 0 to arr_size
  * Use with caution (because of pointer arithmetics)
  */
unsigned int calc_checksum(unsigned int *array, unsigned int arr_size)
{
	unsigned int ret_val = 0x00;
	unsigned int i;
	__asm("NOP");
	//for (i = 0; i < arr_size; ++i)
	//ret_val += *(array + i);
	for(i=0;i<arr_size;i++)
	ret_val += array[i];
	__asm("NOP");
	return ret_val;
}

/*
 * MDB_UART.h
 *
 * Created: 7/7/2021 4:02:01 PM
 *  Author: User
 */ 


#ifndef KEYPAD_H_
#define KEYPAD_H_


#define  RELAY_LED		GPIO(GPIO_PORTC, 01)

#define  KEY1		GPIO(GPIO_PORTA, 15)
#define  KEY2		GPIO(GPIO_PORTA, 14)
#define  KEY3		GPIO(GPIO_PORTC, 15)
#define  KEY4		GPIO(GPIO_PORTC, 14)
#define  KEY5		GPIO(GPIO_PORTC, 11)
#define  KEY6		GPIO(GPIO_PORTC, 10)
#define  KEY7		GPIO(GPIO_PORTB, 15)

#define  KEY8		GPIO(GPIO_PORTB, 14)
#define  KEY9		GPIO(GPIO_PORTB, 13)
#define  KEY10		GPIO(GPIO_PORTB, 12)
#define  KEY11		GPIO(GPIO_PORTB, 11)
#define  KEY12		GPIO(GPIO_PORTB, 10)
#define  KEY13		GPIO(GPIO_PORTA, 11)
#define  KEY14		GPIO(GPIO_PORTA, 10)

#define  KEY15		GPIO(GPIO_PORTC, 7)
#define  KEY16		GPIO(GPIO_PORTC, 6)
#define  KEY17		GPIO(GPIO_PORTA, 7)
#define  KEY18		GPIO(GPIO_PORTA, 6)
#define  KEY19		GPIO(GPIO_PORTA, 5)
#define  KEY20		GPIO(GPIO_PORTA, 4)
#define  KEY21		GPIO(GPIO_PORTB, 7)

#define  KEY22		GPIO(GPIO_PORTB, 6)
#define  KEY23		GPIO(GPIO_PORTB, 5)
#define  KEY24		GPIO(GPIO_PORTB, 4)
#define  KEY25		GPIO(GPIO_PORTA, 3)
#define  KEY26		GPIO(GPIO_PORTA, 2)
#define  KEY27		GPIO(GPIO_PORTC, 3)
#define  KEY28		GPIO(GPIO_PORTC, 2)

#define  R1C1_RELAY		KEY7
#define  R2C1_RELAY		KEY6
#define  R3C1_RELAY		KEY5
#define  R4C1_RELAY		KEY4
#define  R5C1_RELAY		KEY3
#define  R6C1_RELAY		KEY2
#define  R7C1_RELAY		KEY1

#define  R1C2_RELAY		KEY14
#define  R2C2_RELAY		KEY13
#define  R3C2_RELAY		KEY12
#define  R4C2_RELAY		KEY11
#define  R5C2_RELAY		KEY10
#define  R6C2_RELAY		KEY9
#define  R7C2_RELAY		KEY8

#define  R1C3_RELAY		KEY21
#define  R2C3_RELAY		KEY20
#define  R3C3_RELAY		KEY19
#define  R4C3_RELAY		KEY18
#define  R5C3_RELAY		KEY17
#define  R6C3_RELAY		KEY16
#define  R7C3_RELAY		KEY15

#define  R1C4_RELAY		KEY28
#define  R2C4_RELAY		KEY27
#define  R3C4_RELAY		KEY26
#define  R4C4_RELAY		KEY25
#define  R5C4_RELAY		KEY24
#define  R6C4_RELAY		KEY23
#define  R7C4_RELAY		KEY22



extern const  unsigned char Key_Matrix[7][4];
extern unsigned char ucMatched_Row, ucMatched_Col;
extern unsigned char ucKeyPressMask_Flg,ucKey_DelayTimerSRT_FLG,ucKeyEND_FLG;
extern unsigned int uiKey_DelayTimerCount;
extern volatile unsigned char ucKey_Sqnce_Number;

void Start_Key_Pressing(void);
void Clear_Key_Pressing(void);
void Press_Keys(unsigned char Key_posi_Num);
void Compare_2D_Array( unsigned char value);
void Pressed_Matrix_KeyON(unsigned char Row,unsigned char col);
void Pressed_Matrix_KeyOFF(unsigned char Row,unsigned char col);


extern unsigned char gucRx_Product_Code[5];



void motor_delay_ON();
void motor_delay_OFF();
unsigned int compare_strings(const unsigned char a[], unsigned char b[]);
void PressMachineKey(unsigned char gucMCMotorNumber);

void BEVMAX1_PressMachineKey(unsigned char gucMCMotorNumber);
void SEAGA_MachineKey(unsigned char gucMCMotorNumber);




void KeyPAD_INIT(void);

#endif /* MDB_UART_H_ */
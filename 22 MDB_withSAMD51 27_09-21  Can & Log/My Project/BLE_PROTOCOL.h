/*
 * BLE_PROTOCOL.h
 *
 * Created: 15-07-2021 15:19:37
 *  Author: USer
 */


#ifndef BLE_PROTOCOL_H_
#define BLE_PROTOCOL_H_

#define     MACHINE_STATUS_OK						 0x01
#define     MACHINE_STATUS_MDBERR					 0x02
#define     MACHINE_STATUS_DEXERR					 0x03

#define     MACHINE_STATUS_EPORT_CREDIT				 0x05
#define     MACHINE_STATUS_EPORTACTIVE				 0x06
#define     MACHINE_STATUS_EPORT_Tra_NOTCOMPLETED	 0x07
#define     MACHINE_STATUS_POWRON_MDBERR			 0x08


extern unsigned int gfinPaisa,gfinRs,gfBLEPrice;
extern unsigned int guiPriceinPaise;
extern unsigned char gucCRCMatched;
extern unsigned char guc_HW_INfoSnd_OnceFLG;
extern unsigned char E_PORT_CreditStatus;
extern unsigned char ucVendProcessRunning;
extern unsigned char uc_ReadyToCommunicatewithApp;
extern unsigned char ucVendsession4mApp,uchwinfoAckRecved;
extern unsigned char gucAPPConnectedwithoutError;


//extern unsigned char ucTapNpayVendRqstFlg;
//extern unsigned int ucTapNpayVendRqstFTimerCount;


void BLE_CONNECT(void);
void BLE_Communication(void);
unsigned char Verify_Checksum(unsigned char *c,int len);
#endif /* BLE_PROTOCOL_H_ */
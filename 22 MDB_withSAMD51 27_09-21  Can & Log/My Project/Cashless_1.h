/*
 * Cashless_1.c
 *
 * Created: 23-03-2020 11:59:40
 *  Author: Elitebook
 */ 


#ifndef CASHLESS_1_H_
#define CASHLESS_1_H_

#define  CASHLESS_ACK                  0x100

#define  READER_CONFIG_DATA_MASTER      0x001
#define  DISPLAY_RQST_MASTER			0x002
#define  BEGIN_SESSION_MASTER           0x003
#define  SESSION_CAN_RQST_CL_MASTER     0x004
#define  VEND_APPROVED_MASTER		    0x005
#define  VEND_DENIED_MASTER		        0x006
#define  END_SESSION_MASTER 		    0x007
#define  VEND_CANCELLED_CL_MASTER       0x008
#define  PERIPHERALID_MASTER            0x009
#define  ERROR_MASTER					0x00A
#define  CMDOUTOFSEQNC_MASTER           0x00B
#define  REVALUE_APPROVED_MASTER        0x00D
#define  REVALUE_DENIED_MASTER			0x00E
#define  REVALUE_LIMIT_MASTER			0x00F
#define  USER_FILEDATA_MASTER			0x010
#define  TIMEDATE_RQST_MASTER			0x011
#define  DATAENTRYRQST_MASTER			0x012
#define  USER_FILEDATA_MASTER			0x010
#define  DIAGNOSTICRESPONCE_MASTER		0x0FF



//#define  POLL_STATE        1
//#define  VEND_STATE        2


#define interdataDelay  900


extern unsigned char ucCashless1_ReadDisableFlg,ucCashless1_ReadEnableFlg;

extern uint16_t Cashless1_MDB_Rcv_Buff[40];
extern uint8_t Cashless1_MDB_Rcv_Buff_Index,Cashless1_Rcv_Buff_Index_Limit,Cashless1_MDB_Rcv_Cmplt_Flg;


extern unsigned char Poll_state;
extern unsigned char CRC_Setup_Price;
extern unsigned int New_RecvPrice;
extern unsigned char MSB_New_RecvPrice,LSB_New_RecvPrice;

extern uint8_t Cashless1_Step_Flg;
  extern unsigned char Eport_checksum;


extern unsigned char  E_PORT_Start_SessionFLG,ucEportCreditRcv,ucE_Port_trn_Stage;

//unsigned char Cashless_MDB_State=0;
void Cashless_1_Communication(void);
void Listen_EPORT_AsMaster(void);
void Enable_Disable_To_Eport (unsigned state);
unsigned char Cal_EPORT_checksum(unsigned char *array1, unsigned int arr_size1);


typedef struct
{
	char	Cashless1_DeviceConfig_Buff[10];   // 09H  changer setup information
	char	Cashless1_RCV_Price_Buff[5];   // 09H  changer setup information
	uint16_t UInt_Cashless1_RCV_Price;
	uint8_t  uc_Cashless1_VMC_Price_MSB;
	uint8_t  uc_Cashless1_VMC_Price_LSB;
	uint8_t  uc_Cashless1_VMC_Comprt_MSB;
	uint8_t  uc_Cashless1_VMC_Comprt_LSB;
	
}Cashless1_RecvData;


//typedef struct
//{
	//uint16_t featureLevel;
	//uint16_t countryCodeH;
	//uint16_t countryCodeL;
	//uint16_t scaleFactor;
	//uint16_t decimalPlaces;
	//uint16_t maxResponseTime; // seconds, overrides default NON-RESPONSE time
	//uint16_t miscOptions;
//} VMC_Expansion_data;




#endif /* CASHLESS_1_H_ */
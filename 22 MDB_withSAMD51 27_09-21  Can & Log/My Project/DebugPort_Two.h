/*
 * DebugPort_Two.h
 *
 * Created: 21-09-2021 16:04:26
 *  Author: USer
 */ 


#ifndef DEBUGPORT_TWO_H_
#define DEBUGPORT_TWO_H_

#define PB20 GPIO(GPIO_PORTB, 20)
#define PC12 GPIO(GPIO_PORTC, 12)

void initilize_Debug_Two_uart(void) ;
void Debug_Two_PutChar(char data);
void Debug_Two_PutString(char *str);

#endif /* DEBUGPORT_TWO_H_ */
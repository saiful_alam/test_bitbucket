/*
 * MDB_For_Main.c
 *
 * Created: 28-06-2021 13:15:27
 *  Author: USer
 */

#include "basic_requirement.h"



////****** New Variables
unsigned char ACK_Stage_State=0;
unsigned char MDB_Response_Stage=0;
unsigned char VMC_Configur_checksum=0;
unsigned char ucPeriID_checksum=0;
unsigned char ucResPollStage=0;
unsigned char ucSession_END_ACkRcv=0;
unsigned char ucRESET_RecvFLG=0;
unsigned char uc_CreditDoneFlg=0;
unsigned char Vend_statusToApp=0;
unsigned char ucSendVendResponseToApp=0;
unsigned int uiBLETransuctionCount=0;
unsigned char gucAmountCreditedPressKey=0;

unsigned char  ucPresentMDB_Stage=0;
unsigned char  uc_SudddenDisconnectFlg=0,E_PORT_CreditExpiredFlg=0,ucE_Port_CreditExpireStatustoApp;
unsigned char uc_VndReqstTimerFLG=0,ucVendingTimerExpiredFlg=0;
unsigned int ui_VndReqstTimerFCount=0;
unsigned char ucENDsessionCount=0;
unsigned int Eportcreditwaitcount=0;
unsigned char EportcreditwaitFlg=0,EportcreditwaitTimer_ExpiredFlg=0;

unsigned char Vend_Fail_ErrorName=0;

//unsigned char Eport_MDB_SETUP_Data[8];
//unsigned char VMC_PRICE_Data[5];


//unsigned char MDB_Initial_DataLogByte=0;

MDBLogByte MDB_logInfo;




//unsigned char ucVendRqstfrmAppFLG=0;

void MDB_Communication(void)
{
	unsigned int checksum=0;



	if((ucEportVend_KeyPress)&&(E_PORT_Start_SessionFLG)&&(ucEportCreditRcv))
	{
         
			EportcreditwaitFlg=1;
			
			if (EportcreditwaitTimer_ExpiredFlg)
			{
				 Start_Key_Pressing();
				 ucEportVend_KeyPress=0;
				 EportcreditwaitTimer_ExpiredFlg=0;
				 EportcreditwaitFlg=0;
				 Debug_PutString("\r\nEportcredit wait Complete");
				 
			}

		//Eportcreditwaitcount++;
		//if (Eportcreditwaitcount>1000)
		//{
			////Debug_PutString("\r\n Eportcreditwaitcount");
			 //Eportcreditwaitcount=0;
			 //Start_Key_Pressing();
			 //ucEportVend_KeyPress=0;
		//}


	   //Debug_PutString("\r\nucEportCreditRcv");
	   ucE_Port_trn_Stage=3;
	}

	else if ((ucEPORT_NO_CrediteFlg)&&(!ucEportCreditRcv)&&(ucEportVend_KeyPress))
	{
		 ucEportVend_KeyPress=0;
		 ucEPORT_NO_CrediteFlg=0;

		 ucVendProcessRunning=0;
		 gucVendCancelReasone=No_creditFromEPORT;
		 ucSendVendResponseToApp=1;
		 Vend_statusToApp=TOAPP_VND_CANCEL;

		 Debug_PutString("\r\nucEPORT_NO_CrediteFlg");


	}


	//else if((ucEportVend_KeyPress)&&(E_PORT_Start_SessionFLG)&&(!ucEportCreditRcv))
	//{
		 //ucEportVend_KeyPress=0;
		//ucE_Port_trn_Stage=2;
		//Debug_PutString("\r\n No credit for Eport");
	//}


		if(gucAmountCreditedPressKey)
		{
			 Press_Keys(ucKey_Sqnce_Number);

			 MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,5);  // MDB_Logbyte1 Bit 5
			 if (ucKeyEND_FLG)
			 {
				 ucKeyEND_FLG=0;
				 Clear_Key_Pressing();
			 }
		}

		else if(gucCancel_Session)
		{

			gucCancel_Session=0;
		    Debug_PutString("\r\nCanceling Session by VNK Device");
			ucResPollStage=SESSION_CANCEL_REQUEST;
			gucVendCancelReasone=VendCancelByTimeout;
			gucMDB_Step=0;
			
			
		}

if (ucVendingTimerExpiredFlg)
{
	ucVendingTimerExpiredFlg=0;
	Debug_PutString("\r\Vending Timer Expired");
	ucSendVendResponseToApp=1;
	Vend_statusToApp=TOAPP_VND_FAIL;
	
	Vend_Fail_ErrorName=VendfailByTimeout ;
	hardwareInfo.last_vend_status = 0x02;//last vend status as Fail
	
}

if (ucE_Port_CreditExpireStatustoApp)
{
	ucE_Port_CreditExpireStatustoApp=0;
	Debug_PutString("\r\nE_PORT_CreditExpired");
	Vend_statusToApp=TOAPP_VND_CANCEL;
	gucVendCancelReasone=EportVCrediteExpired ;
	hardwareInfo.last_vend_status = 0x02;//last vend status as Fail
	ucSendVendResponseToApp=1;
}



   if (ucSendVendResponseToApp)
   {


	   switch (Vend_statusToApp)
	   {
		   case TOAPP_VND_SUCCESS:
									Debug_PutString("\r\nSuccess Send To APP");
									Send_VendSucess();
									start_5Sec_timer();
									guiSendResponseCounter=0;
									gucRept=dVEND_SUCESS;
									Vend_statusToApp=0;
									ui_VndReqstTimerFCount=0;
									uc_VndReqstTimerFLG=0;
									


									break;
		 case TOAPP_VND_CANCEL:
									Debug_PutString("\r\nCancel Send To APP");
									 Send_VendCancel(gucVendCancelReasone);
									 start_5Sec_timer();
									 guiSendResponseCounter=0;
									 gucRept=dVEND_CANCEL;
									 Vend_statusToApp=0;
									 ui_VndReqstTimerFCount=0;
									 uc_VndReqstTimerFLG=0;
									 break;
		 case TOAPP_VND_FAIL:
									 Debug_PutString("\r\nVend Fail  To APP");
									 Send_VendFail(Vend_Fail_ErrorName);
									 start_5Sec_timer();
									 guiSendResponseCounter=0;
									 gucRept=dVEND_FAIL;
									 Vend_statusToApp=0;
									 ui_VndReqstTimerFCount=0;
									 uc_VndReqstTimerFLG=0;
									 break;


				default: break;

	   }
	   
	   ucSendVendResponseToApp=0;
	   
	   MDB_logInfo.MDB_Logbyte4=MDB_logInfo.MDB_Logbyte4 & 0XFE;
	   MDB_logInfo.MDB_Logbyte3=MDB_logInfo.MDB_Logbyte3 & 0XFE;
	    Debug_PutString("\r\nClearing bit0 of Logbyte4");
   }




	switch (MDB_Response_Stage)
	{
		case RES_RESET_M:

									 Send_Acknowledgement();
									 delay_ms(10);
									 Send_Just_Reset();
									 MDB_NEXT_POLL_STATE=JUST_RESET;
									 MDB_Response_Stage=0;
									 MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,0);  // Bit 0
									 Debug_PutString("\r\nMDB_Response_Stage1");

									 break;

		case RES_JUSTRESET:
									Debug_PutString("\r\nMDB_Response_Stage2");
									Send_Just_Reset();
									ACK_Stage_State=ACK_RCV_JUST_RESET;
									MDB_Response_Stage=0;
									gucMDB_Step=0;
									MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,1); // bit 1
									//Debug_PutString("\r\nMDB_Response_Stage2");
									break;
		case RES_CONFIGSETUP:

									VMC_Configur_checksum = calc_checksum(gucMDB_RecBuff, 6);
									VMC_Configur_checksum &= 0x00FF;

									// compare calculated and received checksums
									if (VMC_Configur_checksum != gucMDB_RecBuff[6])
									{
										MDB_PutChar(VMC_NAK);
										MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,2);  // Bit 2
									}
									else
									{
										Debug_PutString("\r\nMDB_Response_Stage3");
										switch(gucMDB_RecBuff[1])
										{
											case VMC_CONFIG_DATA :
																	Debug_PutString("\r\nGot MDB VMC Configuration Data");
																	// Store VMC data
																	vmc_config.featureLevel   = gucMDB_RecBuff[2];
																	vmc_config.displayColumns = gucMDB_RecBuff[3];
																	vmc_config.displayRows    = gucMDB_RecBuff[4];
																	vmc_config.displayInfo    = gucMDB_RecBuff[5];

																	switch(gucMDB_RecBuff[2])
																	{
																		case 0x01:   MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,6);
																					 MDB_logInfo.MDB_Initial_DataLogByte=Clear_Bit(MDB_logInfo.MDB_Initial_DataLogByte,7);
																					 Debug_PutString("\r\n VMC Level 1");
																					 Debug_PutString("\r\n");
																					 Debug_PutChar(MDB_logInfo.MDB_Initial_DataLogByte);
																					 break;

																		case 0x02:
																					 MDB_logInfo.MDB_Initial_DataLogByte=Clear_Bit(MDB_logInfo.MDB_Initial_DataLogByte,6);
																					 MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit  (MDB_logInfo.MDB_Initial_DataLogByte,7);

																					 Debug_PutString("\r\n VMC Level 2");
																					 Debug_PutString("\r\n");
																					 Debug_PutChar(MDB_logInfo.MDB_Initial_DataLogByte);
																					 break;

																		case 0x03:   MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,6);
																					 MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,7);
																					 Debug_PutString("\r\n VMC Level 3");
																					 Debug_PutString("\r\n");
																					 Debug_PutChar(MDB_logInfo.MDB_Initial_DataLogByte);

																					break;

																	}
																	Send_Cashless_Device_Configuration(); //sending the cashless device configuration to VMC
																	ACK_Stage_State=ACK_RCV_CONFIGDATA;

																	//MDB_NEXT_POLL_STATE = READER_CONFIG_INFO_SLV;
																	break;
											case VMC_MAX_MIN_PRICES :
																		//Debug_PutString("\r\nGot MDB VMC MAX-MIN Price Data");
																		//
																		//VMC_PRICE_Data[0]=gucMDB_RecBuff[2];
																		//VMC_PRICE_Data[1]=gucMDB_RecBuff[3];
																		//VMC_PRICE_Data[2]=gucMDB_RecBuff[4];
																		//VMC_PRICE_Data[3]=gucMDB_RecBuff[5];
																		//Debug_PutString("\r\nVMC Price data:");
																		//Debug_CovertChar_in_ASCIIwithLen(vmc_prices.maxPrice,2);
																		// Store VMC Prices
																		MDB_logInfo.VMCMAXPrice = ((uint16_t)gucMDB_RecBuff[2] << 8) | gucMDB_RecBuff[3];
																		MDB_logInfo.VMCMINPrice = ((uint16_t)gucMDB_RecBuff[4] << 8) | gucMDB_RecBuff[5];
																		Debug_PutString("\r\nVMC Price data Max:");
																		Debug_Put_Num(MDB_logInfo.VMCMAXPrice,6);
																		Debug_PutString("\r\nVMC Price data min:");
																		Debug_Put_Num(MDB_logInfo.VMCMINPrice,6);


																		Send_Acknowledgement();
																		//ACK_Stage_State=ACK_RCV_MAXMIN_PRICE;

																		MDB_NEXT_POLL_STATE = VMC_POLL_ACK;

																		break;
											default :
														break;
										}
									}

									MDB_Response_Stage=0;
									gucMDB_Step=0;

								   break;

	case RES_POLL_ACK:

									switch (ucResPollStage)
									{

									case VMC_POLL_ACK           :   Send_Acknowledgement();
																//Debug_PutString("\r\nPP ");
																		if (ucSession_END_ACkRcv)
																		{
																			//Debug_PutString("\r\nPP ");
																			ucSession_END_ACkRcv=0;
																			ucSendVendResponseToApp=1;
																		}
																	MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																	break;

									case BEGIN_SESSION          :
																	Debug_PutString("\r\nSession Start New ");
																	MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,3);  // MDB_Logbyte1 Bit 3
																	ucPresentMDB_Stage=1;
																	ACK_Stage_State=ACK_RCV_START_SESSION;
																	gpio_set_pin_level(DEX_LED,false);
																	Start_Session();
																	ucENDsessionCount=0;

																	break;
									case SESSION_CANCEL_REQUEST :

																	  Debug_PutString("\r\nCancel_session ");
																	  ACK_Stage_State= ACK_RCV_SESSION_CANCEL;
																      Cancel_Session();
																	  gucMDB_Step=0;
																	  Vend_statusToApp=TOAPP_VND_CANCEL;

																	  break;
									case VEND_APPROVED          :

																	  Debug_PutString("\r\nApprove_vend ");
																	  ucPresentMDB_Stage=2;
																	  ACK_Stage_State= ACK_RCV_VEND_APPROVE;
																	  Approved_Vend();
																	  break;
									case VEND_DENIED            :
																        Debug_PutString("\r\nVEND_DENIED ");
																		ucPresentMDB_Stage=2;
																		ACK_Stage_State= ACK_RCV_VEND_DENIED;
																		Vend_statusToApp=TOAPP_VND_CANCEL;
																		Denied_Vend();


																	break;
									case END_SESSION           :
																	if ((!guStartTaptoPAY_Comm)&&(gucAPPConnectedwithoutError))
																	{
																		ucENDsessionCount++;
																		if (ucENDsessionCount<10)
																		{
																			ACK_Stage_State=ACK_RCV_END_SESSION;
																			ucPresentMDB_Stage=3;
																			End_Session();
																			ucSession_END_ACkRcv=1;  // For  Vend Status send to APP
																		}
																		else
																		{

																			MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																			ucResPollStage=VMC_POLL_ACK;

																		}

																	}
																	else
																	{
																	  MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																	  ucResPollStage=VMC_POLL_ACK;

																	}

																	break;
									case CANCELLED              :
																	Debug_PutString("\r\nVMC Cancel_Poll");
																	MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																	ucResPollStage=VMC_POLL_ACK;
																	break;


									default						:

																	break;
									}




								MDB_Response_Stage=0;
								gucMDB_Step=0;
								break;


	case RES_PERI_ID:     //Debug_PutString("\r\nMDB_Response_Stage4");
	                        ucPeriID_checksum = calc_checksum(gucMDB_RecBuff, 31);
	                        ucPeriID_checksum &= 0x00FF;
	                        // compare calculated and received checksums
							//Debug_Dex_CovertChar_in_ASCIIwithLen(gucMDB_RecBuff,31);

							//Debug_PutStringL(gucMDB_RecBuff,31);

	                       if (ucPeriID_checksum != gucMDB_RecBuff[31])
	                        {
		                        MDB_PutChar(VMC_NAK);
	                        }
	                        else
	                        {
		                        switch(gucMDB_RecBuff[1])
		                        {
			                        case VMC_EXPANSION_REQUEST_ID  :
																	Peripheral_ID_Send();
																	ACK_Stage_State=ACK_RCV_PERI_ID;
																	break;
													default :
													 break;
		                        }
	                        }
							MDB_Response_Stage=0;
							gucMDB_Step=0;
							break;

	case RES_READER:         //Debug_PutString("\r\nMDB_Response_Stage5");
							 checksum = calc_checksum(gucMDB_RecBuff, 2);
							checksum &= 0x00FF;

							if (checksum != gucMDB_RecBuff[2])
							{

								MDB_PutChar(VMC_NAK);
								MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,3);  // Bit 3
							}
							else
							{
								Debug_PutString("\r\nVMC Reader Checksum Matched");
								switch(gucMDB_RecBuff[1])
								{
									case VMC_READER_DISABLE :
																Send_Acknowledgement();
																Debug_PutString("\r\nVMC Read Disable");
																MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																ucResPollStage=VMC_POLL_ACK;
																gpio_set_pin_level(MDB_TEST_LED,1);
																gucMDB_ReaderEnable_Flag=0;
																MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,5);  // Bit 5
																MDB_logInfo.MDB_Initial_DataLogByte=Clear_Bit(MDB_logInfo.MDB_Initial_DataLogByte,4);  // Bit 4
																MDB_logInfo.MDB_Logbyte1=Clear_Bit(MDB_logInfo.MDB_Logbyte1,0);  // MDB_Logbyte1 Bit 0

																break;
									case VMC_READER_ENABLE  :


																Send_Acknowledgement();
																Debug_PutString("\r\nVMC Read Enable");
																MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																ucResPollStage=VMC_POLL_ACK;
																gucMDB_ReaderEnable_Flag=1;
																gpio_set_pin_level(MDB_TEST_LED,0);
																MDB_logInfo.MDB_Initial_DataLogByte=Set_Bit(MDB_logInfo.MDB_Initial_DataLogByte,4);  // Bit 4
																MDB_logInfo.MDB_Initial_DataLogByte=Clear_Bit(MDB_logInfo.MDB_Initial_DataLogByte,5);  // Bit 5
																MDB_logInfo.MDB_Logbyte1=Clear_Bit(MDB_logInfo.MDB_Logbyte1,0);  // MDB_Logbyte1 Bit 0
																break;
									case VMC_READER_CANCEL  :
																Vend_Cancelled();
																Debug_PutString("\r\nVMC Read Canceled");
																gucMDB_ReaderDisable_Flag=0;
																gucMDB_ReaderEnable_Flag=0;
																gucMDB_ReaderCancel_Flag=1;
																gpio_set_pin_level(MDB_TEST_LED,1);
																MDB_logInfo.MDB_Initial_DataLogByte=Clear_Bit(MDB_logInfo.MDB_Initial_DataLogByte,4);  // Bit 4
																MDB_logInfo.MDB_Initial_DataLogByte=Clear_Bit(MDB_logInfo.MDB_Initial_DataLogByte,5);  // Bit 5
																MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,0);  // MDB_Logbyte1 Bit 0

																break;
									default : break;
								}
							}
							MDB_Response_Stage=0;
							gucMDB_Step=0;
							break;

	case RES_VENDRQST:

								//Debug_PutString("\r\nVend 1Request from VMC:- ");
								checksum = calc_checksum(gucMDB_RecBuff,6);
								checksum &= 0x00FF;



								if (checksum != gucMDB_RecBuff[6])
								{
									MDB_PutChar(VMC_NAK);
									MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,7);  // MDB_Logbyte1 Bit 7
								}
								else
								{

									Send_Acknowledgement();
									gucRelayPressCounter=0;
									gucCancel_Session=0;
									gucStartRelayPressCounterFlag=0;

									hardwareInfo.last_vend_status = 0x00;//last vend status as no Status before transaction




									MDB_logInfo.VMC_MDB_Compartment_Data[0]=gucMDB_RecBuff[4];
									MDB_logInfo.VMC_MDB_Compartment_Data[1]=gucMDB_RecBuff[5];
									guiVMCReceivedPrice = gucMDB_RecBuff[2];
									guiVMCReceivedPrice = (guiVMCReceivedPrice<<8)+gucMDB_RecBuff[3];

                                   if((gucAPPConnected)&&(!guStartTaptoPAY_Comm))
                                   {
									if((guiVMCReceivedPrice==guiBLERecPrice)&&(!uc_SudddenDisconnectFlg))
									{
										ucResPollStage = VEND_APPROVED;
										Debug_PutString("\r\nVend Rqst 4m VMC(VNK)");
									}
									else
									{
										gucVendCancelReasone=VendCancelByPriceMismatched;
										ucResPollStage = VEND_DENIED;
										Debug_PutString("\r\nPrice not matched");
										//MDB_NEXT_POLL_STATE = VEND_DENIED_SLV;
									}
                                   }
								   else if ((gucAPPConnected)&&(guStartTaptoPAY_Comm))
								   {
									   ucE_Port_trn_Stage=4;
									   ucResPollStage=VMC_POLL_ACK;
									   Debug_PutString("\r\nS_VEND RQST for E_PORT");

								   }

								}
								 //Debug_PutString("\r\nRES_VENDRQST");
	                           MDB_Response_Stage=0;
							   gucMDB_Step=0;
	                           break;

	case  RES_VNDSUCCESS :

	                              checksum = calc_checksum(gucMDB_RecBuff,4);
	                              checksum &= 0x00FF;

	                              if (checksum != gucMDB_RecBuff[4])
	                              {
		                              MDB_PutChar(VMC_NAK);
	                              }
	                              else
	                              {
		                              //Debug_PutString("\r\n Vend Success from VMC:- ");
									 if(gucAPPConnected)
									  {


										  if (guStartTaptoPAY_Comm)
										  {
											ucE_Port_trn_Stage=5;
											Vend_statusToApp=TOAPP_VND_SUCCESS;
											hardwareInfo.last_vend_status = 0x01;//last vend status as success
											Debug_PutString("\r\nS_Vend Success 4m VMC(EPORT)");

										  }
										  else
										  {
											  MDB_PutChar(VMC_ACK);
											  Vend_statusToApp=TOAPP_VND_SUCCESS;
											  hardwareInfo.last_vend_status = 0x01;//last vend status as success
											  Debug_PutString("\r\nVend Success 4m VMC(VNK)");
										  }
									  }


								 }
								  ucResPollStage = VMC_POLL_ACK;
								  gucMDB_Step=0;
								  MDB_Response_Stage=0;
								  break;

	case RES_SESSIONCOMPLETE:
								//Debug_PutString("\r\n Got Session Complete from MDB");

								checksum = calc_checksum(gucMDB_RecBuff,2);
								checksum &= 0x00FF;

								if (checksum != gucMDB_RecBuff[2])
								{
									MDB_PutChar(VMC_NAK);
									MDB_logInfo.MDB_Logbyte2=Set_Bit(MDB_logInfo.MDB_Logbyte2,3);  // MDB_Logbyte2 Bit 3

								}
								else
								{
									//if((gucMDB_VendReq_Flag==1) && (guckeyPressed==0))//added on 13 Jan if key is not press by VNK HW & directly got Session Complete
									//{

									//}

										if ((guStartTaptoPAY_Comm)&&(gucAPPConnected))
										{

											MDB_logInfo.MDB_Logbyte2=Set_Bit(MDB_logInfo.MDB_Logbyte2,2);  // MDB_Logbyte2 Bit 2

											switch (ucE_Port_trn_Stage)
													{
														case 1 :  // only senssion start by Eport

																 Debug_PutString("\r\nS_KNoVendRqtForTap_N_Pay1");
																 if (ucVendProcessRunning)
																 {
																 Vend_statusToApp=TOAPP_VND_CANCEL;
																 gucVendCancelReasone=KNoVendRqtForTap_N_Pay ;
														         hardwareInfo.last_vend_status = 0x02;//last vend status as Fail
																 ucSendVendResponseToApp=1;
																 }
																  ucEportCreditRcv=0;
																 E_PORT_CreditExpiredFlg=1;


																 break;
														case 2 :
																Debug_PutString("\r\nS_KeyNotPressed");
																 if (ucVendProcessRunning)
																 {
																	Vend_statusToApp=TOAPP_VND_CANCEL;
																	gucVendCancelReasone=KeyNotPressed;
																	hardwareInfo.last_vend_status = 0x02;//last vend status as Fail
																	ucSendVendResponseToApp=1;
																 }
																 E_PORT_CreditExpiredFlg=1;
																 ucEportCreditRcv=0;


																 break;
														case 3:
																Debug_PutString("\r\nS_KNoVendRqtForTap_N_Pay3");
																 if (ucVendProcessRunning)
																 {
																	Vend_statusToApp=TOAPP_VND_CANCEL;
																	gucVendCancelReasone=KNoVendRqtForTap_N_Pay3 ;
																	hardwareInfo.last_vend_status = 0x02;//last vend status as Fail
																	//ucSendVendResponseToApp=1;
																 }
																 E_PORT_CreditExpiredFlg=1;
																 ucEportCreditRcv=0;


																break;
														case 4 :
																Debug_PutString("\r\nS_EportVndStatusNotUpade");
																 if (ucVendProcessRunning)
																 {
																	Vend_statusToApp=TOAPP_VND_CANCEL;
																	gucVendCancelReasone=EportVndStatusNotUpade ;
																	hardwareInfo.last_vend_status = 0x00;//last vend status as Fail
																	ucSendVendResponseToApp=1;
																 }
																 ucEportCreditRcv=0;
																break;


														case 5 : Debug_PutString("\r\nS_E_Port_trn_Stage 5");

																  break;
														default:  break;

													}

											Debug_PutString("\r\nS_Session Complete 4m VMC(Eport)");

										}
										else if ((!guStartTaptoPAY_Comm)&&(gucAPPConnectedwithoutError))
										{
										  Send_Acknowledgement();
										  Debug_PutString("\r\nSession Complete 4m VMC(VNK)");
										  if (gucCardReaderSession_Start)
										  {
											 gucCardReaderSession_Start=0;   //Comment by Saiful for test

										  }


										}
										else if ((!gucAPPConnectedwithoutError))
										{
											Debug_PutString("\r\nSession Complete Eport only");
											gucCardReaderSession_Start=0;  //Comment by Saiful for test
											//ucEportCreditRcv=0;
										}

								    MDB_Response_Stage=RES_POLL_ACK;
									ucResPollStage = END_SESSION;
									//gucRec_VMCSessionComplete_Flag=0;

								}
								gucMDB_Step=0;
								MDB_Response_Stage=0;
								break;
	case RES_VEND_CANCEL:
							checksum = calc_checksum(gucMDB_RecBuff,2);
							checksum &= 0x00FF;

							if (checksum != gucMDB_RecBuff[2])
							{
								MDB_PutChar(VMC_NAK);
							}
							else
							{
								if (gucAPPConnected)
								{

										 if (guStartTaptoPAY_Comm)
										 {
											 ucE_Port_trn_Stage=5;
											 hardwareInfo.last_vend_status = 0x02;//last vend status as cancel

											 gucVendCancelReasone=VendCancelByVMCTOEPORT;
											 Vend_statusToApp=TOAPP_VND_CANCEL;
											 ucSendVendResponseToApp=1;


											 hardwareInfo.last_vend_status = 0x01;//last vend status as success
											 Debug_PutString("\r\nVend Success 4m VMC(EPORT)");
											  Debug_PutString("\r\n Case:RES_VEND_CANCEL");
										 }
										else   // For App with card
										{

											Send_Acknowledgement();


											hardwareInfo.last_vend_status = 0x02;//last vend status as cancel

											gucVendCancelReasone=VendCancelByVMC;
											Vend_statusToApp=TOAPP_VND_CANCEL;
											Debug_PutString("\r\n Case:RES_VEND_CANCEL");

										}

								}
							}

							 MDB_Response_Stage=RES_POLL_ACK;
							 ucResPollStage = VEND_DENIED;
							 //gucRec_VMCSessionComplete_Flag=0;
							 gucMDB_Step=0;

								break;
	case RES_VEND_FAIL_SLAVE:
							checksum = calc_checksum(gucMDB_RecBuff,2);
							checksum &= 0x00FF;

							if (checksum != gucMDB_RecBuff[2])
							{
								MDB_PutChar(VMC_NAK);
							}
							else
							{
								//gucMDB_VendReq_Flag=0;//added on the 14Jan2021 to for error refer Shahil mail
								if (gucAPPConnected)
								{
									if ((guStartTaptoPAY_Comm))
									{
										ucE_Port_trn_Stage=5;
										hardwareInfo.last_vend_status = 0x02;//last vend status as fail
										//Send_Acknowledgement();
										Debug_PutString("\r\nVend Fail from VMC (EPORT)");
										MDB_Response_Stage=RES_POLL_ACK;
										Vend_statusToApp=TOAPP_VND_FAIL;
										Vend_Fail_ErrorName=0X01 ;
										
										E_PORT_CreditExpiredFlg=1;
									}
									else  // For App with card
									{
										hardwareInfo.last_vend_status = 0x02;//last vend status as fail
										Send_Acknowledgement();
										Debug_PutString("\r\nVend Fail from VMC (VNK)");
										MDB_Response_Stage=RES_POLL_ACK;
										Vend_statusToApp=TOAPP_VND_FAIL;
										Vend_Fail_ErrorName=0X01 ;
										
									}
								}


							}
							gucMDB_Step=0;
							Debug_PutString("\r\n2Vend Fail from VMC");
							break;

	case RES_CASH_VEND_SLAVE:
								checksum = calc_checksum(gucMDB_RecBuff,6);
								checksum &= 0x00FF;

								if (checksum != gucMDB_RecBuff[6])
								{
									MDB_PutChar(VMC_NAK);
									MDB_logInfo.MDB_Logbyte2=Set_Bit(MDB_logInfo.MDB_Logbyte2,5);  // MDB_Logbyte2 Bit 5
								}
								else
								{

									MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte2,4);  // MDB_Logbyte2 Bit 1
									if(gucAPPConnected)
									{


										if (guStartTaptoPAY_Comm)
										{
											ucE_Port_trn_Stage=5;
											Vend_statusToApp=TOAPP_VND_SUCCESS;
											hardwareInfo.last_vend_status = 0x01;//last vend status as success
											Debug_PutString("\r\nVend Success 4m VMC(EPORT) Pr_0");
										}
										else
										{
											MDB_PutChar(VMC_ACK);
											Vend_statusToApp=TOAPP_VND_SUCCESS;
											hardwareInfo.last_vend_status = 0x01;//last vend status as success
											Debug_PutString("\r\nVend Success 4m VMC(VNK) Pr_0");
											ucResPollStage=VMC_POLL_ACK;
										}

									}
								}
							  gucMDB_Step=0;
							  MDB_Response_Stage=0;
							  guiMDB_RecBuff_Index=0;
							 Debug_PutString("\r\nCashVend");
							 break;

	case RES_REVALUE_RQST:
							checksum = calc_checksum(gucMDB_RecBuff,6);
							checksum &= 0x00FF;
							if (checksum != gucMDB_RecBuff[6])
							{
									MDB_PutChar(VMC_NAK);
									//MDB_logInfo.MDB_Logbyte2=Set_Bit(MDB_logInfo.MDB_Logbyte2,5);  // MDB_Logbyte2 Bit 5
							}
							else
							{
								if ((guStartTaptoPAY_Comm)&&(gucAPPConnected))
								{

								}
								else
								{

									Send_Acknowledgement();
								}


							}

							 gucMDB_Step=0;
							 MDB_Response_Stage=0;
							 guiMDB_RecBuff_Index=0;
							 Debug_PutString("\r\nCRevalue Reqst");
							 break;



							//break;
		default: break;


	}

	if(guc5SecIntFlag)
	{

		if((gucStart5SecTimerFlag)&&(gucAPPConnected))
		{
			Debug_PutString("\r\n Send Repeat");
			send_After_5sec(gucRept);
		}
	}


}




unsigned char Set_Bit(unsigned char Byte, unsigned char position)
{

	////Byte |=(1<<position);
	//Byte |=(1<<1);
	//Debug_PutString("\r\n ACC");
	//Debug_PutChar(Byte);
		// position th bit of Byte is being set by this operation
		return ((1 << position) | Byte);
}

unsigned char Clear_Bit(unsigned char Byte, unsigned char position)
{
	//Byte = Byte & (~(1 << position));
	return (Byte & (~(1 << (position - 1))));

}












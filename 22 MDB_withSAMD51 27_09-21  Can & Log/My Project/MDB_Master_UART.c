/*
 * MDB_UART.c
 *
 * Created: 7/7/2021 4:01:48 PM
 *  Author: User
 */

#include "basic_requirement.h"


unsigned int guiCardReader_RxData=0;


unsigned int gucCardReader_RecBuff[10];
unsigned int guiCardReader_RecBuff_Index=0;

unsigned char gucCardReaderMDB_Step=0;

unsigned char gucCardReaderSession_Start=0,gucCardReaderSession_END=0;
uint32_t RCVChar=0;
unsigned char RCV_TO_Buff_FLG=0;


unsigned char EportToVmc_Buff[40];
//unsigned int EportToVmc_Buff[40];
unsigned int EportToVmc_Buff_Index=0;
unsigned char ucEportToVmc_RcvCompleteFLG=0;


void SERCOM2_2_Handler()
{

	//volatile unsigned char debugRXbyte;

	if (REG_SERCOM2_USART_INTFLAG & 4)
	{
		RCVChar = REG_SERCOM2_USART_DATA;     /* read the receive char and return it */

       
		//Debug_Two_PutChar(RCVChar);


		

		if ((!RCV_TO_Buff_FLG))
		{
         


			switch(RCVChar)
			{
				//case CASHLESS_ACK:			
											//RCV_TO_Buff_FLG=1;
											//Cashless1_MDB_Rcv_Buff_Index=0;
											//Cashless1_MDB_Rcv_Buff[Cashless1_MDB_Rcv_Buff_Index++]	=RCVChar;
											//Cashless1_Rcv_Buff_Index_Limit=1;
											//Poll_state=1;
											//Cashless1_Step_Flg=10;
											////Debug_PutString("POLL ACK\r\n");
											//break;
											
				case READER_CONFIG_DATA_MASTER:    //0x001
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=1;
													//Debug_PutString("\r\nA");
													//Debug_PutString("\r\nA1");
													break;							
											

				case DISPLAY_RQST_MASTER:			// 0x002
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													//Debug_PutString("\r\nA2");
													Cashless1_Step_Flg=2;
													break;
													
				case BEGIN_SESSION_MASTER:			// 0x003
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,0);  // MDB_Logbyte4 Bit 0
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=3;
													//Debug_PutString("\r\nA3");
													break;									

				case SESSION_CAN_RQST_CL_MASTER:	// 0x004
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,5);
													//Debug_PutString("\r\nA4");
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=4;
													
													break;

				case VEND_APPROVED_MASTER:			// 0x005	
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,1);  // MDB_Logbyte4 Bit 1
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													//Debug_PutString("\r\nA5");
													Cashless1_Step_Flg=5;
													break;

				case VEND_DENIED_MASTER:			// 0x006    
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,2);  // MDB_Logbyte4 Bit 2
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=6;
													//Debug_PutString("\r\nA6");
													
													break;

				case END_SESSION_MASTER:			// 0x007
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,3);  // MDB_Logbyte4 Bit 3
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=7;
													//Debug_PutString("\r\nA7");
													
													break;

				case VEND_CANCELLED_CL_MASTER:		// 0x008
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,4);  // MDB_Logbyte4 Bit 4
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													//Debug_PutString("\r\nCC");
													Cashless1_Step_Flg=8;
													//Debug_PutString("\r\nA8");
													
													break;

				
												
				case PERIPHERALID_MASTER:			// 0x009
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=9;
													//Debug_PutString("\r\nA9");
													break;

				case ERROR_MASTER:					// 0x00A
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,5);  // MDB_Logbyte4 Bit 5
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=10;
													//Debug_PutString("\r\nA10");
													break;
													
				case CMDOUTOFSEQNC_MASTER:			// 0x00B
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,6);  // MDB_Logbyte4 Bit 6
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=11;
													//Debug_PutString("\r\nA11");
													break;
																										
				case REVALUE_APPROVED_MASTER:		// 0x00D
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													MDB_logInfo.MDB_Logbyte4=Set_Bit(MDB_logInfo.MDB_Logbyte4,7);  // MDB_Logbyte4 Bit 6
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=12;
													//Debug_PutString("\r\nA12");
													break;
													
				case REVALUE_DENIED_MASTER:			// 0x00E
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													MDB_logInfo.MDB_Logbyte3=Set_Bit(MDB_logInfo.MDB_Logbyte3,7);  // MDB_Logbyte4 Bit 6
													Cashless1_Step_Flg=13;
													//Debug_PutString("\r\nA13");
													break;																

				case REVALUE_LIMIT_MASTER:			// 0x00F
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=14;
													//Debug_PutString("\r\nA14");
													break;													

				case USER_FILEDATA_MASTER:			// 0x010
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=15;
													//Debug_PutString("\r\nA15");
													break;

				case TIMEDATE_RQST_MASTER:			// 0x011
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=16;
													//Debug_PutString("\r\nA16");
													break;												


				case DIAGNOSTICRESPONCE_MASTER:		// 0x0FF
													RCV_TO_Buff_FLG=1;
													EportToVmc_Buff_Index=0;
													//EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
													Cashless1_Step_Flg=17;
													//Debug_PutString("\r\nA17");
													break;

				default:							break;


			}
		}

		//if ((RCV_TO_Buff_FLG)&&((RCVChar&0x100)!= 0x100)&&(!ucEportToVmc_RcvCompleteFLG))
		if ((RCV_TO_Buff_FLG)&&((RCVChar&0x100)!= 0x100))
		{
			EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
			//Debug_PutString("\r\nAc");
		}
		//else if (((RCV_TO_Buff_FLG)&&((RCVChar&0x100)== 0x100))&&(RCVChar!= 0x100))
		else if (((RCV_TO_Buff_FLG)&&((RCVChar&0x100)== 0x100)))
		{
			EportToVmc_Buff[EportToVmc_Buff_Index++]=RCVChar;
			ucEportToVmc_RcvCompleteFLG=1;
			//Debug_PutString("\r\nAB");
		}






		REG_SERCOM2_USART_INTFLAG = 4;  /* clear interrupt flag */
	}

}




void initilize_MDB_Master_uart(void)
{
	GCLK->PCHCTRL[23].reg=  0x40;		//selecting the main core clock for SERCOM2
	GCLK->PCHCTRL[3].reg=  0x43;		//selecting slow clock for SERCOM2
	MCLK->APBBMASK.reg |= MCLK_APBBMASK_SERCOM2;


	gpio_set_pin_function(PA13, PINMUX_PA13C_SERCOM2_PAD1);
	gpio_set_pin_function(PA12, PINMUX_PA12C_SERCOM2_PAD0);

	REG_SERCOM2_USART_CTRLA |= 1;               /* reset SERCOM7 */
    while (REG_SERCOM2_USART_SYNCBUSY & 1) {}   /* wait for reset to complete */
    REG_SERCOM2_USART_CTRLA = 0x40106004;       /* LSB first, async, no parity,
        PAD[0]-Tx, BAUD uses fraction, 8x oversampling, internal clock */
    REG_SERCOM2_USART_CTRLB = 0x00030001;       /* enable Tx/Rx, one stop bit, 8 bit */
    REG_SERCOM2_USART_BAUD  = (((float)dXTAL_FREQUENCY / 8)/dMDB_MASTER_BAUDRATE);	//set Baud rate ;               /* 1000000/8/9600 = 13.02 */
    REG_SERCOM2_USART_CTRLA |= 2;               /* enable SERCOM2 */
    while (REG_SERCOM2_USART_SYNCBUSY & 2) {}   /* wait for enable to complete */

	REG_SERCOM2_USART_INTENSET = 4;             /* enable receive complete interrupt */
	//Debug_PutString("\n\r Initialize DEX UART");
}

void MDB_Master_PutChar(uint32_t data)
{
    while(!(REG_SERCOM2_USART_INTFLAG & 1)) {}  /* wait for data register empty */
    REG_SERCOM2_USART_DATA = data;              /* send a char */
}

void MDB_Master_PutString(char *str)
{
	unsigned int i=0;
	while(str[i]!='\0')                 //check for NULL character to terminate loop
	{
		while(!(REG_SERCOM2_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM2_USART_DATA = str[i];              /* send a char */
		i++;
	}
}


void MDB_Master_PutStringL(unsigned char *str,unsigned int len)
{
	unsigned int i=0;
	while(len>0)
	{
		while(!(REG_SERCOM2_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM2_USART_DATA = str[i];              /* send a char */
		i++;
		len--;
	}
}

/*
 * MDB_UART.c
 *
 * Created: 7/7/2021 4:01:48 PM
 *  Author: User
 */ 

#include "basic_requirement.h"

unsigned char Convert_DataArray[30];


/* initialize UART3 to transmit at 9600 Baud */
void initilize_Debug_uart(void) 
{
	GCLK->PCHCTRL[36].reg=  0x40;		//selecting the main core clock for SERCOM2
	GCLK->PCHCTRL[3].reg=  0x43;		//selecting slow clock for SERCOM2
	MCLK->APBDMASK.reg |= MCLK_APBDMASK_SERCOM6;
	
	
	gpio_set_pin_function(PC05, PINMUX_PC05C_SERCOM6_PAD1);
	gpio_set_pin_function(PC13, PINMUX_PC13D_SERCOM6_PAD0);
	
	REG_SERCOM6_USART_CTRLA |= 1;               /* reset SERCOM7 */
    while (REG_SERCOM6_USART_SYNCBUSY & 1) {}   /* wait for reset to complete */
    REG_SERCOM6_USART_CTRLA = 0x40106004;       /* LSB first, async, no parity,
        PAD[0]-Tx, BAUD uses fraction, 8x oversampling, internal clock */
    REG_SERCOM6_USART_CTRLB = 0x00030000;       /* enable Tx/Rx, one stop bit, 8 bit */
    REG_SERCOM6_USART_BAUD  = (((float)dXTAL_FREQUENCY / 8)/dBLE_BAUDRATE);	//set Baud rate ;               /* 1000000/8/9600 = 13.02 */
    REG_SERCOM6_USART_CTRLA |= 2;               /* enable SERCOM6 */
    while (REG_SERCOM6_USART_SYNCBUSY & 2) {}   /* wait for enable to complete */
		
	//REG_SERCOM6_USART_INTENSET = 4;             /* enable receive complete interrupt */
	
}

void Debug_PutChar(char data) 
{
    while(!(REG_SERCOM6_USART_INTFLAG & 1)) {}  /* wait for data register empty */
    REG_SERCOM6_USART_DATA = data;              /* send a char */
}

void Debug_PutString(char *str) 
{
	unsigned int i=0;
	while(str[i]!='\0')                 //check for NULL character to terminate loop
	{
		while(!(REG_SERCOM6_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM6_USART_DATA = str[i];              /* send a char */
		i++;
	}
}


void Debug_PutStringL(unsigned char *str,unsigned int len)
{
	unsigned int i=0;
	while(len>0)
	{
		while(!(REG_SERCOM6_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM6_USART_DATA = str[i];              /* send a char */
		i++;
		len--;
	}
}

void Debug_Put_Num(unsigned long Num1,char digit1)
{

	HexToAscii_ndigitUart(Num1,0, digit1);
	for (unsigned int i=0 ; i<digit1 ; i++)
	{
		Debug_PutChar(Convert_DataArray[i]);
	}

	
}

void HexToAscii_ndigitUart(unsigned long Num,unsigned char start, unsigned char digit)
{
	unsigned char i;
	unsigned char j=10;
	for(i=(digit+start) ; i>(start) ; i--)
	{
		Convert_DataArray[i-1]=(Num%j)+'0';
		Num = Num / j;
	}
}



void Debug_CovertChar_in_ASCII(uint8_t data)
{
	unsigned char tempData,tempHex,tempHex1;
	//while (len > 0)
	//{
	tempData = data;
	tempHex=((tempData & 0xF0) >>4);
	tempHex1=((tempData & 0x0F));
	//			Hex_to_char(tempHex);   //convert the HEX to Character
	//			Hex_to_char(tempHex1);  //convert the Hex to Character
	Debug_PutChar(Hex_to_char(tempHex));
	Debug_PutChar(Hex_to_char(tempHex1));
	Debug_PutChar(' ');
	//len--;
	//}
}

void Debug_CovertChar_in_ASCIIwithLen(uint8_t *data,int len)
{
	unsigned char tempData,tempHex,tempHex1;
	int i=0;
	while (len > 0)
	{
	tempData = data[i];
	tempHex=((tempData & 0xF0) >>4);
	tempHex1=((tempData & 0x0F));
	//			Hex_to_char(tempHex);   //convert the HEX to Character
	//			Hex_to_char(tempHex1);  //convert the Hex to Character
	Debug_PutChar(Hex_to_char(tempHex));
	Debug_PutChar(Hex_to_char(tempHex1));
	Debug_PutChar(' ');
	len--;
	i++;
	}
}

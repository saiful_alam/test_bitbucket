/*
 * MDB_Protocol.h
 *
 * Created: 7/5/2021 3:06:40 PM
 *  Author: User
 */ 


#ifndef MDB_PROTOCOL_H_
#define MDB_PROTOCOL_H_


#define TOAPP_VND_SUCCESS 1
#define TOAPP_VND_CANCEL  2
#define TOAPP_VND_FAIL    3

////****** New Variables
extern unsigned char ACK_Stage_State;
extern unsigned char MDB_Response_Stage;
extern unsigned char VMC_Configur_checksum;
extern unsigned char ucPeriID_checksum;
extern unsigned char ucResPollStage;
extern unsigned char  ucSession_END_ACkRcv;
extern unsigned char ucRESET_RecvFLG;
extern unsigned char uc_CreditDoneFlg;
extern  unsigned char Vend_statusToApp;
extern unsigned char ucSendVendResponseToApp;
extern unsigned int uiBLETransuctionCount;
extern unsigned char  ucPresentMDB_Stage;
extern unsigned char  uc_SudddenDisconnectFlg;
//extern unsigned char ucVendRqstfrmAppFLG;

extern unsigned char gucAmountCreditedPressKey,E_PORT_CreditExpiredFlg,ucE_Port_CreditExpireStatustoApp;
extern unsigned char uc_VndReqstTimerFLG,ucVendingTimerExpiredFlg,ucENDsessionCount;
extern unsigned int ui_VndReqstTimerFCount;
extern unsigned int Eportcreditwaitcount;
extern unsigned char EportcreditwaitFlg,EportcreditwaitTimer_ExpiredFlg;
extern unsigned char Vend_Fail_ErrorName;
//extern unsigned char Eport_MDB_SETUP_Data[8];
//extern unsigned char VMC_PRICE_Data[5];
//extern unsigned char MDB_Initial_DataLogByte;


typedef struct
{
	unsigned char MDB_Initial_DataLogByte;
	unsigned char MDB_Logbyte1;  // byte 1
	unsigned char MDB_Logbyte2;
	unsigned char MDB_Logbyte3;
	unsigned char MDB_Logbyte4;
	unsigned char Eport_MDB_SETUP_Data[8];
	unsigned char VMC_MDB_Compartment_Data[2];
	uint16_t VMCMAXPrice;
	uint16_t VMCMINPrice;
	
	
	
}MDBLogByte;


//typedef struct
//{
	//unsigned char LogIntialByteHWINFO;  // byte 1
	//unsigned char Eport_MDB_LVL;
	//unsigned char Eport_MDB_CountryCode_MSB;
	//unsigned char Eport_MDB_CountryCode_LSB;
	//unsigned char Eport_Scale_Factor;
	//unsigned char Eport_Decimal_place;
	//unsigned char Eport_ResponseTime;
	//unsigned char Eport_MiscellaneousByte;
	//unsigned char VMC_MAX_Price_MSB;
	//unsigned char VMC_MAX_Price_LSB;
	//unsigned char VMC_MIN_Price_MSB;
	//unsigned char VMC_MIN_Price_LSB;
	//
//}LogIn_HW_Info;



extern MDBLogByte MDB_logInfo; 
//extern LogIn_HW_Info MDB_SETUP_logInfo; 

void MDB_Communication(void);
unsigned char Set_Bit(unsigned char Byte, unsigned char position);
unsigned char Clear_Bit(unsigned char Byte, unsigned char position);




#endif /* MDB_PROTOCOL_H_ */
/*
 * basic_requirement.c
 *
 * Created: 2019-12-09 11:51:26 AM
 *  Author: User
 */ 

#include "basic_requirement.h"










unsigned char ucInitial_MDB_CommunicationTimer_Flg=0;
unsigned long uiInitial_MDB_CommunicationTimer_Count=0;

unsigned char gucAPPConnected=0;
unsigned char guStartTaptoPAY_Comm=0,ucEportVend_KeyPress=0;
unsigned char ucInitialMDBCommError=0;
unsigned char uc_start_Flg=0;
unsigned int ucEportVend_KeyPressTiemr=0;




void PORT_INIT (void)
{
	
	gpio_set_pin_direction(BLE_Status_LED,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(BLE_RESET_PIN,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(BLE_CONNECT_STATUS_PIN,GPIO_DIRECTION_IN);
	
	
	
	gpio_set_pin_direction(DEX_LED,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(TOGGLE_LED,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(MDB_TEST_LED,GPIO_DIRECTION_OUT);
	
	
	gpio_set_pin_direction(DEX_SEL_RLY,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(EPORT_M_S_RLY,GPIO_DIRECTION_OUT);
	gpio_set_pin_direction(DEVICE_SEL_RLY,GPIO_DIRECTION_OUT);
	
	
	

	
	
	
	
	gpio_set_pin_level(DEVICE_SEL_RLY,1);
	gpio_set_pin_level(BLE_RESET_PIN,0);
	
}






unsigned char Hex_to_char(unsigned char temp)
{
	if(temp==0x00)
	{
		return(temp+0x30);
	}
	else if(temp>=0x01 && temp<=0x09)
	{
		return(temp+0x30);
	}
	else if(temp==0x0A)
	{
		return('A');
	}
	else if(temp==0x0B)
	{
		return('B');
	}
	else if(temp==0x0C)
	{
		return('C');
	}
	else if(temp==0x0D)
	{
		return('D');
	}
	else if(temp==0x0E)
	{
		return('E');
	}
	else if(temp==0x0F)
	{
		return('F');
	}
	else
	{
		return(0xFF);
	}
}




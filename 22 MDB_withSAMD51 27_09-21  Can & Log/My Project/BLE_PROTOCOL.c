/*
 * BLE_PROTOCOL.c
 *
 * Created: 15-07-2021 15:19:14
 *  Author: USer
 */

#include "basic_requirement.h"

unsigned int gfinPaisa=0,gfinRs=0,gfBLEPrice=0;
unsigned int guiPriceinPaise=0;
unsigned char gucCRCMatched=0; //to indicated the received CRC is matched or not matched
unsigned char guc_HW_INfoSnd_OnceFLG=0;
unsigned char E_PORT_CreditStatus=0;
unsigned char ucVendProcessRunning=0;
unsigned char uc_ReadyToCommunicatewithApp=0;

unsigned char ucVendsession4mApp=0,uchwinfoAckRecved=0;
 unsigned char gucAPPConnectedwithoutError=0;

  //unsigned char ucTapNpayVendRqstFlg=0;
 // unsigned int ucTapNpayVendRqstFTimerCount=0;

unsigned char gucBLE_ReCBuffer[dRXBLE_BUFFER_INDEX];

void BLE_CONNECT(void)
{

			if((gpio_get_pin_level(BLE_CONNECT_STATUS_PIN)==0)&&(!gucAPPConnected))  // App connected
			{
				if(!guc_HW_INfoSnd_OnceFLG)
				{

					if(gucCardReaderSession_Start==1)
					{
						hardwareInfo.error_code=MACHINE_STATUS_EPORTACTIVE;
						Debug_PutString("\r\n APP Connected with Error 06");
					}
					else if (ucEportCreditRcv)
					{
					 hardwareInfo.error_code=MACHINE_STATUS_EPORT_CREDIT;
					 Debug_PutString("\r\n APP Connected with Error 05");
					 //uc_ReadyToCommunicatewithApp=0;
					}
					else if (ucInitialMDBCommError)
					{
					  hardwareInfo.error_code=MACHINE_STATUS_POWRON_MDBERR;

					}
					else if (!gucMDB_ReaderEnable_Flag)
					{
						 hardwareInfo.error_code=MACHINE_STATUS_MDBERR;
					}

					else
					{
						//MDB_logInfo.MDB_Logbyte4=MDB_logInfo.MDB_Logbyte4 & 0XFE;
						//MDB_logInfo.MDB_Logbyte3=MDB_logInfo.MDB_Logbyte3 & 0XFE;
						//MDB_logInfo.VMC_MDB_Compartment_Data[0]=0;
						//MDB_logInfo.VMC_MDB_Compartment_Data[1]=0;

						uc_ReadyToCommunicatewithApp=1;     // Used for communication to App without error condition
						Clear_Key_Pressing();				// clearing all the  key pressing related flag
						ucVendProcessRunning=0;				// clearing gucVendProcessRunning flag
						gucAPPConnectedwithoutError=1;

						gpio_set_pin_level(EPORT_M_S_RLY,1);      //connect the EPORT MDB Line   with VNK  Master MDB uart
						gpio_set_pin_level(DEVICE_SEL_RLY,true);  //connect the RX line with VNK Device

						delay_ms(500);

						Enable_Disable_To_Eport (0X00); //sending Read disable to the Eport
						Debug_PutString("\r\n APP Connected");
						uiBLETransuctionCount=0;

						uchwinfoAckRecved=0;
						hardwareInfo.error_code=0x01;
						ucSendVendResponseToApp=0;


					}
					MDB_logInfo.MDB_Logbyte4=MDB_logInfo.MDB_Logbyte4 & 0XFE;
					MDB_logInfo.MDB_Logbyte3=MDB_logInfo.MDB_Logbyte3 & 0XFE;
					MDB_logInfo.VMC_MDB_Compartment_Data[0]=0;
					MDB_logInfo.VMC_MDB_Compartment_Data[1]=0;

					Send_Hardware_Info();
					//Send_Hardware_Info_Debug18();

					if (hardwareInfo.error_code==0x01)
					{
						start_5Sec_timer();
						guiSendResponseCounter=0;
						gucRept=dAPP_CONNECTED;
					}

					gucAPPConnected=1;
					guc_HW_INfoSnd_OnceFLG=1;
					E_PORT_CreditExpiredFlg=0;
				}


			}

////***********************************************	App Disconnected  ********************************************

			else if ((gpio_get_pin_level(BLE_CONNECT_STATUS_PIN)==1)&&(gucAPPConnected))  // App Disconnected
			{

			if((guc_HW_INfoSnd_OnceFLG))
			{
				guc_HW_INfoSnd_OnceFLG=0;

				if (!ucPresentMDB_Stage)   // check whether the MDB session end done properly or not,
				{
							uc_ReadyToCommunicatewithApp=0;
							Enable_Disable_To_Eport (0X01); //sending Read Enable to the Eport
							delay_ms(300);
							reset_5Sec_timer();

							if (uc_start_Flg)
							{
							  gpio_set_pin_level(DEVICE_SEL_RLY,0);//connect the RX line with Card Reader
							  gpio_set_pin_level(EPORT_M_S_RLY,0);
							}


							guStartTaptoPAY_Comm=0;
							gucVendkinAppConnected=0;
							gucAPPConnected=0;
							guiSendResponseCounter=0;   // used for timer count increment.
							gucRept=0;
							guAxpect_Subcode = dNULL;
							E_PORT_Start_SessionFLG=0;   //// *********************
							ucTimmer_Credit_FLG=0;
							ucTimmer_Credit_Count=0;
							Send_MonyACPFLG=0;
							gucStartRelayPressCounterFlag=0;
							gucRelayPressCounter=0;
							ucVendsession4mApp=0;

							uc_VndReqstTimerFLG=0;
							ui_VndReqstTimerFCount=0;
							ucVendingTimerExpiredFlg=0;
							ucVendProcessRunning=0;
							gucAPPConnectedwithoutError=0;

							ucEportCreditRcv=0;

							Debug_PutString("\r\n Rx Line Disconnected by from VNK Device");
							//guc_HW_INfoSnd_OnceFLG=0;
							Debug_PutString("\r\n APP Disconnected");

				}
				else
				{
					uc_SudddenDisconnectFlg=1;
					hardwareInfo.error_code=0x06;
					switch (ucPresentMDB_Stage)
					{

						case 1:			Debug_PutString("\r\n Canceling Session After BT Disconnect");
										//ucResPollStage=SESSION_CANCEL_REQUEST_SLV;
										gucCancel_Session=1;
										//gucMDB_Step=0;
										break;
						case 2:     Debug_PutString("\r\n Vend Approved BT Disconnect");
										break ;
						case 3:     Debug_PutString("\r\n End session BT Disconnect");
										break;
						default:
										break;
					}

				}

				guc_HW_INfoSnd_OnceFLG=0;

			}
			}


	if ((uc_SudddenDisconnectFlg)&&(!ucPresentMDB_Stage))
	{

		Debug_PutString("\r\n APP Delay  Disconnected");


     Enable_Disable_To_Eport (0X01); //sending Read Enable to the Eport

		//MDB_Master_PutChar(0X114);
		//delay_us(interdataDelay);
		//MDB_Master_PutChar(0X001);
		//delay_us(interdataDelay);
		//MDB_Master_PutChar(0X015);
		delay_ms(300);
		 if (uc_start_Flg)
		 {
			gpio_set_pin_level(DEVICE_SEL_RLY,0);//connect the RX line with Card Reader
			gpio_set_pin_level(EPORT_M_S_RLY,0);
		 }


		Debug_PutString("\r\n Rx Line Disconnected by from VNK Device");
		guc_HW_INfoSnd_OnceFLG=0;
		Debug_PutString("\r\n APP late Disconnected");
		guStartTaptoPAY_Comm=0;
		gucVendkinAppConnected=0;
		gucAPPConnected=0;
		guiSendResponseCounter=0;
		gucRept=0;
		guAxpect_Subcode = dNULL;
		//ucCashless1_ReadEnableFlg=1; // Comment on 13-08-21
		E_PORT_Start_SessionFLG=0;
		ucTimmer_Credit_FLG=0;
		ucTimmer_Credit_Count=0;
		Send_MonyACPFLG=0;
		gucStartRelayPressCounterFlag=0;
		gucRelayPressCounter=0;
        uc_ReadyToCommunicatewithApp=0;
		hardwareInfo.error_code=MACHINE_STATUS_OK;
		ucVendsession4mApp=0;
		reset_5Sec_timer();
		uc_VndReqstTimerFLG=0;
		ui_VndReqstTimerFCount=0;
		ucVendingTimerExpiredFlg=0;
		ucVendProcessRunning=0;
		uc_SudddenDisconnectFlg=0;
		gucAPPConnectedwithoutError=0;
		ucEportCreditRcv=0;
	}


}







void BLE_Communication(void)
{

	unsigned char checksum_BLE=0,gucProcessBLE_Frame_Flag=0,gucCRCMatched=0;
	int j=0;

	//----------- copy BLE received data in another buffer -----------------------------------
	if(gucBLE_Frm_rec_Flag)
	{
		//Debug_PutString("\r\n Received response from the BLE:- ");
		//Debug_CovertChar_in_ASCIIwithLen(gucBLE_RXBuffer,guiBLE_RXBuffer_Index);

		for(j=0;j<guiBLE_RXBuffer_Index;j++)
		gucBLE_ReCBuffer[j]=gucBLE_RXBuffer[j];

		gucProcessBLE_Frame_Flag=1;
		gucBLE_Frm_rec_Flag=0;
		guiBLE_RXBuffer_Index=0;
		gucBLE_RXBuffer[guiBLE_RXBuffer_Index]='\0';
	}
	//-------------------------------------------------------------------------------------------------------

	//---------------------- verify checksum of the BLE frame -----------------------------------------------
	if(gucProcessBLE_Frame_Flag)
	{
		gucProcessBLE_Frame_Flag=0;

		checksum_BLE=Verify_Checksum(gucBLE_ReCBuffer,(j-1));

				//Debug_PutString("\r\n Received Checksum:- ");
				//Debug_CovertChar_in_ASCII(checksum_BLE);
				//Debug_PutString("      Calculated Checksum:- ");
				//Debug_CovertChar_in_ASCII(gucBLE_ReCBuffer[j-1]);

		if(checksum_BLE==gucBLE_ReCBuffer[j-1])
		{
			//Debug_PutString("\r\n Checksum Match");

			gucCRCMatched=1;

			if(guiExpectedLen!=guiBLE_Total_Frm_Len)
			{
				gucResp = guAxpect_Subcode;
				gucBLE_ReCBuffer[4]=0x00;//making Zero because we want sent again on the CRC mismatched

				if(gucBLE_ReCBuffer[3]==dVEND_PRODUCT)//updated on the 24/02/2021
				{
					////Debug_PutString("\r\n Checksum not matched for vend request");
					send_NACK_onCRC_Mismatch(gucBLE_ReCBuffer[3]);
					gucCRCMatched=0;
				}
			}
			else
			{
				gucResp = gucBLE_ReCBuffer[3];

				if((gucBLE_ReCBuffer[3] != guAxpect_Subcode) && (gucBLE_ReCBuffer[0]== dPROTO_RESPONSE))
				{
					gucResp = guAxpect_Subcode;
					gucBLE_ReCBuffer[4]=0x00;//making Zero because we want sent again on the CRC mismatched
					gucCRCMatched=1;
				}
			}
		}
		else
		{
			//Debug_PutString("\r\n Checksum Not Match");

			gucCRCMatched=0;

			gucResp = gucBLE_ReCBuffer[3];

			if(gucResp==dVEND_PRODUCT)
			{
				////Debug_PutString("\r\n Checksum not matched for vend request");
				send_NACK_onCRC_Mismatch(gucResp);
			}
			else
			{
				////Debug_PutString("\r\n must sent again");

				if(gucResp == guAxpect_Subcode)//getting
				{
					gucResp = guAxpect_Subcode;
					gucBLE_ReCBuffer[4]=0x00;//making Zero because we want sent again on the CRC mismatched
					gucCRCMatched=1;
				}
				else
				{
					gucBLE_ReCBuffer[4]=0x00;//making Zero because we want sent again on the CRC mismatched
					gucResp = guAxpect_Subcode;
					gucCRCMatched=1;
				}
			}
		}
	}
		//-------------------------------------------------------------------------------------------------------


	//if(gucBLE_Frm_rec_Flag)
{

	if(gucCRCMatched==1)
	{
			gucCRCMatched=0;
		if (uc_ReadyToCommunicatewithApp)
		{

			switch(gucResp)
			{
				case dAPP_CONNECTED:
										break;

				case dAPP_DISCONNECTED:
										break;

				case dHARDWARE_INFO:

										if(((gucBLE_ReCBuffer[4]==0x00) ||(gucBLE_ReCBuffer[0]!=0xA6))&&(!uchwinfoAckRecved)) // if we get NACK of the HW info
										{
											start_5Sec_timer();
											guiSendResponseCounter=0;
											gucRept=dHARDWARE_INFO;
											Debug_PutString("\r\n Got HArdware info NACK from APP1");
										}
										else if (((gucBLE_ReCBuffer[4]==0x01) ||(gucBLE_ReCBuffer[0]==0xA6))&&(!uchwinfoAckRecved))
										{
											gucVendkinAppConnected=1;
											uchwinfoAckRecved=1;
											//gucAPPConnected=1;
											reset_5Sec_timer();
											gucRept=dNULL;
											Debug_PutString("\r\n Got HArdware info ACK from APP1");

											//sending the Cash string or dex data

											int ix=0;
											//for ( ix=0;gucCash_SalesData_Buff[ix]!=0x01;ix++)
											//{
//
											//}
											guiDex_Data_Total_Page = ix/500;
											guiDex_Data_Total_Page=0;
											ix=0;

											if((ix%500)!=0)
											{
												guiDex_Data_Total_Page=guiDex_Data_Total_Page+1;
											}

											if(guiDex_Data_Total_Page>=1)
											{
												guiCurrentDexPage=1;


												Write_Data_on_BLE_Write(guiCurrentDexPage);

												delay_ms(500);

												Send_Parced_DEX_start_Info();
												start_5Sec_timer();
												guiSendResponseCounter=0;
												gucRept=dPARCED_DEX_DATA;
											}

										}



										break;

				case dVEND_PRODUCT:
									if((gucVendkinAppConnected) && (gucBLE_ReCBuffer[0]==0xA5)&&(!ucVendProcessRunning))
									{


											ucVendProcessRunning=1;
											ucVendsession4mApp=1;   // once vend session start App cant send TAP&PAY request this flg will be zero at the disconnection
											uiBLETransuctionCount++;

											Debug_PutString("\r\n *****************************************");
											Debug_PutString("\r\nTransaction No:");
											Debug_Put_Num(uiBLETransuctionCount,3);
											Debug_PutString("\r\nVend request from APP");
											//saving compart ment data ion ASCII
											vendRequest.compartment[0]= gucBLE_ReCBuffer[4];
											vendRequest.compartment[1]= gucBLE_ReCBuffer[5];
											vendRequest.compartment[2]= gucBLE_ReCBuffer[6];
											vendRequest.compartment[3]= gucBLE_ReCBuffer[7];

											vendRequest.price[0]= gucBLE_ReCBuffer[8];
											vendRequest.price[1]= gucBLE_ReCBuffer[9];
											vendRequest.price[2]= gucBLE_ReCBuffer[10];
											vendRequest.price[3]= gucBLE_ReCBuffer[11];

											uc_VndReqstTimerFLG=1;
											ui_VndReqstTimerFCount=0;

											Send_VendRequest_ACK_NACK(dAPP_ACK);
											//reset_5Sec_timer();

											reset_5Sec_timer();
											guAxpect_Subcode = dNULL;

											//if((vendRequest.compartment[0]==0x00) &&
											//((vendRequest.compartment[1]>=0x00))&&
											//(((vendRequest.compartment[2]>=0x41) || ((vendRequest.compartment[2]<=0x46))) &&
											//((vendRequest.compartment[3]>=0x31) || ((vendRequest.compartment[3]<=0x39)))))

											//if((vendRequest.compartment[0]==0x00) &&
											//((vendRequest.compartment[1]>=0x00))&&
											//(((vendRequest.compartment[2]>=0x31) || ((vendRequest.compartment[2]<=0x39))) &&
											//((vendRequest.compartment[3]>=0x30) || ((vendRequest.compartment[3]<=0x39)))))    // For Actual Cantaloupe

											if((vendRequest.compartment[0]==0x00) &&
											((vendRequest.compartment[1]>=0x00))&&
											(((vendRequest.compartment[2]>=0x31) || ((vendRequest.compartment[2]<=0x39))) &&
											((vendRequest.compartment[3]>=0x30) || ((vendRequest.compartment[3]<=0x39)))))    // For Seaga
											{

												Debug_PutString("\r\nOK Compartment");

												MDB_logInfo.MDB_Logbyte1=MDB_logInfo.MDB_Logbyte1 & 0X01;  // Clearing all bit except bit0(Received Reader canceled)
												MDB_logInfo.MDB_Logbyte2=0;  // Clearing all bit of Byte 2
												MDB_logInfo.MDB_Logbyte3=MDB_logInfo.MDB_Logbyte3 & 0X01;  // Clearing all bit except bit0(Start session occur before this event )
												MDB_logInfo.MDB_Logbyte4=MDB_logInfo.MDB_Logbyte4 & 0X01;  // Clearing all bit except bit0(Start session occur before this event )

																											// clear this bit After sending vend status to App.
												MDB_logInfo.VMC_MDB_Compartment_Data[0]=0;                  // Clearing MDB Compartment no
												MDB_logInfo.VMC_MDB_Compartment_Data[1]=0;				    // Clearing MDB Compartment no

												MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,1);  // MDB_Logbyte1 Bit 1

												gucRx_Product_Code[0]= gucBLE_ReCBuffer[4];
												gucRx_Product_Code[1]= gucBLE_ReCBuffer[5];
												gucRx_Product_Code[2]= gucBLE_ReCBuffer[6];
												gucRx_Product_Code[3]= gucBLE_ReCBuffer[7];
												gucRx_Product_Code[4]= '\0';


												gfinRs=vendRequest.price[0]<<8;
												gfinRs=gfinRs+vendRequest.price[1];

												gfinPaisa=vendRequest.price[2]<<8;
												gfinPaisa=gfinPaisa+vendRequest.price[3];
												gfBLEPrice = (gfinRs*100) + (gfinPaisa/100);

												guiBLERecPrice = (unsigned int)gfBLEPrice;

												rev_amount_H=guiBLERecPrice/256;
												rev_amount_L=guiBLERecPrice%256;

											if (gucMDB_ReaderEnable_Flag)
											{
											
												if (guStartTaptoPAY_Comm)
												{


													//if (E_PORT_CreditExpiredFlg)
													//{
														//ucE_Port_CreditExpireStatustoApp=1; // NO EPORT Credit
														//
													//}
													//else
													//{

														ucEportVend_KeyPress=1;
														uiEportVendReqstwithoutCredit=0;



													//}

													//ucE_Port_trn_Stage=2;

												}
												else
												{
													ucPresentMDB_Stage=1;
													ucResPollStage = BEGIN_SESSION;
												}
											}
											else
											{
												
											   //MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,2);  // MDB_Logbyte1 Bit 2
											   ucVendProcessRunning=0;
											   gucVendCancelReasone=MDB_DisableAfter_BT_Connection;
											   ucSendVendResponseToApp=1;
											   Vend_statusToApp=TOAPP_VND_CANCEL;
											   //start_5Sec_timer();
											   // gucDirectVendCancelFlag=1;//used when compartment number  not matched
											   //guiSendResponseCounter=0;
											   //gucRept=dVEND_CANCEL;
											   delay_ms(100); 	
												
											}
											}
											else
											{
												MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,2);  // MDB_Logbyte1 Bit 2
												ucVendProcessRunning=0;
												gucVendCancelReasone=VendCancelWrongComprtment;
												ucSendVendResponseToApp=1;
												Vend_statusToApp=TOAPP_VND_CANCEL;
												//start_5Sec_timer();
												// gucDirectVendCancelFlag=1;//used when compartment number  not matched
												//guiSendResponseCounter=0;
												//gucRept=dVEND_CANCEL;
												delay_ms(100);
											}


										//}

									}

									break;

				case dVEND_SUCESS:
									if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
									{
										start_5Sec_timer();
										guiSendResponseCounter=0;
										gucRept=dVEND_SUCESS;
										Debug_PutString("\r\n Got the vend success NACK from APP");
									}
									else
									{
										Debug_PutString("\r\nSuccess ACK frm APP");

										if (ucVendProcessRunning)
										{
											Send_MachineReady();
											start_5Sec_timer();
											gucRept=dMACHINE_READY;
										}




									}
									break;

				case dVEND_CANCEL:

									if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
									{
										reset_5Sec_timer();
										start_5Sec_timer();
										guiSendResponseCounter=0;
										gucRept=dVEND_CANCEL;
										Debug_PutString("\r\n Got the vend cancel NACK from APP");
									}
									else
									{


										Send_MachineReady();
										ucVendProcessRunning=0;
										start_5Sec_timer();
										gucRept=dMACHINE_READY;

										// reset_5Sec_timer();
										gucVendCancelReasone=0;
										Debug_PutString("\r\n Got the vend cancel ACK from APP");
									}
									break;

				case dVEND_FAIL:

									if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
									{
										start_5Sec_timer();
										guiSendResponseCounter=0;
										gucRept=dVEND_FAIL;
										Debug_PutString("\r\n Got Vend Fail NACK from APP");
									}
									else
									{

										Send_MachineReady();
										ucVendProcessRunning=0;
										reset_5Sec_timer();
										Debug_PutString("\r\n Got Vend Fail ACK from APP");
									}
									break;

				case dMACHINE_READY:

									if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
									{
										start_5Sec_timer();
										guiSendResponseCounter=0;
										gucRept=dMACHINE_READY;
										Debug_PutString("\r\n Got Machine Ready NACK from APP");
									}
									else
									{
										ucVendProcessRunning=0;
										reset_5Sec_timer();
										guiSendResponseCounter=0;
										guAxpect_Subcode = dNULL;
										Debug_PutString("\r\nMachine Ready ACK from APP");
										//BLE_PutString("\r\nGot Machine Ready ACK from APP");
									}
									break;

				case dSEND_DEX_DATA:
									//Debug_PutString("\r\n Got send data ACK");
									if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
									{
										start_5Sec_timer();
										guiSendResponseCounter=0;
										gucRept=dSEND_DEX_DATA;
										Debug_PutString("\r\n Got Send Dex Data NACK from APP");
									}
									else
									{
										reset_5Sec_timer();
										Debug_PutString("\r\n Got Send Dex Data ACK from APP");
									}
									break;

				case dDEX_PAGE_UPDATE:
										if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
										{
											Debug_PutString("\r\n Got Dex Page Update NACK from APP");
										}
										else
										{

											Debug_PutString("\r\n Got Dex Page Update ACK from APP");

											if(guiCurrentDexPage == guiDex_Data_Total_Page)
											{


												reset_5Sec_timer();
												delay_ms(500);
												Debug_PutString("\r\n Dex Data Complete");
												Send_Parced_Dex_data_Complete();
												start_5Sec_timer();
												guiSendResponseCounter=0;
												gucRept= dEND_OF_PARCED_DEX_DATA;
											}
											else
											{

												//delay_ms(1000);
												guiCurrentDexPage++;

												if(guiCurrentDexPage == guiDex_Data_Total_Page)
												{
													reset_5Sec_timer();

													Write_Data_on_BLE_Write(guiCurrentDexPage);
													delay_ms(500);

													Send_Parced_Dex_data_Complete();
													start_5Sec_timer();
													guiSendResponseCounter=0;
													gucRept= dEND_OF_PARCED_DEX_DATA;
												}
												else
												{
													reset_5Sec_timer();

													Write_Data_on_BLE_Write(guiCurrentDexPage);
													delay_ms(500);

													Send_Parced_Dex_Data_Update();
													start_5Sec_timer();
													guiSendResponseCounter=0;
													gucRept=dDEX_PAGE_UPDATE;
												}
											}
										}
										break;

				case dEND_DEX_DATA:
										if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
										{
											//start_5Sec_timer();
											//guiSendResponseCounter=0;
											//gucRept=dEND_DEX_DATA;
											reset_5Sec_timer();
											Debug_PutString("\r\n Got Dex Complete NACK from APP");
										}
										else
										{
											reset_5Sec_timer();
											Debug_PutString("\r\n Got Dex Complete ACK from APP");
										}
										break;

				case dSAFE_DISCONNECT:
												Debug_PutString("\r\n goto safe disconnect ACK from APP");
												break;

				case dEND_OF_PARCED_DEX_DATA:

											if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
											{

												guiSendResponseCounter=0;
												gucRept=dEND_OF_PARCED_DEX_DATA;
												reset_5Sec_timer();
												start_5Sec_timer();
												Debug_PutString("\r\n Got End of Parced DEX DATA  NACK from APP");

											}
											else
											{
												reset_5Sec_timer();
												guAxpect_Subcode = dNULL;
												Debug_PutString("\r\n Got End of Parced DEX DATA  ACK from APP");
											}
											break;

				case dPARCED_DEX_DATA:

											if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
											{
												start_5Sec_timer();
												guiSendResponseCounter=0;
												gucRept=dPARCED_DEX_DATA;
												Debug_PutString("\r\n Got the Parced DEX Data NACK from APP");
												delay_ms(500);
												Write_Data_on_BLE_Write(guiCurrentDexPage);
											}
											else
											{
												Debug_PutString("\r\n Got the Parced Data ACK from APP");
												if(guiCurrentDexPage == guiDex_Data_Total_Page)
												{
													reset_5Sec_timer();

													Write_Data_on_BLE_Write(0xFFFF);

													delay_ms(500);
													BLE_PutString("\r\n Dex Data Complete");
													Send_Parced_Dex_data_Complete();
													start_5Sec_timer();
													guiSendResponseCounter=0;
													gucRept=dEND_OF_PARCED_DEX_DATA;
												}
												else
												{

													//delay_ms(1000);
													guiCurrentDexPage++;
													if(guiCurrentDexPage == guiDex_Data_Total_Page)
													{
														Write_Data_on_BLE_Write(guiCurrentDexPage);

														delay_ms(500);
														Send_Parced_Dex_data_Complete();
														start_5Sec_timer();
														guiSendResponseCounter=0;
														gucRept=dEND_OF_PARCED_DEX_DATA;
													}
													else
													{
														Write_Data_on_BLE_Write(guiCurrentDexPage);

														delay_ms(500);
														Send_Parced_Dex_Data_Update();
														start_5Sec_timer();
														guiSendResponseCounter=0;
														gucRept=dDEX_PAGE_UPDATE;
													}
												}

											}
											break;

				case dTAB_TO_PAY_RQST:

										if((gucVendkinAppConnected) && (gucBLE_ReCBuffer[0]==0xA5)&&(!ucVendProcessRunning)&&(!guStartTaptoPAY_Comm)&&(!ucVendsession4mApp))
										{

											gucMony_Acc_Buff[0]=gucBLE_ReCBuffer[4];
											gucMony_Acc_Buff[1]=gucBLE_ReCBuffer[5];
											gucMony_Acc_Buff[2]=gucBLE_ReCBuffer[6];
											gucMony_Acc_Buff[3]=gucBLE_ReCBuffer[7];
											gucMony_Acc_Buff[4]=gucBLE_ReCBuffer[8];
											gucMony_Acc_Buff[5]=gucBLE_ReCBuffer[9];
											gucMony_Acc_Buff[6]=gucBLE_ReCBuffer[10];
											gucMony_Acc_Buff[7]=gucBLE_ReCBuffer[11];
											
											
											if (gucMDB_ReaderEnable_Flag)
											{
												guStartTaptoPAY_Comm=1;
												Cashless1_Step_Flg=10;
												Poll_state=1;

												ucTimmer_Credit_Count=0;
												ucTimmer_Credit_FLG=1;

												MDB_Master_PutChar(0X114);
												delay_us(interdataDelay);
												MDB_Master_PutChar(0X001);
												delay_us(interdataDelay);
												MDB_Master_PutChar(0X015);

												delay_ms(200);

												gpio_set_pin_level(DEVICE_SEL_RLY,0);
												gpio_set_pin_level(EPORT_M_S_RLY,0);

												Send_TAB_TO_PAY_RQST_ACK_NACK(dAPP_ACK);

												Debug_PutString("\r\n Got Tap to pay req");
												
											}
											else
											{
												Send_TAB_TO_PAY_RQST_ACK_NACK(dAPP_ACK);
												delay_ms(200);
												E_PORT_CreditStatus=0x02;
												Send_Money_Creadited_byCreditCard(E_PORT_CreditStatus);
												start_5Sec_timer();
												guiSendResponseCounter=0;
												gucRept=dMONY_ACCEPTED;
												
											}



										}


										break;
				case dMONY_ACCEPTED :
										if((gucBLE_ReCBuffer[4]==0x00) || (gucBLE_ReCBuffer[0]!=0xA6))
										{
											start_5Sec_timer();
											guiSendResponseCounter=0;
											gucRept=dMONY_ACCEPTED;
											Debug_PutString("\r\n Got Money Accepted NACK from APP");
										}
										else
										{
											reset_5Sec_timer();
											guiSendResponseCounter=0;
											guAxpect_Subcode = dNULL;
											Debug_PutString("\r\n Money Accepted ACK from APP");
										}
										break;
				default:
				break;
			}
		}
	 }

		//gucBLE_Frm_rec_Flag=0;
		//gucClear_BLE_Flags=1;//clear all flags for next reception
	}



}


unsigned char Verify_Checksum(unsigned char *c,int len)
{
	unsigned int i;
	unsigned char cal_checksum = 0 ;
	for (i=0 ; i < len ; i++)
	{
		cal_checksum ^= c[i] ;
	}
	return cal_checksum ;
}
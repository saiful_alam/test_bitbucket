/*
 * MDB_UART.h
 *
 * Created: 7/5/2021 3:04:45 PM
 *  Author: User
 */ 


#ifndef MDB_UART_H_
#define MDB_UART_H_


#define PB16 GPIO(GPIO_PORTB, 16)
#define PB17 GPIO(GPIO_PORTB, 17)

#define dMDB_BAUDRATE 9600



#define VMC_RESET_M			0x110
#define VMC_RESET			0x010

#define VMC_SETUP			0x111

#define VMC_POLL_M			0x112
#define VMC_POLL			0x012

#define VMC_VEND			0x113
#define VMC_READER			0x114
#define VMC_REVALUE			0x115
#define VMC_EXPANSION		0x117


 //// SAiful
#define RES_RESET_M				1
#define RES_JUSTRESET			2
#define RES_CONFIGSETUP			3
#define RES_POLL_ACK			4
#define RES_PERI_ID				5
#define RES_READER				6
#define RES_VENDRQST			7
#define RES_VNDSUCCESS			8
#define RES_SESSIONCOMPLETE		9
#define RES_VEND_CANCEL			10
#define RES_VEND_FAIL_SLAVE		11
#define RES_CASH_VEND_SLAVE		12
#define RES_REVALUE_RQST		13






#define  ACK_RCV_JUST_RESET			1
#define  ACK_RCV_CONFIGDATA			2
#define  ACK_RCV_MAXMIN_PRICE		3
#define	 ACK_RCV_PERI_ID			4
#define	 ACK_RCV_START_SESSION		5
#define  ACK_RCV_VEND_APPROVE		6
#define  ACK_RCV_END_SESSION		7
#define  ACK_RCV_SESSION_CANCEL		8
#define  ACK_RCV_VEND_DENIED		9














#define VMC_ACK_A                   0x0000 // Acknowledgment, Mode-bit is Clr
#define VMC_ACK                     0x0100 // Acknowledgment, Mode-bit is set
#define VMC_NAK                     0x00FF // Negative Acknowledgment
//#define VMC_SILENCE                 0xFFFF // This one is not from standard, it's an impossible value for the VMC


#define  VMC_POLL_ACK           0x01
#define JUST_RESET              0x00
#define READER_CONFIG_INFO      0x01
#define DISPLAY_REQUEST         0x02
#define BEGIN_SESSION           0x03
#define SESSION_CANCEL_REQUEST  0x04
#define VEND_APPROVED           0x05
#define VEND_DENIED             0x06
#define END_SESSION             0x07
#define CANCELLED               0x08
#define PERIPHERAL_ID           0x09
#define MALFUNCTION_ERROR       0x0A
#define CMD_OUT_OF_SEQUENCE     0x0B
#define DIAGNOSTIC_RESPONSE     0xFF

/*
 * MDB VMC Subcommands
 */
// VMC_SETUP
#define VMC_CONFIG_DATA    0x00
#define VMC_MAX_MIN_PRICES 0x01
// VMC_VEND
#define VMC_VEND_REQUEST          0x00
#define VMC_VEND_CANCEL           0x01
#define VMC_VEND_SUCCESS          0x02
#define VMC_VEND_FAILURE          0x03
#define VMC_VEND_SESSION_COMPLETE 0x04
#define VMC_VEND_CASH_SALE        0x05
// VMC_READER
#define VMC_READER_DISABLE 0x00
#define VMC_READER_ENABLE  0x01
#define VMC_READER_CANCEL  0x02
// VMC_EXPANSION
#define VMC_EXPANSION_REQUEST_ID  0x00
#define VMC_EXPANSION_DIAGNOSTICS 0xFF


#define VendCancelByVMC                     0x01
#define VendCancelByPriceMismatched         0x02
#define VendCancelWrongComprtment			0x03
#define VendCancelByTimeout                 0x04
#define KeyNotPressed                       0x05
#define KNoVendRqtForTap_N_Pay              0x06
#define EportVndStatusNotUpade              0x07
#define EportVCrediteExpired                0x08
#define KNoVendRqtForTap_N_Pay3             0x09
#define VendCancelByVMCTOEPORT				0X10
#define VendCancelWrongComprtmentEPORT		0x11
#define VendfailByTimeout                   0x12
#define No_creditFromEPORT					0x13
#define MDB_DisableAfter_BT_Connection		0x14




extern unsigned char rev_amount_H,rev_amount_L;

extern unsigned char gucMDB_Step;

extern unsigned char gucMDB_ReaderDisable_Flag,gucMDB_ReaderEnable_Flag,gucMDB_ReaderCancel_Flag;







extern unsigned int MDB_NEXT_POLL_STATE;
extern unsigned int guiBLERecPrice;
extern unsigned int guiVMCReceivedPrice,guiVMCReceivedCMP;

//extern unsigned char gucVMCRec_CMP_NUM[3];
//extern unsigned char gucVMCRec_CMP_PRICE[4];
//extern unsigned char gucBLERec_PRICE[4];

extern unsigned int gucMDB_RecBuff[80];
extern unsigned int guiMDB_RecBuff_Index;

extern unsigned char gucCancel_Session;


extern unsigned char gucVMC_PER_ID[30];
extern unsigned char gucVMC_PER_ID_Index;





typedef struct
{
	uint8_t featureLevel;
	uint8_t displayColumns;
	uint8_t displayRows;
	uint8_t displayInfo;
} VMC_Config_t;

typedef struct
{
	uint16_t featureLevel;
	uint16_t countryCodeH;
	uint16_t countryCodeL;
	uint16_t scaleFactor;
	uint16_t decimalPlaces;
	uint16_t maxResponseTime; // seconds, overrides default NON-RESPONSE time
	uint16_t miscOptions;
} CASHLESS_DEVICE_Config_t;

//typedef struct
//{
	//uint16_t maxPrice;
	//uint16_t minPrice;
//} VMC_Prices_t;

extern VMC_Config_t vmc_config;
//extern VMC_Prices_t vmc_prices;
extern CASHLESS_DEVICE_Config_t CASHLESS_Device_Config_INDIA,CASHLESS_Device_Config_US;




void initilize_MDB_uart(void);
void MDB_PutChar(unsigned int data);
void MDB_PutString(unsigned char *str) ;
void MDB_PutStringL(unsigned char *str,unsigned int len);



extern unsigned int calc_checksum(unsigned int *array, unsigned int arr_size);
void Send_Cashless_Device_Configuration(void);
void Send_Acknowledgement(void);
void Send_Just_Reset(void);
void Approved_Vend(void);
void Denied_Vend(void);
void Cancel_Session(void);
void Vend_Cancelled(void);
void Start_Session(void);
void End_Session(void);
void Peripheral_ID_Send(void);









void initilize_MDB_uart(void);
void MDB_PutChar(unsigned int data);
void MDB_PutString(unsigned char *str) ;
void MDB_PutStringL(unsigned char *str,unsigned int len);




#endif /* MDB_UART_H_ */
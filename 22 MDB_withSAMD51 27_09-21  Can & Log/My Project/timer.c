/*
 * timer.c
 *
 * Created: 30-Apr-19 1:06:10 PM
 *  Author: User
 */

#include "basic_requirement.h"



unsigned int gucLED_Counter=0;


unsigned int gui5SecCounter=0,guiSendResponseCounter=0;
unsigned char gucStart5SecTimerFlag=0,guc5SecIntFlag=0;

unsigned char gucStartRelayPressCounterFlag=0,gucRelayPressCounter=0;
unsigned int ucTimmer_Credit_Count=0;
unsigned char ucTimmer_Credit_FLG=0,Send_MonyACPFLG=0;
unsigned int uiEportVendReqstwithoutCredit=0;
unsigned char ucEPORT_NO_CrediteFlg=0;
unsigned int uiDisconnectionwaitCount=0;

unsigned char ucLoopStratFlg=0;
unsigned int ucLoopStratCount=0;
//unsigned char ucSuccesssendbyTimer=0;

//unsigned int uiNormalCardpaymentTimerCount=0;


void TC3_Handler(void)
{
	
	if (EportcreditwaitFlg)
	{
		Eportcreditwaitcount++;
		if (Eportcreditwaitcount> SEC*2)
		{
			Eportcreditwaitcount=0;
			EportcreditwaitTimer_ExpiredFlg=1; 
			
		}
		
		
	}
	
	
	
	
//// ****************  This timer  used for  pressing the keys 
	
	if (ucKey_DelayTimerSRT_FLG)
	{
		if ((ucKeyPressMask_Flg==1)||(ucKeyPressMask_Flg==0))
		//if ((ucKeyPressMask_Flg==0))
		{
			uiKey_DelayTimerCount++;
		}
		if (uiKey_DelayTimerCount==8)
		{
			ucKeyPressMask_Flg=2;
			//Debug_PutString("\r\nKeyOFF");
			
		}
		else if(uiKey_DelayTimerCount>=16)
		{
			ucKey_DelayTimerSRT_FLG=0;
			uiKey_DelayTimerCount=0;
			ucKeyPressMask_Flg=1;
			//Debug_PutString("\r\nNEXT KEY");
			
		}
	}
	
//// *******************************************	

//// ****************  This timer  used for  Timeout.
/// ** After receiving vend string if nothing happen from VMC then after this timer 

	if (uc_VndReqstTimerFLG)
	{
		ui_VndReqstTimerFCount++;
		
		//Debug_PutString("\r\nEPORTCredit TTTTTT:");
		if (ui_VndReqstTimerFCount>SEC*45)
		{
			
			ucVendingTimerExpiredFlg=1;
			uc_VndReqstTimerFLG=0;
			ui_VndReqstTimerFCount=0;
		}

	}


//// ********* Timer used for if EPORT does not  credit  to the VMC up to 30sec after receiving the vend request 
////********** then from this "ucEPORT_NO_CrediteFlg" timer flag   VNK HW will send a vend fail status to app.
	
if (ucEportVend_KeyPress)
	{
		uiEportVendReqstwithoutCredit++;
		if (uiEportVendReqstwithoutCredit>SEC*30)
		{
			uiEportVendReqstwithoutCredit=0;
			ucEPORT_NO_CrediteFlg=1;
			
		}
	}
///*************************************************************************************************


//// ********* Timer used  if sudden disconnect is happen at the middle of any transduction 

	if ((uc_SudddenDisconnectFlg)&&((gpio_get_pin_level(BLE_CONNECT_STATUS_PIN)==1)))
	{
		uiDisconnectionwaitCount++;
		if (uiDisconnectionwaitCount>SEC*20)
		{
			uiDisconnectionwaitCount=0;
			uc_SudddenDisconnectFlg=0;
			ucPresentMDB_Stage=0;
		}
	}

////******************  This timer is use to identifies whether the user tap the card or not. After   Tap& PAY request (DD)  from App this timer is activated and if there 
//******************** are not Begin  session  CMD from Eport within 50 Sec it will send the no money credite to the VMC (DE) to APP 
	 if (ucTimmer_Credit_FLG)
	 {
		 		 ucTimmer_Credit_Count++;
		 if (ucTimmer_Credit_Count>SEC*50)
		 {
			 Debug_PutString("\r\nEPORTCredit timer:");
			 Debug_Put_Num(ucTimmer_Credit_Count,3);
			 Send_MonyACPFLG=1;
			 ucTimmer_Credit_FLG=0;
			 ucTimmer_Credit_Count=0;
		 }

	 }
////********************************************************************************************

//******************** This timer is used for sending the repeat data to the BLE 

	 if (gucStart5SecTimerFlag)
	 {
		 gui5SecCounter++;
	 }

	 if ((gui5SecCounter>=50) && (gucStart5SecTimerFlag))
	 {
		 guc5SecIntFlag=1;
		 gui5SecCounter=0;
	 }
	////******************************************************************************************** 
	 

	 if(gucStartRelayPressCounterFlag)
	 {
		 gucRelayPressCounter++;

		  if((gucRelayPressCounter>=SEC*15))
		  {
			  gucRelayPressCounter=0;
			  gucStartRelayPressCounterFlag=0;
			  gucCancel_Session=1;

			  //ucResPollStage=SESSION_CANCEL_REQUEST_SLV;

		  }
	 }





//******************** This timer is used for terminating the initial while loop if  MDB SETUP not configure properly.

		 if (ucInitial_MDB_CommunicationTimer_Flg)
		 {
			 uiInitial_MDB_CommunicationTimer_Count++;	
			if (uiInitial_MDB_CommunicationTimer_Count>SEC*20)
			 {
				 uiInitial_MDB_CommunicationTimer_Count=0;
				 ucInitial_MDB_CommunicationTimer_Flg=0;
				 gucMDB_ReaderEnable_Flag=1;
				  Debug_PutString("Start ESC Timer\r\n");
				 MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
				 ucResPollStage=VMC_POLL_ACK;
				 ucRESET_RecvFLG=1;
			 }
		 }


////********************************************************************

//******************** This timer is used for Re initialize the controller .
	 if (ucLoopStratFlg)
	 {
		 ucLoopStratCount++;
		 if (ucLoopStratCount> (SEC*22))
		 {

			ucLoopStratCount=0;
			Debug_PutString("\r\n Main ReInit");
			NVIC_SystemReset();
		 }
	 }



	TC3->COUNT16.INTFLAG.reg |= 0xFF;
}

void init_timer100ms()
{
	GCLK->PCHCTRL[26].reg=  0x40;		//selecting the main core clock for SERCOM7
	MCLK->APBBMASK.reg |= MCLK_APBBMASK_TC3;


	// Configure Count Mode (16-bit)
	TC3->COUNT16.CTRLA.bit.MODE = 0x0;

	// Configure Prescaler for divide by 256 (0.5MHz clock to COUNT)
	TC3->COUNT16.CTRLA.bit.PRESCALER = 0x6;

	// Configure TC3 Compare Mode for compare channel 0
	TC3->COUNT16.WAVE.reg = 0x1;										// "Match Frequency" operation

	// Initialize compare value	 for 100mS
	TC3->COUNT16.CC[0].reg = 19520;

	// Enable TC3
	TC3->COUNT16.CTRLA.bit.ENABLE = 1;

	REG_TC3_INTENSET = 1;           /* enable overflow interrupt */
}



void reset_5Sec_timer(void)
{
	guc5SecIntFlag=0;
	gui5SecCounter=0;
	gucStart5SecTimerFlag=0;
}

void start_5Sec_timer(void)
{
	guc5SecIntFlag=0;
	gui5SecCounter=0;
	gucStart5SecTimerFlag=1;
}


void send_After_5sec(unsigned char lResp)
{
	guiSendResponseCounter++;

	if(guiSendResponseCounter<3)
	{
		switch(lResp)
		{
			case dAPP_CONNECTED:
				//					Debug_PutString("\r\nSending again Hardware Info bcz not getting ACK");
									Send_Hardware_Info();
									//Send_Hardware_Info_Debug18();
									start_5Sec_timer();
									break;

			case dAPP_DISCONNECTED:
			//						Debug_PutString("\r\n App Disconnect  Now");
									break;

			case dHARDWARE_INFO:
									Send_Hardware_Info();
									//Send_Hardware_Info_Debug18();
									start_5Sec_timer();
									break;

			case dVEND_PRODUCT:
			//						Debug_PutString("\r\n Got the vend request");
									break;

			case dVEND_SUCESS:
			//						Debug_PutString("\r\nSending again Vend Success bcz not getting ACK");
									Send_VendSucess();
									start_5Sec_timer();
									break;

			case dVEND_CANCEL:
			//						Debug_PutString("\r\nSending again Vend Cancel bcz not getting ACK");
									Send_VendCancel(gucVendCancelReasone);
									start_5Sec_timer();
									break;

			case dVEND_FAIL:
			//						Debug_PutString("\r\nSending again Vend Fail bcz not getting ACK");
									Send_VendFail(Vend_Fail_ErrorName);
									start_5Sec_timer();
									break;

			case dMACHINE_READY:
			//						Debug_PutString("\r\nSending again Machine Ready bcz not getting ACK");
									Send_MachineReady();
									start_5Sec_timer();
									break;

			case dSEND_DEX_DATA:
			//						Debug_PutString("\r\nSending again DEX DATA bcz not getting ACK");
									break;

			case dDEX_PAGE_UPDATE:
			//						Debug_PutString("\r\nSending again DEX Page Upadate bcz not getting ACK");
									Send_Parced_Dex_Data_Update();
									start_5Sec_timer();
									break;

			case dEND_OF_PARCED_DEX_DATA:
			//						Debug_PutString("\r\nSending again End of PARCED DEX DATA bcz not getting ACK");
									Send_Parced_Dex_data_Complete();
									start_5Sec_timer();
									break;

			case dEND_DEX_DATA:
			//						Debug_PutString("\r\nSending again End of DEX DATA bcz not getting ACK");
									break;

			case dSAFE_DISCONNECT:
			//						Debug_PutString("\r\n got safe disconnect ACK");
									break;

			case dPARCED_DEX_DATA:
			//						Debug_PutString("\r\nSending again PARCED DEX DATA bcz not getting ACK");
									Send_Parced_DEX_start_Info();
									start_5Sec_timer();
									break;
			case dMONY_ACCEPTED:
									//Debug_PutString("\r\nSending again Machine Ready bcz not getting ACK");
									Send_Money_Creadited_byCreditCard(E_PORT_CreditStatus);
									start_5Sec_timer();
									break;
			default:
									break;
		}
	}
	else
	{
		//		Debug_PutString("\r\n sending data 3 time done but not getting any response from APP");
		  guiSendResponseCounter=0;
		  switch(lResp)
		  {
			  case dAPP_CONNECTED:
									//                                    DEBUG_BLE_PutString("R-dAPP_CONNECTED\r\n");
									guAxpect_Subcode = dVEND_PRODUCT;
									gucVendkinAppConnected=1;
									gucAPPConnected=1;

									break;
			  case dAPP_DISCONNECTED:
									 break;

			  case dHARDWARE_INFO:   //DEBUG_BLE_PutString("R-dHARDWARE_INFO\r\n");
									  guAxpect_Subcode = dVEND_PRODUCT;
									  gucVendkinAppConnected=1;
									  gucAPPConnected=1;
									  break;

			  case dVEND_PRODUCT:
										break;

			  case dVEND_SUCESS:    //DEBUG_BLE_PutString("dMACHINE_READY\r\n");
									  guAxpect_Subcode = dMACHINE_READY;
									  
									  break;

			  case dVEND_CANCEL:
									  guAxpect_Subcode = dMACHINE_READY;
									  
									  break;

			  case dVEND_FAIL:
									  guAxpect_Subcode = dMACHINE_READY;
									 
									  break;

			  case dMACHINE_READY:
									  guAxpect_Subcode = dVEND_PRODUCT;
									  break;



			  default:
								 break;
		  }
		reset_5Sec_timer();
	}

}


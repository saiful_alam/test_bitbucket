/*
 * basic_requirement.h
 *
 * Created: 2019-12-09 11:51:39 AM
 *  Author: User
 */ 


#ifndef BASIC_REQUIREMENT_H_
#define BASIC_REQUIREMENT_H_

#include <atmel_start.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sam.h"

#include "MDB_UART.h"
#include "MDB_Protocol.h"
#include "DEBUG_UART.h"
#include "ble.h"
#include "Cashless_1.h"
#include "timer.h"
#include "keypad.h"
#include "BLE_PROTOCOL.h"
#include "MDB_MASTER_UART.h"
#include "DebugPort_Two.h"

#define dXTAL_FREQUENCY		48000000

#define  dBLE_BAUDRATE				115200
#define  dMDB_BAUDRATE				9600
#define  dDEX_BAUDRATE				9600
#define  dDEBUG_BAUDRATE			115200
#define  dMDB_MASTER_BAUDRATE		9600



#define SnB                 0x01
#define TnC_BaseVersion     0x02
#define SANITORY            0x03
#define MMS                 0x04
#define PIZZA               0x05
#define OATS                0x06
#define ELEVEND             0x07


#define dHARDWARE_VERSION	0x01
#define dSOFWARE_VERSION	0x05
#define dMACHINE_TYPE       SnB



extern unsigned char ucInitial_MDB_CommunicationTimer_Flg;
extern unsigned long uiInitial_MDB_CommunicationTimer_Count;

extern unsigned char gucAPPConnected;
extern unsigned char guStartTaptoPAY_Comm,ucEportVend_KeyPress;
extern unsigned char ucInitialMDBCommError;
extern unsigned char uc_start_Flg;
extern unsigned int ucEportVend_KeyPressTiemr;










#define BLE_Status_LED              GPIO(GPIO_PORTA, 28)
#define BLE_RESET_PIN               GPIO(GPIO_PORTA, 19)
#define BLE_CONNECT_STATUS_PIN      GPIO(GPIO_PORTC, 16)




#define DEX_LED GPIO(GPIO_PORTA,23)
#define TOGGLE_LED GPIO(GPIO_PORTB, 18)
#define MDB_TEST_LED GPIO(GPIO_PORTA, 22)



#define DEX_SEL_RLY GPIO(GPIO_PORTB,19)
#define EPORT_M_S_RLY GPIO(GPIO_PORTA, 21)
#define DEVICE_SEL_RLY   GPIO(GPIO_PORTC, 20)

//-------------------------------------------------------------------------------------------

void PORT_INIT (void);


unsigned char Hex_to_char(unsigned char temp);
void Debug_CovertString_in_ASCII(uint8_t *data, uint16_t len);
void Debug_CovertChar_in_ASCII(uint8_t data);


#endif /* BASIC_REQUIREMENT_H_ */
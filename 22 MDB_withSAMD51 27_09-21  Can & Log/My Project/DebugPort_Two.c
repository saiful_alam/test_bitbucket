/*
 * DebugPort_Two.c
 *
 * Created: 21-09-2021 16:03:57
 *  Author: USer
 */ 
#include "basic_requirement.h"

unsigned char Convert_DataArray[30];


/* initialize UART3 to transmit at 9600 Baud */
void initilize_Debug_Two_uart(void) 
{
	GCLK->PCHCTRL[37].reg=  0x40;		//selecting the main core clock for SERCOM2
	GCLK->PCHCTRL[3].reg=  0x43;		//selecting slow clock for SERCOM2
	MCLK->APBDMASK.reg |= MCLK_APBDMASK_SERCOM7;
	
	
	gpio_set_pin_function(PB20, PINMUX_PB20D_SERCOM7_PAD1);
	gpio_set_pin_function(PC12, PINMUX_PC12C_SERCOM7_PAD0);
	
	REG_SERCOM7_USART_CTRLA |= 1;               /* reset SERCOM7 */
    while (REG_SERCOM7_USART_SYNCBUSY & 1) {}   /* wait for reset to complete */
    REG_SERCOM7_USART_CTRLA = 0x40106004;       /* LSB first, async, no parity,
        PAD[0]-Tx, BAUD uses fraction, 8x oversampling, internal clock */
    REG_SERCOM7_USART_CTRLB = 0x00030000;       /* enable Tx/Rx, one stop bit, 8 bit */
    REG_SERCOM7_USART_BAUD  = (((float)dXTAL_FREQUENCY / 8)/dBLE_BAUDRATE);	//set Baud rate ;               /* 1000000/8/9600 = 13.02 */
    REG_SERCOM7_USART_CTRLA |= 2;               /* enable SERCOM6 */
    while (REG_SERCOM7_USART_SYNCBUSY & 2) {}   /* wait for enable to complete */
		
	//REG_SERCOM6_USART_INTENSET = 4;             /* enable receive complete interrupt */
	
}


void Debug_Two_PutChar(char data)
{
	while(!(REG_SERCOM7_USART_INTFLAG & 1)) {}  /* wait for data register empty */
	REG_SERCOM7_USART_DATA = data;              /* send a char */
}


void Debug_Two_PutString(char *str)
{
	unsigned int i=0;
	while(str[i]!='\0')                 //check for NULL character to terminate loop
	{
		while(!(REG_SERCOM7_USART_INTFLAG & 1)) {}  /* wait for data register empty */
		REG_SERCOM7_USART_DATA = str[i];              /* send a char */
		i++;
	}
}
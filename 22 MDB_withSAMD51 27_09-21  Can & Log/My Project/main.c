//#include <atmel_start.h>

#include "basic_requirement.h"

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	initilize_Debug_uart();
	initilize_BLE_uart();
	initilize_MDB_uart();
	initilize_MDB_Master_uart();
	initilize_Debug_Two_uart();
	
	 init_timer100ms();
	Debug_PutString("\r\nInit Debug UART");
	Debug_Two_PutString("\r\nInit Debug TWO UART");
	
	
	Debug_PutString("\r\nInit MDB UART");
	Debug_PutString("\r\nINT BLE");
	 PORT_INIT ();
	 KeyPAD_INIT();
	  gpio_set_pin_level(BLE_RESET_PIN,1);
	  gpio_set_pin_level(MDB_TEST_LED,1); 
	   gpio_set_pin_level(DEX_LED,1);   


	NVIC_EnableIRQ(SERCOM2_2_IRQn);   // enable Blue tooth UART interrupt at NVIC
	NVIC_EnableIRQ(SERCOM5_2_IRQn);   /* enable interrupt at NVIC */
	NVIC_EnableIRQ(SERCOM3_2_IRQn);   /* enable interrupt at NVIC */
	NVIC_EnableIRQ(TC3_IRQn);     // enable TC3 interrupt in NVIC
	__enable_irq();                 /* enable interrupt globally */
	
	
		gucMDB_Step=0;
		guAxpect_Subcode = dAPP_CONNECTED;
	    gucVendCancelReasone=0x00;

			
			//BLE_CONNECT();
		Debug_PutString("\r\nMain Loop11111");		
	    gpio_set_pin_level(EPORT_M_S_RLY,1);
	   gpio_set_pin_level(DEVICE_SEL_RLY,1);
	   ucInitial_MDB_CommunicationTimer_Flg=1;
	   uiInitial_MDB_CommunicationTimer_Count=0;
	  gpio_set_pin_level(BLE_RESET_PIN,0); 
	  
	  gucAPPConnected=0;
	  delay_ms(200);
     
		  
	    ucInitialMDBCommError=1;
		while (!gucMDB_ReaderEnable_Flag)
		{
            BLE_CONNECT();

			MDB_Communication();
			//delay_us(10);
			gucLED_Counter++;
			if ((gucLED_Counter>=500))
			{
				//gpio_toggle_pin_level(TOGGLE_LED);
				gucLED_Counter=0;
			}
			if (gucMDB_ReaderEnable_Flag)
			{
				
				ucInitial_MDB_CommunicationTimer_Flg=0;
				ucInitialMDBCommError=0;
				uc_start_Flg=1;
				break;
			}
		}
	
	
	   delay_ms(500);
		gpio_set_pin_level(DEVICE_SEL_RLY,0);
		gpio_set_pin_level(EPORT_M_S_RLY,0);
	
	////**********************************************************

		
	
	Cashless1_Step_Flg=10;
	//Poll_state=3;

	
	Debug_PutString("\r\nMain Loop entry");
	
	/* Replace with your application code */
	
		//Cashless1_Step_Flg=10;
		//Poll_state=3;

		
	ucLoopStratFlg=1;	
	while (1) 
	{
		ucLoopStratCount=0;  //  ****************************
		
		//toggle LED here
		gucLED_Counter++;
		
		if ((gucLED_Counter>=50000))
		{
			 static unsigned int count1=0;
			 count1++;
			 
			//Compare_2D_Array(0x36); 
			//
			//Preseed_Matrix_Key(ucMatched_Row,ucMatched_Col);
			 
			//Debug_PutString("\r\nCount:") ;
			//Debug_Put_Num(count1,5);
			gpio_toggle_pin_level(TOGGLE_LED);
			gucLED_Counter=0;
		}

		BLE_CONNECT();
		BLE_Communication();  
		MDB_Communication();
		Listen_EPORT_AsMaster();

		
			if (Send_MonyACPFLG)
			{
				Send_MonyACPFLG=0;

				Debug_PutString("\r\nT Session");
				ucTimmer_Credit_FLG=0;
				ucTimmer_Credit_Count=0;
				E_PORT_CreditStatus=0x00;
				guStartTaptoPAY_Comm=0;
				E_PORT_Start_SessionFLG=0; 
				gpio_set_pin_level(EPORT_M_S_RLY,1);
				gpio_set_pin_level(DEVICE_SEL_RLY,true);//connect the RX line with VNK Device
				
				Send_Money_Creadited_byCreditCard(E_PORT_CreditStatus);
				
				delay_ms(300);
				
				//Send_Money_Creadited_byCreditCard(E_PORT_CreditStatus);
				MDB_Master_PutChar(0X114);
				delay_us(interdataDelay);
				MDB_Master_PutChar(0X000);
				delay_us(interdataDelay);
				MDB_Master_PutChar(0X014);
				
				
				
				start_5Sec_timer();
				guiSendResponseCounter=0;
				gucRept=dMONY_ACCEPTED;
				
			}
		
		
		
		
	}
}

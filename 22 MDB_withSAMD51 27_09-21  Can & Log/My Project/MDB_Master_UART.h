/*
 * MDB_UART.h
 *
 * Created: 7/7/2021 4:02:01 PM
 *  Author: User
 */ 


#ifndef MDB_MASTER_UART_H_
#define MDB_MASTER_UART_H_


#define PA13 GPIO(GPIO_PORTA, 13)
#define PA12 GPIO(GPIO_PORTA, 12)

extern unsigned char gucCardReaderSession_Start,gucCardReaderSession_END;
extern unsigned char RCV_TO_Buff_FLG;

extern unsigned char  EportToVmc_Buff[40];
extern unsigned int EportToVmc_Buff_Index;
extern unsigned char ucEportToVmc_RcvCompleteFLG;


void initilize_MDB_Master_uart(void);
void MDB_Master_PutChar(uint32_t data) ;
void MDB_Master_PutString(char *str);
void MDB_Master_PutStringL(unsigned char *str,unsigned int len);

#endif /* MDB_UART_H_ */